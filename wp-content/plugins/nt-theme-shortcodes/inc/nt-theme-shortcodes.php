<?php



// word limiter
function cryptoland_limit_words($string, $limit) {
    $words = explode(' ', $string);
    return implode(' ', array_slice($words, 0, $limit));
}

// element animation if have it
if ( ! function_exists( 'cryptoland_aos' ) ) {
    function cryptoland_aos( $aos, $delay, $duration, $offset, $easing,  $anchor, $placement, $once ) {

        if ( ( isset( $aos  ) ) && ( $aos  != '' ) ){

            $el_aos = ( isset( $aos       ) && $aos  	!= '' ) ? ' data-aos="'. $aos .'"' : '';
            $el_del = ( isset( $delay     ) && $delay != '' ) ? ' data-aos-delay="'. $delay .'"' : '';
            $el_eas = ( isset( $easing    ) && $easing != '' ) ? ' data-aos-easing="'. $easing .'"' : '';
            $el_dur = ( isset( $duration  ) && $duration != '' ) ? ' data-aos-duration="'. $duration .'"' : '';
            $el_off = ( isset( $offset    ) && $offset != '' ) ? ' data-aos-offset="'. $offset .'"' : '';
            $el_anc = ( isset( $anchor    ) && $anchor != '' ) ? ' data-aos-anchor="'. $anchor .'"' : '';
            $el_pla = ( isset( $placement ) && $placement != '' ) ? ' data-aos-placement="'. $placement .'"' : '';
            $el_once = ( isset( $once      ) && $once != '' ) ? ' data-aos-once="'. $once .'"' : '';

            $data = $el_aos . " " . $el_del . " " . $el_eas . " " . $el_dur . " " . $el_off . " " . $el_anc . " " . $el_pla . " " . $el_once;
            return $data;

        } else {
            return false;
        }

    }
}

// visual composer custom css class for design type
if ( ! function_exists( 'cryptoland_vc_custom_css_class' ) ) {
    function cryptoland_vc_custom_css_class( $var ) {
        $bg  = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $var, ' ' ),  $atts='' );
        return $bg;
    }
}

// typography h1-h6, p elements
if ( ! function_exists( 'cryptoland_element' ) ) {
    function cryptoland_element( $text='Add your text', $tag='p', $class='', $color='', $size='', $lineh='', $anim='' ) {

        if ( ( isset( $text ) ) && ( $text != '' ) ) {

            $tags = array(
                'div'  => array( 'class' => array(),	'id' => array() ),
                'ul'  => array( 'class' => array(),	'id' => array() ),
                'li'  => array( 'class' => array(),	'id' => array() ),
                'span'  => array( 'class' => array(), 'id' => array() ),
                'i'   => array( 'class' => array() ),
                'mark'  => array(),
                'p'   => array( 'class'  => array() ),
                'b'   => array(),
                'br'  => array(),
                'strong'=> array(),
                'a'   => array( 'href' => array(),'class' => array(),'title' => array() )
            );
            $text = wp_kses( $text, $tags );

            $el_tag 	  = ( isset( $tag    ) && $tag  	!= '' ) ? $tag : '';
            $el_class = ( isset( $class  ) && $class  != '' ) ? ' class="'.$class.'"' : '';
            $lineheight = ( isset( $lineh  ) && $lineh  != '' ) ? ' line-height:'.esc_attr( $lineh ).'!important;' 	: '';
            $fontsize = ( isset( $size   ) && $size   != '' ) ? ' font-size:'.esc_attr( $size ).'!important;' 	: '';
            $fontcolor = ( isset( $color  ) && $color  != '' ) ? ' color:'.esc_attr( $color ).'!important;' 		: '';
            $el_transform = ( isset( $transform   ) && $transform   != '' ) ? ' text-transform:'.esc_attr( $transform ).'!important;' 	: '';
            $anim = ( isset( $anim   ) && $anim   != '' ) ?  $anim : '';

            if ( $fontsize != '' || $fontcolor != '' || $lineheight != '' || $el_transform != '' ){
                $style = ' style="'.$fontsize.$fontcolor.$lineheight.$el_transform.'"';
            } else {
                $style = '';
            }

            if ( $el_tag != '' ){
                $element = '<'.$el_tag.''.$el_class.''.$style.''.$anim.'>'.$text.'</'.$el_tag.'>';
            } else {
                $element = $text;
            }

        } else {  return false; } // end $text

        return $element;
    } // end function
} // end function_exists

// typography h1-h6, p elements
if ( ! function_exists( 'cryptoland_css' ) ) {
    function cryptoland_css( $c='', $s='', $lh='' ) {

        $f_c = ( isset( $c  ) && $c  != '' ) ? ' color:'.esc_attr( $c ).'!important;' 		: '';
        $f_s = ( isset( $s   ) && $s   != '' ) ? ' font-size:'.esc_attr( $s ).'!important;' 	: '';
        $l_h = ( isset( $lh  ) && $lh  != '' ) ? ' line-height:'.esc_attr( $lh ).'!important;' 	: '';

        if ( $f_c != '' || $f_s != '' || $l_h != '' ){
            return $style = ' style="'.$f_c.$f_s.$l_h.'"';
        }else{
            return false;
        }
    } // end function
} // end function_exists



// icon function
if ( ! function_exists( 'cryptoland_icon' ) ) {
    function cryptoland_icon( $el_class='fa fa-pencil', $el_size='', $el_color='' ) {

        if ( ( isset( $el_class  ) ) && ( $el_class  != '' ) ) {

            $class = ( isset( $el_class  ) && $el_class  != '' ) ? ' class="'.esc_attr( $el_class ).'"' : '';
            $size  = ( isset( $el_size   ) && $el_size   != '' ) ? ' font-size:'.esc_attr( $el_size ).' !important;' : '';
            $color = ( isset( $el_color  ) && $el_color  != '' ) ? ' color:'.esc_attr( $el_color ).' !important;' : '';

            if ( $size != '' || $color != '' ){ 	$style = ' style="'.$size.$color.'"'; } else {  $style = '';  }

            $icon = '<span'.$class.''.$style.'></span>';

            return $icon;

        } else {
            return false;
        }

    }
}

//button function
if ( ! function_exists( 'cryptoland_btn' ) ) {
    function cryptoland_btn(  $link='', $class='', $bgtype='', $bg='', $clr='', $hvrbg='', $hvrclr='', $innerel='', $icon='' ) {

        $link	= ( $link == '||' ) ? '' : $link;
        $link	= vc_build_link( $link );
        $title	=  isset( $link['title'] ) && $link['title'] != '' ? esc_html( $link['title']) : '';
        $target	=  isset( $link['target'] ) && $link['target'] != '' ? ' target="'.esc_attr( $link['target']).'"' : '';
        $href	=  isset( $link['url'] ) && $link['url'] != '' ? ' href="'.esc_attr( $link['url']).'"' : '';

        if ( $title  ) {
            $class	= ( isset( $class ) && $class  != '' ) ? ' class="'.esc_attr( $class ).'"' : '';
            $bg = ( isset( $bg ) && $bg != '' ) ? 'background-color:'.esc_attr( $bg ).';background-image:none!important;' : '';
            $clr	= ( isset( $clr ) && $clr  != '' ) ? 'color:'.esc_attr( $clr ).';' : '';
            $style  = ( $bg != '' || $clr != '' ) ? ' style="'.$bg.$clr.'"' : '';

            $hvrbg	= ( isset( $hvrbg ) && $hvrbg != '' ) ? ' data-hvrbg="'.esc_attr( $hvrbg ).'"' : '';
            $hvrclr	= ( isset( $hvrclr ) && $hvrclr != '' ) ? ' data-hvrclr="'.esc_attr( $hvrclr ).'"' : '';
            $bgdata	 = ( isset( $bgtype ) == 'custom' || $bgtype  == 'custom' ) ? ' data-btnstyle="custom"' : '';
            $data  = ( $hvrbg != '' && $hvrclr != '' ) ? $bgdata.$hvrbg.$hvrclr : '';

            $title = ( $innerel != '' ) ? '<'.$innerel.'>'.$title.'</'.$innerel.'>' : $title;

            $bgtype	 = ( isset( $bgtype ) == 'custom' || $bgtype  == 'custom' ) ? $bgtype : '';

            if($bgtype == 'custom'){
                $btn = '<a'.$class.$href.$target.$style.$data.'>'.$title.'</a>';
            }else{
                $btn = '<a'.$class.$href.$target.'>'.$title.'</a>';
            }
            return $btn;
        }else{
            return false;
        }// end if
    }
}

// button function
if ( ! function_exists( 'cryptoland_img' ) ) {
    function cryptoland_img( $imgsrc='', $imgclass='', $imgwidth='200', $imgheight='200', $data='' ) {

        if ( ( isset( $imgsrc ) ) && ( $imgsrc != '' ) ) {

            // Gets url of the image
            $image = wp_get_attachment_url( $imgsrc,'full' ); // Gets the Image alt
            $image_alt = get_post_meta( $imgsrc, '_wp_attachment_image_alt', true );
            // Gets the image name with exstention
            $image_filename = basename ( get_attached_file( $imgsrc ) );
            // Gets the image name without exstention
            $image_title = get_the_title( $imgsrc );

            if (  $image_alt != '' ) {
                $imagealt = $image_alt;
            }else{
                $imagealt = $image_filename;
            }

            // Gets url of the image
            $img_src   = ( isset( $imgsrc ) && $imgsrc != '' ) ? ' src="'.esc_url( $image ).'"' : '';
            $img_class = ( isset( $imgclass ) && $imgclass != '' ) ? ' class="'.esc_attr( $imgclass ).'"' : '';
            $img_width = ( isset( $imgwidth ) && $imgwidth != '' ) ? ' width="'.esc_attr( $imgwidth ).'"' : '';
            $img_height = ( isset( $imgheight ) && $imgheight != '' ) ? ' height="'.esc_attr( $imgheight ).'"' : '';
            $img_data = ( isset( $data ) && $data != '' ) ? ' data-aos="'.esc_attr( $data ).'"' : '';
            $img_alt = ( $imagealt != '' ) ? ' alt="'.esc_attr( $imagealt ).'"' : '';

            $imageo = '<img'.$img_src . $img_class . $img_width . $img_height . $img_alt . $img_data.'>';

            return $imageo;
        }
        else{
            return false;
        } // end imgsrc

    } // end cryptoland_img
} // end function_exists



/*-----------------------------------------------------------------------------------*/
/*	HERO HOME 1
/*-----------------------------------------------------------------------------------*/

function cryptoland_home_hero( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"subtitle" => '',
	"bigtitle" => '',
	"smtitle" => '',
	"text" => '',
	"con_aos" => '',
	"rimg" => '',
	"img_aos" => '',
	"bgimg" => '',
	"bgoff" => '',
	"bgclrtype" => '',
	"bgclr" => '',
	"bgclr1" => '',
	"bgclr2" => '',
	//custom style
	"stclr" => '',
	"stsz" => '',
	"stlh" => '',
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	"smtclr" => '',
	"smtsz" => '',
	"smtlh" => '',
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	"css" => '',
	), $atts) );

	$bg_off = $bgoff != 'no' ? ' xs-bgimg-off' : '';
	$bg_on = $bgoff == 'yes' ? ' xs-bgclr-on' : '';
	$con_aos = $con_aos != '' ? $con_aos : '';
	$bgimg  = wp_get_attachment_url( $bgimg,'full' );

	$bgclr = $bgclr != '' ? $bgclr : '#9c6ea9';
	$bgclr1 = $bgclr1 != '' ? $bgclr1 : '#9c6ea9';
	$bgclr2 = $bgclr2 != '' ? $bgclr2 : '#f2949e';

	$bgclrtype = $bgclrtype != 'one' ? ' style="background: -webkit-gradient(linear,left top,right top,color-stop(-2.49%,'.$bgclr1.'),to('.$bgclr2.'));
    background: -webkit-linear-gradient(left,'.$bgclr1.' -2.49%,'.$bgclr2.' 100%);
    background: -o-linear-gradient(left,'.$bgclr1.' -2.49%,'.$bgclr2.' 100%);
    background: linear-gradient(90deg,'.$bgclr1.' -2.49%,'.$bgclr2.' 100%);"' : ' style="background-color:'.$bgclr.';"';

	$bgimg  = $bgimg != '' ? ' style="background: url('.$bgimg.');background-position: bottom center;background-repeat: no-repeat;background-size: cover;"' : '' ;

	$out = '';
		$out .= '<div class="promo'.$bg_off.'"'.$bgimg.'>';
		$out .= '<div class="promobgclr'.$bg_on.'"'.$bgclrtype.'></div>';
			$out .= '<div class="container">';
				$out .= '<div class="row align-items-center ">';
					$out .= '<div class="col">';
						$out .= '<div class="promo__content" data-aos="'.$con_aos.'">';
							if ( $subtitle !='' ){$out .= '<div class="promo__subtitle"'.cryptoland_css( $stclr, $stsz, $stlh ).'>'.$subtitle.'</div>';}

							if ( $bigtitle !='' ){$out .= '<h1 class="title title--big title--white promo__title"'.cryptoland_css( $tclr, $tsz, $tlh ).'>'.$bigtitle.'';}
							if ( $smtitle !='' ){$out .= '<span'.cryptoland_css( $smtclr, $smtsz, $smtlh ).'>'.$smtitle.'</span>';}
							if ( $bigtitle !='' ){$out .= '</h1>';}
							if ( $text !='' ){$out .= '<p'.cryptoland_css( $pclr, $psz, $plh ).'>'.$text.'</p>';}

							$out .= do_shortcode( $content );

						$out .= '</div>';

						$out .= cryptoland_img( $rimg, $imgclass='promo__img', $imgwidth='', $imgheight='', $data='' );

					$out .= '</div>';
				$out .= '</div>';
			$out .= '</div>';
		$out .= '</div>';

	return $out;
}
add_shortcode('cryptoland_home_hero_shortcode', 'cryptoland_home_hero');


/*-----------------------------------------------------------------------------------*/
/*	HERO HOME 3
/*-----------------------------------------------------------------------------------*/

function cryptoland_home3_hero( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "title" => '',
    "desc" => '',
    "contanim" => '',
    "promoimg" => '',
    "imganim" => '',
    "scrollimg" => '',
	//timer
    "hidetimer" => '',
    "hidelabel" => '',
    "dlabel" => '',
    "hlabel" => '',
    "minlabel" => '',
    "seclabel" => '',
    "month" => '',
    "day" => '',
    "year" => '',
    "hours" => '',
    "min" => '',
    "sec" => '',
    "msec" => '',
	//btn 1
	"link1" => '',
	"btnsize1" => '',
	"bgtype1" => '',
	"btnbg1" => '',
	"btnclr1" => '',
	"hvrbg1" => '',
	"hvrclr1" => '',
	//btn 2
	"link2" => '',
	"btnsize2" => '',
	"bgtype2" => '',
	"btnbg2" => '',
	"btnclr2" => '',
	"hvrbg2" => '',
	"hvrclr2" => '',
	//payments img
    "payments" => '',
    "payimg" => '',
  	//custom style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	//subtitle
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	//timer label
	"lclr" => '',
	"lsz" => '',
	"llh" => '',
	//css
	"css" => '',
	), $atts) );

	$loop = (array) vc_param_group_parse_atts($payments);

	//2018, 11, 5, 0, 0, 0, 0
	$year = $year != '' ? esc_attr($year).', ' : '2019 ';
	$month = $month != '' ? esc_attr($month).', ' : '11 ';
	$day = $day != '' ? esc_attr($day).', ' : '5, ';
	$hours = $hours != '' ? esc_attr($hours).', ' : '0, ';
	$min = $min != '' ? esc_attr($min).', ' : '0, ';
	$sec = $sec != '' ? esc_attr($sec).', ' : '0, ';

	$msec =  '0';
	$time = $year.$month.$day.$hours.$min.$sec.$msec;

	$lclr = $lclr != '' ? ' color:'.$lclr.';' : '';
	$lsz = $lsz != '' ? ' font-size:'.esc_attr($lsz).';' : '';
	$llh = $llh != '' ? ' line-height:'.esc_attr($llh).';' : '';
	$labelstyle = $lclr != '' || $lsz != '' || $llh != ''? ' style="'.$lclr.$lsz.$llh.'"' : '';

	$out = '';

		$out .= '<div class="promo promo3 '.cryptoland_vc_custom_css_class($css).'">';
			$contanim = $contanim != '' ? ' data-aos="'.esc_attr($contanim).'"' : '';
			$out .= '<div class="promo__content"'.$contanim.'>';
					//title
					$out .= cryptoland_element( $title, $tag='h1', $class='', $tclr, $tsz, $tlh, $anim='' );
					//desc
					$out .= cryptoland_element( $desc, $tag='p', $class='', $pclr, $psz, $plh, $anim='' );

				$hidelabelclass = $hidelabel != false ? ' hiding-label' : '';
				$out .= '<div class="timer-wrap'.$hidelabelclass.'">';
					//timer
					if( $hidetimer != true ){
					$out .= '<div class="timer timer2" id="timer" data-date="'.$time.'"></div>';
					}
					if( $hidetimer != true AND $hidelabel != true ){
					$out .= '<div class="timer__titles"'.$labelstyle.'>';
						$dlabel = $dlabel != '' ? esc_attr($dlabel) : 'Days';
						$hlabel = $hlabel != '' ? esc_attr($hlabel) : 'Hours';
						$minlabel = $minlabel != '' ? esc_attr($minlabel) : 'Minutes';
						$seclabel = $seclabel != '' ? esc_attr($seclabel) : 'Seconds';
						$out .= '<div>'.esc_html($dlabel).'</div>';
						$out .= '<div>'.esc_html($hlabel).'</div>';
						$out .= '<div>'.esc_html($minlabel).'</div>';
						$out .= '<div>'.esc_html($seclabel).'</div>';
					$out .= '</div>';
					}
				$out .= '</div>';

				$out .= '<div class="promo__btns-wrap">';
					//bottom button 1
					$out .= cryptoland_btn( $link1,$class='btn btn--orange '.$btnsize1,$bgtype1,$btnbg1,$btnclr1,$hvrbg1,$hvrclr1,$innerel='span',$icon='' );
					//bottom button 2
					$out .= cryptoland_btn( $link2,$class='btn btn--blue '.$btnsize2,$bgtype2,$btnbg2,$btnclr2,$hvrbg2,$hvrclr2,$innerel='',$icon='' );
				$out .= '</div>';
			if ( ! empty($loop) ){
				$out .= '<div class="payments">';
						foreach ( $loop as $item ) {
						if ( isset($item['payimg']) !='' ){$out .= ''.cryptoland_img( $item['payimg'], $imgclass='', $imgwidth='', $imgheight='', $data='' ).'';}
						}
				$out .= '</div>';
			}
			$out .= '</div>';
			$imganim = $imganim != '' ? esc_attr($imganim) : '';
			$out .= ''.cryptoland_img( $promoimg, $imgclass='promo__img', $imgwidth='', $imgheight='', $data=$imganim ).'';
			$out .= '<div class="scroll-down">';
				$out .= ''.cryptoland_img( $scrollimg, $imgclass='', $imgwidth='', $imgheight='', $data=$imganim ).'';
			$out .= '</div>';
		$out .= '</div>';

	return $out;
}
add_shortcode('cryptoland_home3_hero_shortcode', 'cryptoland_home3_hero');

/*-----------------------------------------------------------------------------------*/
/*	HERO HOME 4
/*-----------------------------------------------------------------------------------*/

function cryptoland_home4_hero( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "title" => '',
    "thintitle" => '',
    "desc" => '',
    "titleanim" => '',
    "titledelay" => '',
    "scrollimg" => '',
	//timer
    "hidetimer" => '',
    "hidelabel" => '',
    "dlabel" => '',
    "hlabel" => '',
    "minlabel" => '',
    "seclabel" => '',
    "month" => '',
    "day" => '',
    "year" => '',
    "hours" => '',
    "min" => '',
    "sec" => '',
    "msec" => '',
    "timeranim" => '',
    "timerdelay" => '',
	//text
	"text1" => '',
	"text2" => '',
    "textanim" => '',
    "textdelay" => '',
	//btn 1
	"link1" => '',
	"btnsize1" => '',
	"bgtype1" => '',
	"btnbg1" => '',
	"btnclr1" => '',
	"hvrbg1" => '',
	"hvrclr1" => '',
	//btn 2
	"link2" => '',
	"btnsize2" => '',
	"bgtype2" => '',
	"btnbg2" => '',
	"btnclr2" => '',
	"hvrbg2" => '',
	"hvrclr2" => '',
    "btnanim" => '',
    "btndelay" => '',
	//payments img
	"paymentss" => '',
    "payanim" => '',
    "paydelay" => '',
  	//title style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	//subtitle
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	//text 1
	"p1clr" => '',
	"p1sz" => '',
	"p1lh" => '',
	//text 2
	"p2clr" => '',
	"p2sz" => '',
	"p2lh" => '',
	//timer label
	"lclr" => '',
	"lsz" => '',
	"llh" => '',
	//css
	"css" => '',
	), $atts) );

	//2018, 11, 5, 0, 0, 0, 0
	$year = $year != '' ? esc_attr($year).', ' : '2019 ';
	$month = $month != '' ? esc_attr($month).', ' : '11 ';
	$day = $day != '' ? esc_attr($day).', ' : '5, ';
	$hours = $hours != '' ? esc_attr($hours).', ' : '0, ';
	$min = $min != '' ? esc_attr($min).', ' : '0, ';
	$sec = $sec != '' ? esc_attr($sec).', ' : '0, ';

	$msec =  '0';
	$time = $year.$month.$day.$hours.$min.$sec.$msec;

	$lclr = $lclr != '' ? ' color:'.$lclr.';' : '';
	$lsz = $lsz != '' ? ' font-size:'.esc_attr($lsz).';' : '';
	$llh = $llh != '' ? ' line-height:'.esc_attr($llh).';' : '';
	$labelstyle = $lclr != '' || $lsz != '' || $llh != ''? ' style="'.$lclr.$lsz.$llh.'"' : '';
	$thintitle = $thintitle != '' ? ' <span>'.esc_html($thintitle).'</span>' : '';

	$out = '';

		$out .= '<div id="first-screen" class="promo promo4 '.cryptoland_vc_custom_css_class($css).'">';

					$titledelay = $titledelay != '' ? ' data-aos-delay="'.esc_attr($titledelay).'"' : '';
					$titleanim = $titleanim != '' ? ' data-aos="'.esc_attr($titleanim).'" data-aos-anchor="#first-screen" '.$titledelay.'' : '';
					//title
					$out .= cryptoland_element( $title.$thintitle, $tag='h1', $class='', $tclr, $tsz, $tlh, $titleanim );
					//desc
					$out .= cryptoland_element( $desc, $tag='p', $class='', $pclr, $psz, $plh, $titleanim );

					$hidelabelclass = $hidelabel != false ? ' hiding-label' : '';
					$out .= '<div class="timer-wrap timer_wrap3'.$hidelabelclass.'">';
						//timer
						if( $hidetimer != true ){
						$timerdelay = $timerdelay != '' ? ' data-aos-delay="'.esc_attr($timerdelay).'"' : '';
						$timeranim = $timeranim != '' ? ' data-aos="'.esc_attr($timeranim).'" data-aos-anchor="#first-screen" '.$timerdelay.'' : '';
						$out .= '<div class="timer timer3" id="timer" data-date="'.$time.'"'.$timeranim.'></div>';
						}
						if( $hidetimer != true AND $hidelabel != true ){
						$out .= '<div class="timer__titles"'.$labelstyle.'>';
							$dlabel = $dlabel != '' ? esc_attr($dlabel) : 'Days';
							$hlabel = $hlabel != '' ? esc_attr($hlabel) : 'Hours';
							$minlabel = $minlabel != '' ? esc_attr($minlabel) : 'Minutes';
							$seclabel = $seclabel != '' ? esc_attr($seclabel) : 'Seconds';
							$out .= '<div>'.esc_html($dlabel).'</div>';
							$out .= '<div>'.esc_html($hlabel).'</div>';
							$out .= '<div>'.esc_html($minlabel).'</div>';
							$out .= '<div>'.esc_html($seclabel).'</div>';
						$out .= '</div>';
						}
					$out .= '</div>';
					$textdelay = $textdelay != '' ? ' data-aos-delay="'.esc_attr($textdelay).'"' : '';
					$textanim = $textanim != '' ? ' data-aos="'.esc_attr($textanim).'" data-aos-anchor="#first-screen" '.$textdelay.'' : '';
					$out .= '<div data-aos="fade-up" data-aos-anchor="#first-screen" data-aos-delay="200" class="promo__text-wrap">';
						//text
						$out .= cryptoland_element( $text1, $tag='p', $class='promo__text-style-1', $p1clr, $p1sz, $p1lh, $textanim );
						//text
						$out .= cryptoland_element( $text2, $tag='p', $class='promo__text-style-2', $p1clr, $p1sz, $p1lh, $textanim );

					$out .= '</div>';

					// buttons
					$btndelay = $btndelay != '' ? ' data-aos-delay="'.esc_attr($btndelay).'"' : '';
					$btnanim = $btnanim != '' ? ' data-aos="'.esc_attr($btnanim).'" data-aos-anchor="#first-screen" '.$btndelay.'' : '';
					$out .= '<div class="promo__btns-wrap"'.$btnanim.'>';

					//bottom button 1
					$out .= cryptoland_btn( $link1,$class='btn btn--red '.$btnsize1,$bgtype1,$btnbg1,$btnclr1,$hvrbg1,$hvrclr1,$innerel='span',$icon='' );
					//bottom button 2
					$out .= cryptoland_btn( $link2,$class='btn btn--blue '.$btnsize2,$bgtype2,$btnbg2,$btnclr2,$hvrbg2,$hvrclr2,$innerel='',$icon='' );

					$out .= '</div>';


					if ( ! empty($paymentss) ){
					$paydelay = $paydelay != '' ? ' data-aos-delay="'.esc_attr($paydelay).'"' : '';
					$payanim = $payanim != '' ? ' data-aos="'.esc_attr($payanim).'" data-aos-anchor="#first-screen" '.$paydelay.'' : '';
					$out .= '<div class="payments"'.$payanim.'>';

							$image_ids = explode(',',$paymentss);
							foreach( $image_ids as $image_id ){
							$images = wp_get_attachment_image_src( $image_id, 'company_logo' );
							$out .= '<img src="'.$images[0].'" alt="'.$atts['title'].'">';
							$images++;
							}

					$out .= '</div>';
					}
			if ( $scrollimg != '' ){
			$out .= '<div class="scroll-down">';
				$out .= cryptoland_img( $scrollimg, $imgclass='', $imgwidth='', $imgheight='', $data='' );
			$out .= '</div>';
			}
		$out .= '</div>';

	return $out;
}
add_shortcode('cryptoland_home4_hero_shortcode', 'cryptoland_home4_hero');



/*-----------------------------------------------------------------------------------*/
/*	HERO HOME 5
/*-----------------------------------------------------------------------------------*/

function cryptoland_home5_hero( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "title" => '',
    "thintitle" => '',
    "desc" => '',
    "titleanim" => '',
    "titledelay" => '',
    "descanim" => '',
    "descdelay" => '',
	//timer
    "hidetimer" => '',
    "hidelabel" => '',
    "dlabel" => '',
    "hlabel" => '',
    "minlabel" => '',
    "seclabel" => '',
    "month" => '',
    "day" => '',
    "year" => '',
    "hours" => '',
    "min" => '',
    "sec" => '',
    "circlebg" => '',
    "dclr" => '',
    "hclr" => '',
    "minclr" => '',
    "secclr" => '',
    "timeranim" => '',
    "timerdelay" => '',
	//btn 1
	"link1" => '',
	"btnsize1" => '',
	"bgtype1" => '',
	"btnbg1" => '',
	"btnclr1" => '',
	"hvrbg1" => '',
	"hvrclr1" => '',
	//btn 2
	"link2" => '',
	"btnsize2" => '',
	"bgtype2" => '',
	"btnbg2" => '',
	"btnclr2" => '',
	"hvrbg2" => '',
	"hvrclr2" => '',
    "btnanim" => '',
    "btndelay" => '',
	//payments img
	"paymentss" => '',
    "payanim" => '',
    "paydelay" => '',
  	//title style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	//desc style
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	//timer label
	"lclr" => '',
	"lsz" => '',
	"llh" => '',
	//css
	"bgimg1" => '',
	"bgimg2" => '',
	"parallax1" => '',
	"parallax2" => '',
	"parallax3" => '',
	), $atts) );


	$lclr = $lclr != '' ? ' color:'.$lclr.';' : '';
	$lsz = $lsz != '' ? ' font-size:'.esc_attr($lsz).';' : '';
	$llh = $llh != '' ? ' line-height:'.esc_attr($llh).';' : '';
	$labelstyle = $lclr != '' || $lsz != '' || $llh != ''? ' style="'.$lclr.$lsz.$llh.'"' : '';
	$thintitle = $thintitle != '' ? ' <span>'.esc_html($thintitle).'</span>' : '';
	$parallax1 = wp_get_attachment_url( $parallax1, 'full' );
	$parallax2 = wp_get_attachment_url( $parallax2, 'full' );
	$parallax3 = wp_get_attachment_url( $parallax3, 'full' );

	$out = '';
		$out .= '<div id="first-screen" class="first-screen">';
			//bg and parallax__img
			$out .= ''.cryptoland_img( $bgimg1, $imgclass='first-screen__bg-1', $imgwidth='', $imgheight='', $data='' ).'';
			$out .= ''.cryptoland_img( $bgimg2, $imgclass='first-screen__bg-2', $imgwidth='', $imgheight='', $data='' ).'';
			if( $parallax1 != '' ){$out .= '<img src="'.$parallax1.'" data-jarallax-element="-100" alt="" class="first-screen__round-1">';}
			if( $parallax2 != '' ){$out .= '<img src="'.$parallax2.'" data-jarallax-element="-120" alt="" class="first-screen__round-2">';}
			if( $parallax3 != '' ){$out .= '<img src="'.$parallax3.'" data-jarallax-element="-100" alt="" class="first-screen__round-3">';}
			$out .= '<div class="container">';
				$out .= '<div class="row">';
					$out .= '<div class="col-12">';
						//title
						$titledelay = $titledelay != '' ? ' data-aos-delay="'.esc_attr($titledelay).'"' : '';
						$titleanim = $titleanim != '' ? ' data-aos="'.esc_attr($titleanim).'" data-aos-anchor="#first-screen" '.$titledelay.'' : '';
						$out .= cryptoland_element( $title.$thintitle, $tag='h1', $class='', $tclr, $tsz, $tlh, $titleanim );
						//desc
						$descdelay = $descdelay != '' ? ' data-aos-delay="'.esc_attr($descdelay).'"' : '';
						$descanim = $descanim != '' ? ' data-aos="'.esc_attr($descanim).'" data-aos-anchor="#first-screen" '.$descdelay.'' : '';
						$out .= cryptoland_element( $desc, $tag='p', $class='', $pclr, $psz, $plh, $descanim );
						//circle timer
						if( $hidetimer != true ){
							$timerdelay = $timerdelay != '' ? ' data-aos-delay="'.esc_attr($timerdelay).'"' : '';
							$timeranim = $timeranim != '' ? ' data-aos="'.esc_attr($timeranim).'" data-aos-anchor="#first-screen" '.$timerdelay.'' : '';
							$dlabel = $dlabel != '' ? esc_attr($dlabel) : 'Days';
							$dclr = $dclr != '' ? esc_attr($dclr) : '#e2e7f8';
							$hlabel = $hlabel != '' ? esc_attr($hlabel) : 'Hours';
							$hclr = $hclr != '' ? esc_attr($hclr) : '#e2e7f8';
							$minlabel = $minlabel != '' ? esc_attr($minlabel) : 'Minutes';
							$minclr = $minclr != '' ? esc_attr($minclr) : '#e2e7f8';
							$seclabel = $seclabel != '' ? esc_attr($seclabel) : 'Seconds';
							$secclr = $secclr != '' ? esc_attr($secclr) : '#e2e7f8';
							$timerlabel = ' data-label="'.$dlabel.','.$hlabel.','.$minlabel.','.$seclabel.'"';
							$labelclr = ' data-progresclr="'.$dclr.','.$hclr.','.$minclr.','.$secclr.'"';
							$circlebg = $circlebg != '' ? ' data-circlebg="'.esc_attr($circlebg).'"' : ' data-circlebg="#5382f7"';

							//2019-06-15 00:00:00"
							$year = $year != '' ? esc_attr($year) : '2018';
							$month = $month != '' ? esc_attr($month) : '06';
							$day = $day != '' ? esc_attr($day) : '15';
							$hours = $hours != '' ? esc_attr($hours) : '00';
							$min = $min != '' ? esc_attr($min) : '00';
							$sec = $sec != '' ? esc_attr($sec) : '00';
							$time = ' data-date="'.$year.'-'.$month.'-'.$day.' '.$hours.':'.$min.':'.$sec.'"';

							$out .= '<div class="TimeCircles js-TimeCircles"'.$time.$timerlabel.$labelclr.$circlebg.$timeranim.$labelstyle.'></div>';
						}
						// buttons
						$btndelay = $btndelay != '' ? ' data-aos-delay="'.esc_attr($btndelay).'"' : '';
						$btnanim = $btnanim != '' ? ' data-aos="'.esc_attr($btnanim).'" data-aos-anchor="#first-screen" '.$btndelay.'' : '';
						$out .= '<div class="first-screen__btns-wrap"'.$btnanim.'>';

						//bottom button 1
						$out .= cryptoland_btn( $link1,$class='btn btn--orange '.$btnsize1,$bgtype1,$btnbg1,$btnclr1,$hvrbg1,$hvrclr1,$innerel='span',$icon='' );
						//bottom button 2
						$out .= cryptoland_btn( $link2,$class='btn btn--blue '.$btnsize2,$bgtype2,$btnbg2,$btnclr2,$hvrbg2,$hvrclr2,$innerel='',$icon='' );

						$out .= '</div>';
						//payments image
						if ( ! empty($paymentss) ){
						$paydelay = $paydelay != '' ? ' data-aos-delay="'.esc_attr($paydelay).'"' : '';
						$payanim = $payanim != '' ? ' data-aos="'.esc_attr($payanim).'" data-aos-anchor="#first-screen" '.$paydelay.'' : '';
						$out .= '<div class="payments"'.$payanim.'>';
								$image_ids = explode(',',$paymentss);
								foreach( $image_ids as $image_id ){
								$images = wp_get_attachment_image_src( $image_id, 'full' );
								$out .= '<img src="'.$images[0].'" alt="'.$atts['title'].'">';
								$images++;
								}
						$out .= '</div>';
						}
					$out .= '</div>';
				$out .= '</div>';
			$out .= '</div>';
		$out .= '</div>';

	return $out;
}
add_shortcode('cryptoland_home5_hero_shortcode', 'cryptoland_home5_hero');


/*-----------------------------------------------------------------------------------*/
/*	HERO HOME 6
/*-----------------------------------------------------------------------------------*/

function cryptoland_home6_hero( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "title" => '',
    "thintitle" => '',
    "desc" => '',
    "titleanim" => '',
    "titledelay" => '',
    "descanim" => '',
    "descdelay" => '',
	//icobar
    "bgclr" => '',
    "barbgtype" => '',
    "barbgclr" => '',
    "barbgclr1" => '',
    "barbgclr2" => '',
    "barclrtype" => '',
    "barclr" => '',
    "barclr1" => '',
    "barclr2" => '',
    "barwidth" => '',
	//loop bar marker
    "icobar" => '',
    "markertitle" => '',
    "markervalue" => '',
    "maxtitle" => '',
    "maxvalue" => '',
    "text1" => '',
    "text2" => '',
    "icoanim" => '',
    "icodelay" => '',
	//btn 1
	"link1" => '',
	"btnsize1" => '',
	"bgtype1" => '',
	"btnbg1" => '',
	"btnclr1" => '',
	"hvrbg1" => '',
	"hvrclr1" => '',
	//btn 2
	"link2" => '',
	"btnsize2" => '',
	"bgtype2" => '',
	"btnbg2" => '',
	"btnclr2" => '',
	"hvrbg2" => '',
	"hvrclr2" => '',
    "btnanim" => '',
    "btndelay" => '',
	//payments img
	"payments" => '',
    "payanim" => '',
    "paydelay" => '',
  	//title style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	//desc style
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	//max title
	"mtclr" => '',
	"mtsz" => '',
	"mtlh" => '',
	//max title
	"mxtclr" => '',
	"mxtsz" => '',
	"mxtlh" => '',
	//ico text
	"itclr" => '',
	"itsz" => '',
	"itlh" => '',
	//css
	"css" => '',
	), $atts) );

	$thintitle = $thintitle != '' ? ' <span>'.esc_html($thintitle).'</span>' : '';


	$out = '';
		$out .= '<div id="first-screen" class="first-screen section section--no-pad-top '.cryptoland_vc_custom_css_class($css).'">';

			//title
			$titledelay = $titledelay != '' ? ' data-aos-delay="'.esc_attr($titledelay).'"' : '';
			$titleanim = $titleanim != '' ? ' data-aos="'.esc_attr($titleanim).'" data-aos-anchor="#first-screen" '.$titledelay.'' : '';
			$out .= cryptoland_element( $title.$thintitle, $tag='h1', $class='', $tclr, $tsz, $tlh, $titleanim );
			//desc
			$descdelay = $descdelay != '' ? ' data-aos-delay="'.esc_attr($descdelay).'"' : '';
			$descanim = $descanim != '' ? ' data-aos="'.esc_attr($descanim).'" data-aos-anchor="#first-screen" '.$descdelay.'' : '';
			$out .= cryptoland_element( $desc, $tag='p', $class='', $pclr, $psz, $plh, $descanim );

			//icobar
			//animation
			$icodelay = $icodelay != '' ? ' data-aos-delay="'.esc_attr($icodelay).'"' : '';
			$icoanim = $icoanim != ''? ' data-aos="'.esc_attr($icoanim).'" data-aos-anchor="#first-screen"'.$icodelay : '';

			//bar bg color
			$bgclr = $bgclr != '' ? ' style="background-color:'.$bgclr.';"' : '';
			$barbgclr = $barbgclr != '' ? $barbgclr : 'rgba(2,21,39,.2)';
			$barbgclr1 = $barbgclr1 != '' ? $barbgclr1 : '#9c6ea9';
			$barbgclr2 = $barbgclr2 != '' ? $barbgclr2 : '#f2949e';

			//bar progress bg color
			$barbgtype = $barbgtype == 'grad' ? ' style="background: -webkit-gradient(linear,left top,right top,color-stop(-2.49%,'.$barbgclr1.'),to('.$barbgclr1.'));
			background: -webkit-linear-gradient(left,'.$barbgclr1.' -2.49%,'.$barbgclr2.' 100%);
			background: -o-linear-gradient(left,'.$barbgclr1.' -2.49%,'.$barbgclr2.' 100%);
			background: linear-gradient(90deg,'.$barbgclr1.' -2.49%,'.$barbgclr2.' 100%);"' : ' style="background-color:'.$barbgclr.';"';

			$barwidth = $barwidth != '' ? $barwidth : '50%';
			$barclr = $barclr != '' ? $barclr : '#9c6ea9';
			$barclr1 = $barclr1 != '' ? $barclr1 : '#9c6ea9';
			$barclr2 = $barclr2 != '' ? $barclr2 : '#f2949e';

			$barclrtype = $barclrtype == 'grad' ? ' style="width:'.$barwidth.';background: -webkit-gradient(linear,left top,right top,color-stop(-2.49%,'.$barclr1.'),to('.$barclr2.'));
			background: -webkit-linear-gradient(left,'.$barclr1.' -2.49%,'.$barclr2.' 100%);
			background: -o-linear-gradient(left,'.$barclr1.' -2.49%,'.$barclr2.' 100%);
			background: linear-gradient(90deg,'.$barclr1.' -2.49%,'.$barclr2.' 100%);"' : ' style="width:'.$barwidth.';background-color:'.$barclr.';"';


			$out .= '<div class="progress-bar"'.$icoanim.'>';
				$out .= '<div class="progress-bar__line"'.$barbgtype.'>';
					$out .= '<span'.$barclrtype.'></span>';
					// max value
					$out .= cryptoland_element( $maxtitle, $tag='P', $class='', $mxtclr, $mxtsz, $mxtlh, $anim='' );
					//loop icobar item
					$loop = (array) vc_param_group_parse_atts($icobar);
					foreach ( $loop as $item ) {
						if (isset($maxvalue) != '' && isset($item['markervalue']) != ''){
						//calculate marker position on bar
						$position = floor( ( $item['markervalue'] / $maxvalue ) * 100);
						$markerafter = $position <= $barwidth ? ' has_after' : '';
						//marker typography
						$mtclr = $mtclr != '' ? ' color:'.esc_attr($mtclr).';' : '';
						$mtsz = $mtsz != '' ? ' font-size:'.esc_attr($mtsz).';' : '';
						$mxtlh = $mxtlh != '' ? ' line-height:'.esc_attr($mxtlh).';' : '';
						$mstyle = $mtclr != '' || $mtsz != '' || $mxtlh != '' || $position != '' ? ' style="left:'.$position.'%;'.$mtclr.$mtsz.$mxtlh.'"' : ' style="left:'.$position.'%;"';
						//bar item

						$markertitle = isset($item['markertitle']) != '' ? $item['markertitle'] : $item['markervalue'];
						$out .= '<div class="progress-bar__metka"'.$mstyle.'>'.$markertitle.'</div>';

						}
					}
				$out .= '</div>';
				$out .= '<div class="progress-bar__footer">';
					// text1
					$out .= cryptoland_element( $text1, $tag='P', $class='', $itclr, $itsz, $itlh, $anim='' );
					// text2
					$out .= cryptoland_element( $text2, $tag='P', $class='', $itclr, $itsz, $itlh, $anim='' );
				$out .= '</div>';
			$out .= '</div>';

			// buttons
			$btndelay = $btndelay != '' ? ' data-aos-delay="'.esc_attr($btndelay).'"' : '';
			$btnanim = $btnanim != '' ? ' data-aos="'.esc_attr($btnanim).'" data-aos-anchor="#first-screen" '.$btndelay.'' : '';
			$out .= '<div class="first-screen__btns-wrap"'.$btnanim.'>';

			//bottom button 1
			$out .= cryptoland_btn( $link1,$class='btn btn--orange '.$btnsize1,$bgtype1,$btnbg1,$btnclr1,$hvrbg1,$hvrclr1,$innerel='span',$icon='' );
			//bottom button 2
			$out .= cryptoland_btn( $link2,$class='btn btn--blue '.$btnsize2,$bgtype2,$btnbg2,$btnclr2,$hvrbg2,$hvrclr2,$innerel='',$icon='' );

			$out .= '</div>';
			//payments image
			if ( ! empty($payments) ){
			$paydelay = $paydelay != '' ? ' data-aos-delay="'.esc_attr($paydelay).'"' : '';
			$payanim = $payanim != '' ? ' data-aos="'.esc_attr($payanim).'" data-aos-anchor="#first-screen" '.$paydelay.'' : '';
			$out .= '<div class="payments"'.$payanim.'>';
					$image_ids = explode(',',$payments);
					foreach( $image_ids as $image_id ){
					$images = wp_get_attachment_image_src( $image_id, 'full' );
					$out .= '<img src="'.$images[0].'" alt="'.$atts['title'].'">';
					$images++;
					}
			$out .= '</div>';
			}

		$out .= '</div>';

	return $out;
}
add_shortcode('cryptoland_home6_hero_shortcode', 'cryptoland_home6_hero');


/*-----------------------------------------------------------------------------------*/
/*	HOME 3 ECONOMY SECTION
/*-----------------------------------------------------------------------------------*/

function cryptoland_home3_economy( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "subtitle" => '',
    "title" => '',
    "desc" => '',
    "bgimg" => '',
	//video
    "vbtnimg" => '',
    "vurl" => '',
	//list
    "list" => '',
    "listitem" => '',
  	//subtitle style
	"stclr" => '',
	"stsz" => '',
	"stlh" => '',
  	//title style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	//desc style
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	//desc style
	"lclr" => '',
	"lsz" => '',
	"llh" => '',
  	//custom style
	"contentcss"=> '',
	), $atts) );

	$loop = (array) vc_param_group_parse_atts($list);
	$out = '';

		$out .= '<div class="economy">';
			if ( $vurl != '' ) {
			$out .= '<a data-jarallax-element="-40" href="'.$vurl.'" class="economy__video-btn video-btn popup-youtube">';
				$out .= ''.cryptoland_img( $vbtnimg, $imgclass='', $imgwidth='', $imgheight='', $data='' ).'';
			$out .= '</a>';
			}
			$out .= '<div class="economy__block '.cryptoland_vc_custom_css_class($contentcss).'">';
				$out .= '<div class="economy__block-content">';
					$out .= '<div class="section-header section-header--white section-header--tire section-header--small-margin">';
						//subtitle
						$out .= cryptoland_element( $subtitle, $tag='h4', $class='', $stclr, $stsz, $stlh, $anim='' );
						//title
						$out .= cryptoland_element( $title, $tag='h2', $class='', $tclr, $tsz, $tlh, $anim='' );
					$out .= '</div>';
					$out .= cryptoland_element( $desc, $tag='p', $class='', $pclr, $psz, $plh, $anim='' );
					if ( $loop ) {
					$out .= '<ul>';
						foreach ( $loop as $item ) {
						if ( isset( $item['listitem'] ) != '' ) {
						$out .= cryptoland_element( $item['listitem'], $tag='li', $class='', $lclr, $lsz, $llh, $anim='' );
						}
						}
					$out .= '</ul>';
					}
				$out .= '</div>';
			$out .= '</div>';
		$out .= '</div>';

	return $out;
}
add_shortcode('cryptoland_home3_economy_shortcode', 'cryptoland_home3_economy');

/*-----------------------------------------------------------------------------------*/
/*	PARTNERS
/*-----------------------------------------------------------------------------------*/

function cryptoland_partners( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "partners" => '',
    "img" => '',
  	//custom style
	"css" => '',
	), $atts) );

	$loop = (array) vc_param_group_parse_atts($partners);
	$out = '';
	$out .= '<div class="partners partners--top-animation '.cryptoland_vc_custom_css_class($css).'">';
		$out .= '<div class="partners__logoes">';
		foreach ( $loop as $item ) {
			if ( isset( $item['img'] ) != '' ) {
			$out .= '<div class="partners__logo">';
				$out .= ''.cryptoland_img( $item['img'], $imgclass='', $imgwidth='', $imgheight='', $data='' ).'';
			$out .= '</div>';
			}
		}
		$out .= '</div>';
	$out .= '</div>';
	return $out;
}
add_shortcode('cryptoland_partners_shortcode', 'cryptoland_partners');

/*-----------------------------------------------------------------------------------*/
/*	ROAD MAP HOME 1
/*-----------------------------------------------------------------------------------*/

function cryptoland_roadmap1( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "map" => '',
    "date" => '',
    "desc" => '',
    "active" => '',
  	//custom style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	"css" => '',
	), $atts) );

	$loop = (array) vc_param_group_parse_atts($map);
	$out = '';

	$out .= '<div class="row">';
		$out .= '<div class="col-lg-10 offset-lg-1">';
			$out .= '<div class="road road-2 '.cryptoland_vc_custom_css_class($css).'">';
			foreach ( $loop as $item ) {
				$activei = isset($item['active']) == true ? ' road__item-active' : '';
				$out .= '<div class="road__item'.$activei.'">';
					$out .= '<div class="road__item-metka"></div>';
					$out .= '<div class="road__item-content">';
						$out .= cryptoland_element( $item['date'], $tag='div', $class='road__item-title', $tclr, $tsz, $tlh, $anim='' );
						$out .= cryptoland_element( $item['desc'], $tag='p', $class='', $pclr, $psz, $plh, $anim='' );
					$out .= '</div>';
				$out .= '</div>';
			}
			$out .= '</div>';
		$out .= '</div>';
	$out .= '</div>';

	return $out;
}
add_shortcode('cryptoland_roadmap1_shortcode', 'cryptoland_roadmap1');

/*-----------------------------------------------------------------------------------*/
/*	ROAD MAP HOME 2-3
/*-----------------------------------------------------------------------------------*/

function cryptoland_roadmap( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "map" => '',
    "date" => '',
    "desc" => '',
    "active" => '',
  	//custom style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	"css" => '',
	), $atts) );

	$loop = (array) vc_param_group_parse_atts($map);
	$out = '';

	$out .= '<div class="row">';
		$out .= '<div class="col-lg-6 offset-lg-4 col-sm-8 offset-sm-4">';
			$out .= '<div class="road road-2 '.cryptoland_vc_custom_css_class($css).'">';
			foreach ( $loop as $item ) {
				$activei = isset($item['active']) == true ? ' road__item-active' : '';
				$out .= '<div class="road__item'.$activei.'">';
					$out .= '<div class="road__item-metka"></div>';
					$out .= '<div class="road__item-content">';
						$out .= cryptoland_element( $item['date'], $tag='div', $class='road__item-title', $tclr, $tsz, $tlh, $anim='' );
						$out .= cryptoland_element( $item['desc'], $tag='p', $class='', $pclr, $psz, $plh, $anim='' );
					$out .= '</div>';
				$out .= '</div>';
			}
			$out .= '</div>';
		$out .= '</div>';
	$out .= '</div>';

	return $out;
}
add_shortcode('cryptoland_roadmap_shortcode', 'cryptoland_roadmap');


/*-----------------------------------------------------------------------------------*/
/*	ROAD MAP HOME 4
/*-----------------------------------------------------------------------------------*/
function cryptoland_roadmap4( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "row1" => '',
    "title1" => '',
    "date1" => '',
    "desc1" => '',
    "anim" => '',
  	//custom style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	"dtclr" => '',
	"dtsz" => '',
	"dtlh" => '',
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	"css" => '',
	), $atts) );

	$anim = $anim != '' ? ' data-aos="'.esc_attr($anim).'"' : '';
	$loop1 = (array) vc_param_group_parse_atts($row1);

	$out = '';
		$out .= '<div class="roadmap__content '.cryptoland_vc_custom_css_class($css).'"'.$anim .'>';
		$countitem = 1;
		foreach ( $loop1 as $item ) {
			if ($countitem % 3 == 1) {
			$out .= '<div class="roadmap__row">';
			}
				$out .= '<div class="roadmap__item">';
					$out .= cryptoland_element( $item['title1'], $tag='div', $class='roadmap__title', $tclr, $tsz, $tlh, $anim='' );
					$out .= cryptoland_element( $item['date1'], $tag='div', $class='roadmap__data', $tclr, $tsz, $tlh, $anim='' );
					$out .= cryptoland_element( $item['desc1'], $tag='p', $class='', $pclr, $psz, $plh, $anim='' );
				$out .= '</div>';
			if ($countitem % 3 == 0) {
			$out .= '</div>';
			}
			$countitem++;
		}

		$out .= '</div>';

	return $out;
}
add_shortcode('cryptoland_roadmap4_shortcode', 'cryptoland_roadmap4');

/*-----------------------------------------------------------------------------------*/
/*	ROAD MAP HOME 5-6
/*-----------------------------------------------------------------------------------*/

function cryptoland_roadmap56( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "map" => '',
    "date" => '',
    "desc" => '',
    "active" => '',
    "anim" => '',
  	//custom style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	"css" => '',
	), $atts) );
	$anim = $anim != '' ? ' data-aos="'.esc_attr($anim).'"' : '';
	$loop = (array) vc_param_group_parse_atts($map);
	$out = '';

	$out .= '<div class="roadmap roadmap-5 '.cryptoland_vc_custom_css_class($css).'"'.$anim.'>';
	$count = 1;
	foreach ( $loop as $item ) {

		$activei = isset($item['active']) == true ? ' roadmap__item--active' : '';
		if($count == 1){
		$out .= '<div class="roadmap__item roadmap__item--past">';
		}else{
		$out .= '<div class="roadmap__item'.$activei.'">';
		}
				$out .= cryptoland_element( $item['date'], $tag='div', $class='roadmap__item-title', $tclr, $tsz, $tlh, $anim='' );
				$out .= '<div class="roadmap__item-marker"></div>';
				$out .= cryptoland_element( $item['desc'], $tag='div', $class='roadmap__item-text', $pclr, $psz, $plh, $anim='' );
			$out .= '</div>';
			$count++;
	}
	$out .= '</div>';

	return $out;
}
add_shortcode('cryptoland_roadmap56_shortcode', 'cryptoland_roadmap56');



/*-----------------------------------------------------------------------------------*/
/*	PROCESS HOME 1-5
/*-----------------------------------------------------------------------------------*/

function cryptoland_process( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "process" => '',
    "img" => '',
    "line" => '',
    "title" => '',
    "desc" => '',
    "anim" => '',
    "delay" => '',
  	//custom style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	), $atts) );


	$loop = (array) vc_param_group_parse_atts($process);
	$out = '';
	$line  = wp_get_attachment_url( $line,'full' );
	$out .= '<div class="process__steps process-1-5">';
		foreach ( $loop as $item ) {
			$delay = isset( $item['delay'] ) != '' ? ' data-aos-delay="'.$item['delay'].'"' : '';
			$anim = isset( $item['anim'] ) != '' ? ' data-aos="'.$item['anim'].'"'.$delay : '';

		$out .= '<div class="process__step"'.$anim.'>';
			if ( isset( $item['img'] ) != '' ) {
			$out .= '<div class="process__step-icon">';
				$out .= ''.cryptoland_img( $item['img'], $imgclass='', $imgwidth='', $imgheight='', $data='' ).'';
			$out .= '</div>';
			}
			if ( isset( $item['title'] ) != '' ) {
			$out .= cryptoland_element( $item['title'], $tag='h3', $class='process__step-title', $tclr, $tsz, $tlh, $anim='' );
			}
			if ( isset( $item['desc'] ) != '' ) {
			$out .= cryptoland_element( $item['desc'], $tag='p', $class='', $tclr, $tsz, $tlh, $anim='' );
			}
		$out .= '</div>';
		}
	$out .= '</div>';


	return $out;
}
add_shortcode('cryptoland_process_shortcode', 'cryptoland_process');

/*-----------------------------------------------------------------------------------*/
/*	PROCESS HOME-6
/*-----------------------------------------------------------------------------------*/

function cryptoland_process6( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "img" => '',
    "arrow" => '',
    "arrowbg" => '',
    "moboff" => '',
    "line" => '',
    "title" => '',
    "desc" => '',
    "brd" => '',
    "brdtype" => '',
    "brdclr" => '',
    "addanim" => '',
    "anim" => '',
    "delay" => '',
  	//title style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	//desc style
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	), $atts) );


	$delay = $delay != '' ? ' data-aos-delay="'.$delay .'"': '';
	$anim = $addanim == true && $anim != '' ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';

	$brd = $brd != '' ? $brd : '3px';
	$brdtype = $brdtype != '' ? $brdtype : 'solid';
	$brdclr = $brdclr != '' ? $brdclr : '#16bf86';
	$arrowbg = $arrowbg != '' ? ' style="background-color: '.$arrowbg .';"' : '';
	$moboff = $moboff == true ? ' mob-off' : '';
	$out = '';

	$out .= '<div class="process-item" style="border: '.$brd.' '.$brdtype.' '.$brdclr.';"'.$anim.' >';
		$out .= '<div class="process-item__icon">';
			$out .= cryptoland_img( $img, $imgclass='', $imgwidth='', $imgheight='', $data='' );
		$out .= '</div>';
		$out .= cryptoland_element( $title, $tag='div', $class='process-item__title', $tclr, $tsz, $tlh, $anim='' );
		$out .= cryptoland_element( $desc, $tag='p', $class='', $pclr, $psz, $plh, $anim='' );
		$out .= '<div class="process-item__arrow'.$moboff.'"'.$arrowbg.'>';
			$out .= cryptoland_img( $arrow, $imgclass='', $imgwidth='', $imgheight='', $data='' );
		$out .= '</div>';
	$out .= '</div>';

	return $out;
}
add_shortcode('cryptoland_process6_shortcode', 'cryptoland_process6');


/*-----------------------------------------------------------------------------------*/
/*	FACTS HOME 3
/*-----------------------------------------------------------------------------------*/

function cryptoland_facts3( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "bgimg" => '',
    "process" => '',
    "img" => '',
    "title" => '',
  	//custom style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	), $atts) );

	$loop = (array) vc_param_group_parse_atts($process);
	$out = '';
		$out .= '<div class="facts__line">';
			$out .= '<div class="facts__line-wrapper">';
				$out .= '<div class="facts__line-list">';
				foreach ( $loop as $item ) {
					$out .= '<div class="facts__item">';
						$out .= cryptoland_img( $item['img'], $imgclass='facts__icon', $imgwidth='', $imgheight='', $data='' );
						$out .= cryptoland_element( $item['title'], $tag='div', $class='facts__title', $tclr, $tsz, $tlh, $anim='' );
					$out .= '</div>';
				}
				$out .= '</div>';
			$out .= '</div>';
			$out .= cryptoland_img( $bgimg, $imgclass='facts__bg', $imgwidth='', $imgheight='', $data='' );
		$out .= '</div>';
	return $out;
}
add_shortcode('cryptoland_facts3_shortcode', 'cryptoland_facts3');

/*-----------------------------------------------------------------------------------*/
/*	COUNTER
/*-----------------------------------------------------------------------------------*/

function cryptoland_counter( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "number" => '',
    "percentage" => '',
    "align" => '',
  	//custom style
	"nclr" => '',
	"nsz" => '',
	"nlh" => '',
	), $atts) );

	$align = $align != '' ? $align : '';
	$nclr = $nclr != '' ? 'color:'.$nclr : '';
	$nsz = $nsz != '' ? 'font-size:'.$nsz : '';
	$nlh = $nlh != '' ? 'line-height:'.$nlh : '';
	$percentage = $percentage != '' ? '<span class="percentage">'.esc_attr($percentage).'</span>' : '';
	$style = $nclr != '' || $nsz != '' || $nlh ? ' style="'.$nclr.$nsz.$nlh.'"' : '';

	$out = '';
	$out .='<div class="counter__item-value numscroller '.$align.'"'.$style.'>';
		$out .='<div class="counter">'.esc_html($number).'</div>';
		$out .=$percentage;
	$out .='</div>';
	return $out;
}
add_shortcode('cryptoland_counter_shortcode', 'cryptoland_counter');


/*-----------------------------------------------------------------------------------*/
/*	COUNT NUMBER HOME 3
/*-----------------------------------------------------------------------------------*/
function cryptoland_counter3( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"dataimg" => '',
	"databg" => '',
	"title1" => '',
	"num1" => '',
	"per1" => '',
	"title2" => '',
	"num2" => '',
	"per2" => '',
	"title3" => '',
	"num3" => '',
	"per3" => '',
	"title4" => '',
	"num4" => '',
	"per4" => '',
	//custom style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	"nclr" => '',
	"nsz" => '',
	"nlh" => '',
	), $atts) );

	$per1 = $per1 != '' ? ' " data-percantage="'.$per1.'' : '';
	$per2 = $per2 != '' ? ' " data-percantage="'.$per2.'' : '';
	$per3 = $per3 != '' ? ' " data-percantage="'.$per3.'' : '';
	$per4 = $per4 != '' ? ' " data-percantage="'.$per4.'' : '';
	$out = '';

	$out .= '<div class="data" id="stat">';
		$out .= '<div class="container data__container">';
			$out .= '<div class="row">';
				$out .= '<div class="col">';
					$out .= ''.cryptoland_img( $dataimg, $imgclass='data__img', $imgwidth='', $imgheight='', $data='' ).'';
					$out .= '<div class="counter__item counter__item-1"'.$per1.'>';
						$out .= cryptoland_element( $title1, $tag='div', $class='counter__item-title', $tclr, $tsz, $tlh, $anim='' );
						$out .= cryptoland_element( $num1, $tag='div', $class='counter counter__item-value numscroller'.$per1.'', $nclr, $nsz, $nlh, $anim='' );
					$out .= '</div>';
					$out .= '<div class="counter__item counter__item-2">';
						$out .= cryptoland_element( $title2, $tag='div', $class='counter__item-title', $tclr, $tsz, $tlh, $anim='' );
						$out .= cryptoland_element( $num2, $tag='div', $class='counter counter__item-value numscroller'.$per2.'', $nclr, $nsz, $nlh, $anim='' );
					$out .= '</div>';
					$out .= '<div class="counter__item counter__item-3">';
						$out .= cryptoland_element( $title3, $tag='div', $class='counter__item-title', $tclr, $tsz, $tlh, $anim='' );
						$out .= cryptoland_element( $num3, $tag='div', $class='counter counter__item-value numscroller'.$per3.'', $nclr, $nsz, $nlh, $anim='' );
					$out .= '</div>';
					$out .= '<div class="counter__item counter__item-4">';
						$out .= cryptoland_element( $title4, $tag='div', $class='counter__item-title', $tclr, $tsz, $tlh, $anim='' );
						$out .= cryptoland_element( $num4, $tag='div', $class='counter counter__item-value numscroller'.$per4.'', $nclr, $nsz, $nlh, $anim='' );
					$out .= '</div>';
				$out .= '</div>';
			$out .= '</div>';
		$out .= '</div>';
		$out .= ''.cryptoland_img( $databg, $imgclass='data__bg', $imgwidth='', $imgheight='', $data='' ).'';
	$out .= '</div>';

	return $out;
}
add_shortcode('cryptoland_counter3_shortcode', 'cryptoland_counter3');


/*-----------------------------------------------------------------------------------*/
/*	ICOBAR TIMER HOME 1
/*-----------------------------------------------------------------------------------*/

function cryptoland_icobartimer( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "title" => '',
	"barbgimg" => '',
    "bgclr" => '',
    "brdclrtype" => '',
    "brdclr" => '',
    "brdclr1" => '',
    "brdclr2" => '',
    "barclrtype" => '',
    "barclr" => '',
    "barclr1" => '',
    "barclr2" => '',
    "barwidth" => '',
	//loop bar marker
    "icobar" => '',
    "markertitle" => '',
    "markervalue" => '',
    "maxtitle" => '',
    "maxvalue" => '',
	//timer
    "timertitle" => '',
    "month" => '',
    "day" => '',
    "year" => '',
    "hours" => '',
    "min" => '',
    "sec" => '',
	//button
    "btnlink" => '',
    "btnsize" => '',
    "bgtype" => '',
    "btnbg" => '',
    "btnclr" => '',
    "hvrbg" => '',
    "hvrclr" => '',
  	//custom style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	"btclr" => '',
	"btsz" => '',
	"btlh" => '',
	"mxbtclr" => '',
	"mxbtsz" => '',
	"mxbtlh" => '',
	"tmtclr" => '',
	"tmtsz" => '',
	"tmtlh" => '',
	), $atts) );

	$loop = (array) vc_param_group_parse_atts($icobar);
	$barbg  = wp_get_attachment_url( $barbgimg,'full' );

	//December 20, 2018 15:37:25
	$month = $month != '' ? $month : 'December';
	$day = $day != '' ? $day : ' 20,';
	$year = $year != '' ? $year : ' 2020';
	$hours = $hours != '' ? $hours : '15';
	$min = $min != '' ? $min : '37';
	$sec = $sec != '' ? $sec : '25';
	$time = $month.' '.$day.', '.$year.' '.$hours.':'.$min.':'.$sec;
	$out = '';


	$bgclr = $bgclr != '' ? $bgclr : '#fff';
	$brdclr = $brdclr != '' ? $brdclr : '#9c6ea9';
	$brdclr1 = $brdclr1 != '' ? $brdclr1 : '#9c6ea9';
	$brdclr2 = $brdclr2 != '' ? $brdclr2 : '#f2949e';

	$brdclrtype = $brdclrtype != 'one' ? ' style="background: -webkit-gradient(linear,left top,right top,color-stop(-2.49%,'.$brdclr1.'),to('.$brdclr2.'));
    background: -webkit-linear-gradient(left,'.$brdclr1.' -2.49%,'.$brdclr2.' 100%);
    background: -o-linear-gradient(left,'.$brdclr1.' -2.49%,'.$brdclr2.' 100%);
    background: linear-gradient(90deg,'.$brdclr1.' -2.49%,'.$brdclr2.' 100%);"' : ' style="background-color:'.$brdclr.';"';


	$barwidth = $barwidth != '' ? $barwidth : '50%';
	$barclr = $barclr != '' ? $barclr : '#9c6ea9';
	$barclr1 = $barclr1 != '' ? $barclr1 : '#9c6ea9';
	$barclr2 = $barclr2 != '' ? $barclr2 : '#f2949e';

	$barclrtype = $barclrtype != 'one' ? ' style="width:'.$barwidth.';background: -webkit-gradient(linear,left top,right top,color-stop(-2.49%,'.$barclr1.'),to('.$barclr2.'));
    background: -webkit-linear-gradient(left,'.$barclr1.' -2.49%,'.$barclr2.' 100%);
    background: -o-linear-gradient(left,'.$barclr1.' -2.49%,'.$barclr2.' 100%);
    background: linear-gradient(90deg,'.$barclr1.' -2.49%,'.$barclr2.' 100%);"' : ' style="width:'.$barwidth.';background-color:'.$barclr.';"';

		$out .= '<div class="ico ico__body">';
			$out .= '<div class="ico__body-border sm-off"'.$brdclrtype.'></div>';
			$out .= '<div class="ico__body-bg" style="background-color:'.$bgclr.';"></div>';
			$out .= '<div class="ico__content">';
				//section title
				$out .= cryptoland_element( $title, $tag='h2', $class='ico__title', $tclr, $tsz, $tlh, $anim='' );

				$out .= '<div class="ico__bar"'; if($barbg != ''){ $out .= ' style="background-image:url('.$barbg.');"';}$out .= '>';
					$out .= '<div class="ico__bar-progress"'.$barclrtype.'></div>';


					//loop icobar item
					foreach ( $loop as $item ) {
						//calculate marker position on bar
						//$maxval = $maxvalue != '' ? $maxvalue : 12000;

						$position = floor( ( $item['markervalue'] / $maxvalue ) * 100);
						//bar item
					$out .= '<div class="ico__bar-marker ico__bar-marker--'.$item['markervalue'].'" style="left:'.$position.'%;">';
						$out .= cryptoland_element( $item['markertitle'], $tag='div', $class='ico__bar-marker-title', $btclr, $btsz, $btlh, $anim='' );
					$out .= '</div>';

					}

					// max value
					$out .= cryptoland_element( $maxtitle, $tag='div', $class='ico__bar-max-value', $mxbtclr, $mxbtsz, $mxbtlh, $anim='' );

				$out .= '</div>';

				$out .= '<div class="ico__row">';
					//timer
					$out .= '<div class="ico__timer">';
						$out .= cryptoland_element( $timertitle, $tag='h4', $class='ico__timer-title', $tmtclr, $tmtsz, $tmtlh, $anim='' );
						$out .= '<div class="timer timer1" id="timer1" data-date="'.$time.'"></div>';
					$out .= '</div>';

					//bottom button
					$out .= cryptoland_btn( $btnlink,$class='btn '.$btnsize,$bgtype,$btnbg,$btnclr,$hvrbg,$hvrclr,$innerel='',$icon='' );

				$out .= '</div>';
			$out .= '</div>';
		$out .= '</div>';

	return $out;
}
add_shortcode('cryptoland_icobartimer_shortcode', 'cryptoland_icobartimer');


/*-----------------------------------------------------------------------------------*/
/*	ICOBAR TIMER HOME 2
/*-----------------------------------------------------------------------------------*/

function cryptoland_icobartimer2( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "title" => '',
    "subtitle" => '',
    "bgclr" => '',
    "barbgtype" => '',
    "barbgclr" => '',
    "barbgclr1" => '',
    "barbgclr2" => '',
    "barclrtype" => '',
    "barclr" => '',
    "barclr1" => '',
    "barclr2" => '',
    "barwidth" => '',
	//loop bar marker
    "icobar" => '',
    "markertitle" => '',
    "markervalue" => '',
    "maxtitle" => '',
    "maxvalue" => '',
    "notice" => '',
    "icotext" => '',
	//timer
    "labels" => '',
    "month" => '',
    "day" => '',
    "year" => '',
    "hours" => '',
    "min" => '',
    "sec" => '',
	//loop payments img
	"link" => '',
	"btnsize" => '',
	"bgtype" => '',
	"btnbg" => '',
	"btnclr" => '',
	"hvrbg" => '',
	"hvrclr" => '',
	//loop payments img
    "payments" => '',
    "payimg" => '',
  	//custom style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	//subtitle
	"stclr" => '',
	"stsz" => '',
	"stlh" => '',
	//timer
	"tmtclr" => '',
	"tmtsz" => '',
	"tmtlh" => '',
	//max title
	"mxbtclr" => '',
	"mxbtsz" => '',
	"mxbtlh" => '',
	//ico text
	"itclr" => '',
	"itsz" => '',
	"itlh" => '',
	), $atts) );

	$loop = (array) vc_param_group_parse_atts($icobar);
	$loop2 = (array) vc_param_group_parse_atts($payments);

	//December 20, 2018 15:37:25
	$month = $month != '' ? $month : 'December';
	$day = $day != '' ? $day : ' 20,';
	$year = $year != '' ? $year : ' 2020';
	$hours = $hours != '' ? $hours : '15';
	$min = $min != '' ? $min : '37';
	$sec = $sec != '' ? $sec : '25';
	$time = $month.' '.$day.', '.$year.' '.$hours.':'.$min.':'.$sec;

	$tmtclr = $tmtclr != '' ? ' color:'.$tmtclr.';' : '';
	$tmtsz = $tmtsz != '' ? ' font-size:'.$tmtsz.';' : '';
	$tmtlh = $tmtlh != '' ? ' line-height:'.$tmtlh.';' : '';
	$timerstyle = $tmtclr != '' || $tmtsz != '' || $tmtlh != ''? ' style="'.$tmtclr.$tmtsz.$tmtlh.'"' : '';


	$bgclr = $bgclr != '' ? ' style="background-color:'.$bgclr.';"' : '';
	$barbgclr = $barbgclr != '' ? $barbgclr : '#fff';
	$barbgclr1 = $barbgclr1 != '' ? $barbgclr1 : '#9c6ea9';
	$barbgclr2 = $barbgclr2 != '' ? $barbgclr2 : '#f2949e';

	$barbgtype = $barbgtype == 'grad' ? ' style="background: -webkit-gradient(linear,left top,right top,color-stop(-2.49%,'.$barbgclr1.'),to('.$barbgclr1.'));
    background: -webkit-linear-gradient(left,'.$barbgclr1.' -2.49%,'.$barbgclr2.' 100%);
    background: -o-linear-gradient(left,'.$barbgclr1.' -2.49%,'.$barbgclr2.' 100%);
    background: linear-gradient(90deg,'.$barbgclr1.' -2.49%,'.$barbgclr2.' 100%);"' : ' style="background-color:'.$barbgclr.';"';


	$barwidth = $barwidth != '' ? $barwidth : '50%';
	$barclr = $barclr != '' ? $barclr : '#9c6ea9';
	$barclr1 = $barclr1 != '' ? $barclr1 : '#9c6ea9';
	$barclr2 = $barclr2 != '' ? $barclr2 : '#f2949e';

	$barclrtype = $barclrtype == 'grad' ? ' style="width:'.$barwidth.';background: -webkit-gradient(linear,left top,right top,color-stop(-2.49%,'.$barclr1.'),to('.$barclr2.'));
    background: -webkit-linear-gradient(left,'.$barclr1.' -2.49%,'.$barclr2.' 100%);
    background: -o-linear-gradient(left,'.$barclr1.' -2.49%,'.$barclr2.' 100%);
    background: linear-gradient(90deg,'.$barclr1.' -2.49%,'.$barclr2.' 100%);"' : ' style="width:'.$barwidth.';background-color:'.$barclr.';"';

	$out = '';
		$out .= '<div class="ico"'.$bgclr.'>';
			//title
			$out .= cryptoland_element( $title, $tag='div', $class='ico__title', $tclr, $tsz, $tlh, $anim='' );
			//subtitle
			$out .= cryptoland_element( $subtitle, $tag='div', $class='ico__subtitle', $stclr, $stsz, $stlh, $anim='' );

			//timer
			$out .= '<div class="timer timer11" id="timer11" data-labels="'.$labels.'" data-date="'.$time.'"'.$timerstyle.'></div>';

			$out .= '<div class="progress-bar"'.$barbgtype.'>';
				$out .= '<div class="ico__bar-progress"'.$barclrtype.'></div>';
				//loop icobar item
				foreach ( $loop as $item ) {
					//calculate marker position on bar
					//$maxval = $maxvalue != '' ? $maxvalue : 12000;
					$position = floor( ( $item['markervalue'] / $maxvalue ) * 100);
					$markerafter = $position <= $barwidth ? ' has_after' : '';
					//bar item
				$out .= '<div class="progress-bar__metka progress-bar__metka--'.$item['markervalue'].$markerafter.'" style="left:'.$position.'%;">'.$item['markertitle'].'</div>';
				}
				// max value
				$out .= cryptoland_element( $maxtitle, $tag='div', $class='ico__bar-max-value-style-2', $mxbtclr, $mxbtsz, $mxbtlh, $anim='' );
				// max value
				$out .= cryptoland_element( $notice, $tag='div', $class='progress-bar__notice', $mxbtclr, $mxbtsz, $mxbtlh, $anim='' );

			$out .= '</div>';
			// max value
			$out .= cryptoland_element( $icotext, $tag='div', $class='ico__text', $itclr, $itsz, $itlh, $anim='' );
			//bottom button
			$out .= cryptoland_btn( $link,$class='ico__btn '.$btnsize,$bgtype,$btnbg,$btnclr,$hvrbg,$hvrclr,$innerel='',$icon='' );
			//payments image
			$out .= '<div class="payments">';
			foreach ( $loop2 as $item ) {
				$out .= ''.cryptoland_img( $item['payimg'], $imgclass='', $imgwidth='', $imgheight='', $data='' ).'';
			}
			$out .= '</div>';
		$out .= '</div>';

	return $out;
}
add_shortcode('cryptoland_icobartimer2_shortcode', 'cryptoland_icobartimer2');


/*-----------------------------------------------------------------------------------*/
/*	SERVICE ITEM HOME 1
/*-----------------------------------------------------------------------------------*/

function cryptoland_service_item( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "img" => '',
    "title" => '',
    "desc" => '',
	"clrtype" => '',
    "anim" => '',
    "delay" => '',
    "btnlink" => '',
    "bgtype" => '',
    "btnbg" => '',
    "btnclr" => '',
    "hvrbg" => '',
    "hvrclr" => '',
  	//custom style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	), $atts) );

	$clrtype = $clrtype != '' ? $clrtype : '#16bf86';
	$delay = $delay != '' ? ' data-aos-delay="'.$delay.'" ' : '';
	$anim = $anim != '' ? ' data-aos="'.$anim.'" '.$delay : '';
	$out = '';
		$out .= '<div class="service service__item '.$clrtype.'" '.$anim.'>';
			$out .= '<div class="service__item-content">';
				$out .= '<div class="service__item-icon">';
					$out .= ''.cryptoland_img( $img, $imgclass='', $imgwidth='', $imgheight='', $data='' ).'';
				$out .= '</div>';
				$out .= cryptoland_element( $title, $tag='h3', $class='service__item-title', $tclr, $tsz, $tlh, $anim='' );
				$out .= cryptoland_element( $desc, $tag='p', $class='', $pclr, $psz, $plh, $anim='' );

			$out .= '</div>';
			//bottom button
			$out .= cryptoland_btn( $btnlink,$class='service__btn '.$bgtype,$bgtype,$btnbg,$btnclr,$hvrbg,$hvrclr,$innerel='',$icon='' );
		$out .= '</div>';

	return $out;
}
add_shortcode('cryptoland_service_item_shortcode', 'cryptoland_service_item');

/*-----------------------------------------------------------------------------------*/
/*	SERVICES HOME 3
/*-----------------------------------------------------------------------------------*/
function cryptoland_services3( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "style" => '',//style
    "bgimg1" => '',//style
	//style 1
    "iconimg1" => '',
    "title1" => '',
    "iconimg2" => '',
    "title2" => '',
    "iconimg3" => '',
    "title3" => '',
    "iconimg4" => '',
    "title4" => '',
	//style 2
	"services" => '',
	"iconimg" => '',
	"title" => '',
	"anim" => '',
	"delay" => '',
	"clrtype" => '',
	//style 3
	"simg" => '',
	"stitle" => '',
	"clrtype2" => '',
	"anim2" => '',
	"delay2" => '',
  	//custom style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	), $atts) );

	$loop = (array) vc_param_group_parse_atts($services);

	$out = '';
	$bgred = 'background-color: #e85f70; box-shadow: 0 0 51px rgba(232, 95, 112, 0.74); box-shadow: 0 0 51px rgba(232, 95, 112, 0.74);';
	$bgorange = 'background-color: #fa8936; background-image: linear-gradient(-234deg, #ea9d64 0%, #fa8936 100%); box-shadow: 0 0 51px rgba(250, 137, 54, 0.74);';
	$bggreen = 'background-image: linear-gradient(-234deg, #6ae472 0%, #4bc253 100%); box-shadow: 0 0 51px rgba(75, 194, 83, 0.74);';
	$bgblue = 'background-color: #0090d5; background-image: linear-gradient(-234deg, #29aceb 0%, #0090d5 100%); box-shadow: 0 0 51px rgba(0, 144, 213, 0.74);';

	if ( $style  =='default' ){
	$out .= '<div class="section-services">';

		$out .= '<div class="services__items">';
			$out .= '<div class="services__left">';
				$out .= '<div data-aos="fade-up" class="service">';
					$out .= '<div class="service__bg" style="background-color: #e85f70; box-shadow: 0 0 51px rgba(232, 95, 112, 0.74); box-shadow: 0 0 51px rgba(232, 95, 112, 0.74);"></div>';
					$out .= ''.cryptoland_img( $iconimg1, $imgclass='', $imgwidth='', $imgheight='', $data='' ).'';
					$out .= cryptoland_element( $title1, $tag='div', $class='service__title', $tclr, $tsz, $tlh, $anim='' );
				$out .= '</div>';
				$out .= '<div data-aos="fade-up" data-aos-delay="200" class="service">';
					$out .= '<div class="service__bg" style="background-color: #fa8936; background-image: linear-gradient(-234deg, #ea9d64 0%, #fa8936 100%); box-shadow: 0 0 51px rgba(250, 137, 54, 0.74);"></div>';
					$out .= ''.cryptoland_img( $iconimg2, $imgclass='', $imgwidth='', $imgheight='', $data='' ).'';
					$out .= cryptoland_element( $title2, $tag='div', $class='service__title', $tclr, $tsz, $tlh, $anim='' );
				$out .= '</div>';
			$out .= '</div>';
			$out .= '<div class="services__right">';
				$out .= '<div data-aos="fade-up" data-aos-delay="400" class="service" >';
					$out .= '<div class="service__bg" style="background-image: linear-gradient(-234deg, #6ae472 0%, #4bc253 100%); box-shadow: 0 0 51px rgba(75, 194, 83, 0.74);"></div>';
					$out .= ''.cryptoland_img( $iconimg3, $imgclass='', $imgwidth='', $imgheight='', $data='' ).'';
					$out .= cryptoland_element( $title3, $tag='div', $class='service__title', $tclr, $tsz, $tlh, $anim='' );
				$out .= '</div>';
				$out .= '<div data-aos="fade-up" data-aos-delay="600" class="service">';
					$out .= '<div class="service__bg" style="background-color: #0090d5; background-image: linear-gradient(-234deg, #29aceb 0%, #0090d5 100%); box-shadow: 0 0 51px rgba(0, 144, 213, 0.74);"></div>';
					$out .= ''.cryptoland_img( $iconimg4, $imgclass='', $imgwidth='', $imgheight='', $data='' ).'';
					$out .= cryptoland_element( $title4, $tag='div', $class='service__title', $tclr, $tsz, $tlh, $anim='' );
				$out .= '</div>';
			$out .= '</div>';
		$out .= '</div>';
		if ($bgimg1 != ''){$out .= ''.cryptoland_img( $bgimg1, $imgclass='services__bg', $imgwidth='', $imgheight='', $data='' ).'';}
	$out .= '</div>';
	}elseif($style  =='inline'){
	$out .= '<div class="section-services">';
	$out .= '<div class="container">';
		$out .= '<div class="row">';
			$out .= '<div class="col">';

		$out .= '<div class="services__items">';
			$out .= '<div class="services__left">';
			if ( $loop ) {
				foreach ( $loop as $item ) {
				$delay = isset( $item['delay'] )!= '' ? ' data-aos-delay="'.$item['delay'].'"' : '';
				$delay = isset( $item['anim'] )!= '' ? ' data-aos="'.$item['anim'].'"'.$delay : '';
				$out .= '<div data-aos="fade-up" class="service mb30">';
					$bgcolor =  isset( $item['clrtype'] ) != '' ? $item['clrtype'] : $bgred;
					if($bgcolor == 'orange'){
						$bgcolor=$bgorange;
					}elseif($bgcolor == 'green'){
						$bgcolor=$bggreen;
					}elseif($bgcolor == 'blue'){
						$bgcolor=$bgblue;
					}else{
						$bgcolor=$bgred;
					}
					$out .= '<div class="service__bg" style="'.$bgcolor.'"></div>';
					if ( isset( $item['iconimg'] )!= ''){$out .= ''.cryptoland_img( $item['iconimg'], $imgclass='', $imgwidth='', $imgheight='', $data='' ).'';}
				$out .= '</div>';
				}
			}
		$out .= '</div>';
		$out .= '</div>';
		$out .= '</div>';
		$out .= '</div>';
		$out .= '</div>';
			if ($bgimg1 != ''){$out .= ''.cryptoland_img( $bgimg1, $imgclass='services__bg', $imgwidth='', $imgheight='', $data='' ).'';}
			if ($bgimg2 != ''){$out .= ''.cryptoland_img( $bgimg2, $imgclass='services__cosmos', $imgwidth='', $imgheight='', $data='' ).'';}
	$out .= '</div>';

	}elseif($style  =='single'){
		$out .= '<div data-aos="fade-up" class="service">';
			$bgcolor2 = $colortype2 != '' ? $colortype2 != '' : $bgred;
			$out .= '<div class="service__bg" style="background-color: '.$bgcolor2.'"></div>';
			if ($simg != ''){$out .= ''.cryptoland_img( $simg, $imgclass='', $imgwidth='', $imgheight='', $data='' ).'';}
			if ($stitle != ''){$out .= cryptoland_element( $stitle, $tag='div', $class='service__title', $tclr, $tsz, $tlh, $anim='' );}
		$out .= '</div>';
	}



	return $out;
}
add_shortcode('cryptoland_services3_shortcode', 'cryptoland_services3');


/*-----------------------------------------------------------------------------------*/
/*	SERVICE ITEM HOME 4
/*-----------------------------------------------------------------------------------*/

function cryptoland_service_item4( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "img" => '',
    "title" => '',
    "desc" => '',
    "anim" => '',
    "delay" => '',
  	//custom style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	), $atts) );

	$delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
	$anim = $anim != '' ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';
	$out = '';

		$out .= '<div class="service"'.$anim.'>';
			$out .= '<div class="service__icon">';
				$out .= ''.cryptoland_img( $img, $imgclass='', $imgwidth='', $imgheight='', $data='' ).'';
			$out .= '</div>';
			$out .= '<div class="service__content">';
				$out .= cryptoland_element( $title, $tag='div', $class='service__title', $tclr, $tsz, $tlh, $anim='' );
				$out .= cryptoland_element( $desc, $tag='p', $class='', $pclr, $psz, $plh, $anim='' );
			$out .= '</div>';
		$out .= '</div>';
	return $out;
}
add_shortcode('cryptoland_service_item4_shortcode', 'cryptoland_service_item4');

/*-----------------------------------------------------------------------------------*/
/*	SERVICE ITEM HOME 5
/*-----------------------------------------------------------------------------------*/

function cryptoland_service_item5( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "img" => '',
    "title" => '',
    "desc" => '',
    "anim" => '',
    "delay" => '',
  	//custom style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	), $atts) );

	$delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
	$anim = $anim != '' ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';
	$out = '';
		$out .= '<div class="services__item"'.$anim.'>';
			$out .= '<div class="services__item-icon">';
				$out .= cryptoland_img( $img, $imgclass='', $imgwidth='', $imgheight='', $data='' );
			$out .= '</div>';

			$out .= '<div class="services__item-content">';
				$out .= cryptoland_element( $title, $tag='div', $class='services__item-title', $tclr, $tsz, $tlh, $anim='' );
				$out .= cryptoland_element( $desc, $tag='p', $class='services__item-text', $pclr, $psz, $plh, $anim='' );
			$out .= '</div>';
		$out .= '</div>';
	return $out;
}
add_shortcode('cryptoland_service_item5_shortcode', 'cryptoland_service_item5');

/*-----------------------------------------------------------------------------------*/
/*	HOW IT WORKS ITEM HOME 4
/*-----------------------------------------------------------------------------------*/

function cryptoland_steps_item4( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "img" => '',
    "num" => '',
    "title" => '',
    "desc" => '',
    "anim" => '',
    "delay" => '',
  	//custom style
	"nclr" => '',
	"nsz" => '',
	"nlh" => '',
	"nbgclr" => '',
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	), $atts) );

	$delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
	$anim = $anim != '' ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';
	$nclr = $nclr != '' ? ' color:'.esc_attr($nclr).';' : '';
	$nsz = $nsz != '' ? ' font-size:'.esc_attr($nsz).';' : '';
	$nlh = $nlh != '' ? ' line-height:'.esc_attr($nlh).';' : '';
	$nbgclr = $nbgclr != '' ? ' background-color:'.esc_attr($nbgclr).';' : '';
	$style =  $nclr != '' || $nsz != '' || $nlh != '' || $nbgclr != '' ? ' style="'.$nclr.$nsz.$nlh.$nbgclr.'"' : '';

	$out = '';
	$out .= '<div class="step"'.$anim.'>';
		$out .= '<div class="step__icon">';
					$out .= '<div class="step__num"'.$style.'>'.esc_html($num).'</div>';
			$out .= cryptoland_element( $num, $tag='div', $class='step__num', $nclr, $nsz, $nlh, $anim='' );
			$out .= ''.cryptoland_img( $img, $imgclass='', $imgwidth='', $imgheight='', $data='' ).'';
		$out .= '</div>';
		$out .= cryptoland_element( $title, $tag='div', $class='step__title', $tclr, $tsz, $tlh, $anim='' );
		$out .= cryptoland_element( $desc, $tag='p', $class='', $pclr, $psz, $plh, $anim='' );
	$out .= '</div>';
	return $out;
}
add_shortcode('cryptoland_steps_item4_shortcode', 'cryptoland_steps_item4');


/*-----------------------------------------------------------------------------------*/
/*	ABSOLUTE IMAGE
/*-----------------------------------------------------------------------------------*/

function cryptoland_positioned_img( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "img" => '',
    "width" => '',
    "minwidth" => '',
    "height" => '',
    "opacity" => '',
    "top" => '',
    "right" => '',
    "bottom" => '',
    "left" => '',
    "center" => '',
	"hidelg" => '',
	"hidemd" => '',
	"hidesm" => '',
	"rellg" => '',
	"relmd" => '',
	"relsm" => '',
  	//custom style

	), $atts) );
	$top = $top != '' ? ' top:'.$top .';': '';
	$right = $right != '' ? ' right:'.$right .';': '';
	$bottom = $bottom != '' ? ' bottom:'.$bottom .';': '';
	$left = $left != '' ? ' left:'.$left .';': '';
	$width = $width != '' ? ' width:'.$width .';': '';
	$minwidth = $minwidth != '' ? ' min-width:'.$minwidth .';': '';
	$height = $height != '' ? ' height:'.$height .';': '';
	$opacity = $opacity != '' ? ' opacity:'.$opacity .';': '';
	$center = $center == true ? ' horizontal-center': '';
	$hidelg = $hidelg == true ? ' lg-off': '';
	$hidemd = $hidemd == true ? ' md-off': '';
	$hidesm = $hidesm == true ? ' sm-off': '';
	$rellg = $rellg == true ? ' lg-rel': '';
	$relmd = $relmd == true ? ' md-rel': '';
	$relsm = $relsm == true ? ' sm-rel': '';
	$style = $top != '' ||  $right != '' ||  $bottom != '' ||  $left != '' ||  $width != '' ||  $minwidth != '' ||  $height != '' ||  $opacity != '' ? ' style="'.$width.$minwidth.$height.$top.$right.$bottom.$left.$opacity.'"': '';
	$img  = wp_get_attachment_url( $img,'full' );
	$out = '';
		$out .= '<img src="'.$img.'"'.$style.' alt="" class="absolute__bg'.$center.$hidelg.$hidemd.$hidesm.$rellg.$relmd.$relsm.'">';
	return $out;
}
add_shortcode('cryptoland_positioned_img_shortcode', 'cryptoland_positioned_img');

/*-----------------------------------------------------------------------------------*/
/*	JARALLAX IMAGE ELEMENT
/*-----------------------------------------------------------------------------------*/

function cryptoland_jarallax_img( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "yoffset" => '',
    "xoffset" => '',
    "parallaxtype" => '',
    "speed" => '',
    "img" => '',
    "width" => '',
    "minwidth" => '',
    "height" => '',
    "opacity" => '',
    "top" => '',
    "right" => '',
    "bottom" => '',
    "left" => '',
    "center" => '',
	"hidelg" => '',
	"hidemd" => '',
	"hidesm" => '',
  	//custom style

	), $atts) );
	$top = $top != '' ? ' top:'.$top.';': '';
	$right = $right != '' ? ' right:'.$right.';': '';
	$bottom = $bottom != '' ? ' bottom:'.$bottom.';': '';
	$left = $left != '' ? ' left:'.$left.';': '';
	$width = $width != '' ? ' width:'.$width.';': '';
	$minwidth = $minwidth != '' ? ' min-width:'.$minwidth.';': '';
	$height = $height != '' ? ' height:'.$height .';': '';
	$opacity = $opacity != '' ? ' opacity:'.$opacity.';': '';
	$center = $center == true ? ' horizontal-center': '';
	$hidelg = $hidelg == true ? ' lg-off': '';
	$hidemd = $hidemd == true ? ' md-off': '';
	$hidesm = $hidesm == true ? ' sm-off': '';
	$parallaxtype = $parallaxtype == '' ? ' data-type="'.$parallaxtype.'"': '';
	$speed = $speed == '' ? ' data-speed="'.$speed.'"': '';
	$yoffset = $yoffset != '' ?  $yoffset : '';
	$xoffset = $xoffset != '' ?  $xoffset : '';
	$offset = $xoffset != '' || $yoffset != '' ? ' data-jarallax-element="'.$yoffset.$xoffset.'"' : '';

	$style = $top != '' ||  $right != '' ||  $bottom != '' ||  $left != '' ||  $width != '' ||  $minwidth != '' ||  $height != '' ||  $opacity != '' ? ' style="'.$width.$minwidth.$height.$top.$right.$bottom.$left.$opacity.'"': '';
	$img  = wp_get_attachment_url( $img,'full' );
	$out = '';

		$out .= '<img src="'.$img.'"'.$style.$offset.$parallaxtype.$speed.' alt="" class="absolute__bg parallax__img'.$center.$hidelg.$hidemd.$hidesm.'">';
	return $out;
}
add_shortcode('cryptoland_jarallax_img_shortcode', 'cryptoland_jarallax_img');



/*-----------------------------------------------------------------------------------*/
/*	APP SECTION
/*-----------------------------------------------------------------------------------*/

function cryptoland_app( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"bgimg" => '',
	"stitle" => '',
	"title" => '',
	"desc" => '',
	"con_aos" => '',
	"rimg" => '',
	"img_aos" => '',
	"img_delay" => '',
	"limg1" => '',
	"link1" => '',
	"link2" => '',
	"limg2" => '',
	//custom style
	"stclr" => '',
	"stsz" => '',
	"stlh" => '',
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	"pclr" => '',
	"psz" => '',
	"plh" => '',

	), $atts) );

	$img_aos = $img_aos != '' ? $img_aos : '';
	$img_delay = $img_delay != '' ? $img_delay : '400';
	$con_aos = $con_aos != '' ? ' data-aos="'.$con_aos .'"': '';
	$img_aos = $img_aos != '' ? ' data-aos="'.$img_aos.'" data-aos-offset="-200" data-aos-delay="'.$img_delay.'"' : '';
	$bgimg  = wp_get_attachment_url( $bgimg,'full' );
	$bgimg  = $bgimg != '' ? ' style="background-image: url('.$bgimg.');"' : '' ;

	$out = '';

		$out .= '<div class="app section"'.$bgimg.'>';
			$out .= '<div class="container">';
				$out .= '<div class="row">';
					$out .= '<div class="col-lg-5 col-md-8"'.$con_aos.'>';
						$out .= '<div class="block-header block-header--animated">';
							$out .= cryptoland_element( $stitle, $tag='div', $class='subtitle subtitle--tire', $stclr, $stsz, $stlh, $anim='' );
							$out .= cryptoland_element( $title, $tag='h2', $class='title title--black title--medium', $tclr, $tsz, $tlh, $anim='' );
						$out .= '</div>';
						$out .= cryptoland_element( $desc, $tag='p', $class='', $pclr, $psz, $plh, $anim='' );

						$out .= '<div class="app__dowload-links">';
							if($limg1){
							$out .= '<a href="'.esc_url($link1).'" class="app__link">';
								$out .= ''.cryptoland_img( $limg1, $imgclass='', $imgwidth='', $imgheight='', $data='' ).'';
							$out .= '</a>';
							}
							if($limg2){
							$out .= '<a href="'.esc_url($link2).'" class="app__link">';
								$out .= ''.cryptoland_img( $limg2, $imgclass='', $imgwidth='', $imgheight='', $data='' ).'';
							$out .= '</a>';
							}
						$out .= '</div>';
					$out .= '</div>';

					$out .= ''.cryptoland_img( $rimg, $imgclass='app__iphone', $imgwidth='', $imgheight='', $data=$img_aos ).'';
				$out .= '</div>';
			$out .= '</div>';
		$out .= '</div>';

	return $out;
}
add_shortcode('cryptoland_app_shortcode', 'cryptoland_app');


/*-----------------------------------------------------------------------------------*/
/*	PLATFORM SECTION HOME 2
/*-----------------------------------------------------------------------------------*/

function cryptoland_platform2( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"bgimg" => '',
	"stitle" => '',
	"title" => '',
	"desc" => '',
	"con_aos" => '',
	"rimg" => '',
	"anim" => '',
	"delay" => '',
	//button
	"btnlink" => '',
	"btnsize" => '',
	"bgtype" => '',
	"btnbg" => '',
	"btnclr" => '',
	"hvrbg" => '',
	"hvrclr" => '',
	//custom style
	"stclr" => '',
	"stsz" => '',
	"stlh" => '',
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	"pclr" => '',
	"psz" => '',
	"plh" => '',

	), $atts) );

	$delay = $delay != '' ? $delay : '';
	$anim = $anim != '' ? ' data-aos="'.$anim.'" data-aos-delay="'.$delay.'"' : '';

	$out = '';
		$out .= '<div class="section platform">';
			$out .= '<div class="container">';
				$out .= '<div class="row">';
					$out .= '<div class="col-lg-6 col-xl-5 col-md-8 platform__content">';

						$out .= '<div'.$anim.'>';
							$out .= '<div class="section-header section-header--tire section-header--small-margin">';
								$out .= cryptoland_element( $stitle, $tag='h4', $class='', $stclr, $stsz, $stlh, $anim='' );
								$out .= cryptoland_element( $title, $tag='h2', $class='', $tclr, $tsz, $tlh, $anim='' );
							$out .= '</div>';
							$out .= cryptoland_element( $desc, $tag='p', $class='', $pclr, $psz, $plh, $anim='' );
							$out .= '<div class="inline-block">';

							//bottom button 1
							$out .= cryptoland_btn( $btnlink,$class='btn btn--orange btn--purpure-shadow '.$btnsize,$bgtype,$btnbg,$btnclr,$hvrbg,$hvrclr,$innerel='',$icon='' );

							$out .= '</div>';
						$out .= '</div>';
						$out .= ''.cryptoland_img( $rimg, $imgclass='platform__img', $imgwidth='', $imgheight='', $data='' ).'';
					$out .= '</div>';

				$out .= '</div>';
			$out .= '</div>';
			$bgimg  = wp_get_attachment_url( $bgimg,'full' );
			$out .= '<img src="'.esc_url($bgimg).'" data-jarallax-element="-140" class="parallax-el" alt="">';
		$out .= '</div>';


	return $out;
}
add_shortcode('cryptoland_platform2_shortcode', 'cryptoland_platform2');

/*-----------------------------------------------------------------------------------*/
/*	TOKEN SECTION
/*-----------------------------------------------------------------------------------*/
function cryptoland_token( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"bgimg" => '',
	"stitle" => '',
	"title" => '',
	"con_aos" => '',
	"desctitle" => '',
	"list" => '',
	//button
    "btnlink" => '',
	"btnsize" => '',
	"bgtype" => '',
	"btnbg" => '',
    "btnclr" => '',
    "hvrbg" => '',
    "hvrclr" => '',
	//custom style
	"stclr" => '',
	"stsz" => '',
	"stlh" => '',
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	"tpclr" => '',
	"tpsz" => '',
	"tplh" => '',
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	), $atts) );

	$loop = (array) vc_param_group_parse_atts($list);
	$con_aos = $con_aos != '' ? ' data-aos="'.$con_aos .'"': '';

	$bgimg  = wp_get_attachment_url( $bgimg,'full' );
	$bgimg  = $bgimg != '' ? ' style="background-image: url('.esc_url($bgimg).');"' : '' ;

	$out = '';

		$out .= '<div class="token section">';
			$out .= '<div class="token-bg md-off"'.$bgimg.'></div>';
			$out .= '<div class="container">';
				$out .= '<div class="row">';
					$out .= '<div class="col-lg-6 offset-lg-6"'.$con_aos.'>';
						$out .= '<div class="block-header block-header--animated">';
							$out .= cryptoland_element( $stitle, $tag='div', $class='subtitle subtitle--tire', $stclr, $stsz, $stlh, $anim='' );
							$out .= cryptoland_element( $title, $tag='h2', $class='title title--medium title--black', $tclr, $tsz, $tlh, $anim='' );
						$out .= '</div>';
						if ( $loop ) {
						$out .= '<ul class="token__info-list">';
							foreach ( $loop as $item ) {
							if ( isset( $item['label'] ) != '' || isset( $item['ltitle'] ) != '' ) {
							$out .= '<li><span>'.$item['label'].'</span> '.$item['ltitle'].'</li>';
							}
							}
						$out .= '</ul>';
						}
						$out .= '<div class="token__desc">';
							$out .= cryptoland_element( $desctitle, $tag='div', $class='token__desc-title', $tpclr, $tpsz, $tplh, $anim='' );
							$out .= '<div class="token__desc-text">';
								$out .= '<p>'.do_shortcode( $content ).'</p>';
							$out .= '</div>';
						$out .= '</div>';
						//bottom button
						$out .= cryptoland_btn( $btnlink,$class='btn '.$bgtype.' '.$btnsize,$bgtype,$btnbg,$btnclr,$hvrbg,$hvrclr,$innerel='',$icon='' );

					$out .= '</div>';
				$out .= '</div>';
			$out .= '</div>';
		$out .= '</div>';

	return $out;
}
add_shortcode('cryptoland_token_shortcode', 'cryptoland_token');


/*-----------------------------------------------------------------------------------*/
/*	DOCUMENT DOWNLOAD HOME 1-6
/*-----------------------------------------------------------------------------------*/
function cryptoland_docitem( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"bgimg" => '',
	"title" => '',
	//animation
	"addanim" => '',
	"anim" => '',
	"delay" => '',
	//btn
	"btnlink" => '',
	//custom style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	"css" => '',
	), $atts) );


	$delay = $delay != '' ? ' data-aos-delay="'.$delay .'"': '';
	$anim = $addanim == true && $anim != '' ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';

	$btnlink	= ( $btnlink == '||' ) ? '' : $btnlink;
	$btnlink	= vc_build_link( $btnlink );
	$btn_title	= $btnlink['title'];
	$btn_target	= $btnlink['target'];
	$btn_href	= $btnlink['url'];
	$btn_target = $btn_target != '' ? ' target="'.$btn_target.'"' : '';

	$out = '';
	$out .= '<div class="doc__wrap"'.$anim.'>';
	$out .= '<a href="'.$btn_href.'"'.$btn_target.' class="doc '.cryptoland_vc_custom_css_class($css).'">';
		$out .= cryptoland_element( $btn_title, $tag='div', $class='doc__name', $tclr, $tsz, $tlh, $anim='' );
		$out .= '<div class="doc__img">';
		$out .= cryptoland_img( $bgimg, $imgclass='', $imgwidth='', $imgheight='', $data='' );
		$out .= '</div>';
	$out .= '</a>';
	$out .= '</div>';
	return $out;
}
add_shortcode('cryptoland_docitem_shortcode', 'cryptoland_docitem');

/*-----------------------------------------------------------------------------------*/
/*	DOCUMENT DOWNLOAD HOME 2-3
/*-----------------------------------------------------------------------------------*/
function cryptoland_docitem2( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"bgimg" => '',
	"img" => '',
	"title" => '',
	//animation
	"addanim" => '',
	"anim" => '',
	"delay" => '',
	"link" => '',
	//custom style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	"css" => '',
	), $atts) );

	$delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
	$anim = $addanim == true && $anim != '' ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';

	$classimg = $title == '' ? ' title--none': '';

	$link = ( $link == '||' ) ? '' : $link;
	$link = vc_build_link( $link );
	$atitle = $link['title'];
	$target = $link['target'];
	$href = $link['url'];
	$target = $target != '' ? ' target="'.$target.'"' : '';
	$bgimg = wp_get_attachment_url( $bgimg,'full' );
	$bgimg = $bgimg != '' ? ' style="background-image:url('.esc_url($bgimg).');"' : '';
    $rand=rand();
    if($bgimg != ''):
        echo '<style>.rand-'.esc_attr($rand).':before{background:none;}</style>';
    endif;
	$out = '';
	$out .= '<div  class="doc_wrapper"'.$anim.'>';
		$out .= '<a  class="rand-'.esc_attr($rand).' doc'.$classimg.'" href="'.$href.'"'.$target.' title="'.$atitle.'"'.$bgimg.'>';
			$out .= '<div class="doc__content">';
				$out .= cryptoland_img( $img, $imgclass='', $imgwidth='', $imgheight='', $data='' );
				$out .= cryptoland_element( $title, $tag='div', $class='doc__title', $tclr, $tsz, $tlh, $anim='' );
			$out .= '</div>';
		$out .= '</a>';
	$out .= '</div>';
	return $out;
}
add_shortcode('cryptoland_docitem2_shortcode', 'cryptoland_docitem2');

/*-----------------------------------------------------------------------------------*/
/*	DOCUMENT DOWNLOAD HOME 4
/*-----------------------------------------------------------------------------------*/
function cryptoland_docitem4( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"title" => '',
	"doctype" => '',
	//animation
	"addanim" => '',
	"anim" => '',
	"delay" => '',
	"link" => '',
	//custom style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	"iclr" => '',
	"isz" => '',
	"ilh" => '',
	"css" => '',
	), $atts) );

	$delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
	$anim = $addanim == true && $anim != '' ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';

	$link	= ( $link == '||' ) ? '' : $link;
	$link	= vc_build_link( $link );
	$atitle	= $link['title'];
	$target	= $link['target'];
	$href	= $link['url'];
	$target = $target != '' ? ' target="'.$target.'"' : '';

	$out = '';
	$out .= '<div class="doc__wrap"'.$anim.'>';
	$out .= '<a href="'.$href.'"'.$target.' title="'.$atitle.'" class="doc '.cryptoland_vc_custom_css_class($css).'">';
		$out .= '<div class="doc__content">';
			$out .= '<div class="doc__icon">';
				$out .= cryptoland_element( $doctype, $tag='span', $class='', $tclr, $tsz, $tlh, $anim='' );
			$out .= '</div>';
			$out .= cryptoland_element( $title, $tag='div', $class='doc__title', $tclr, $tsz, $tlh, $anim='' );
		$out .= '</div>';
	$out .= '</a>';
	$out .= '</div>';
	return $out;
}
add_shortcode('cryptoland_docitem4_shortcode', 'cryptoland_docitem4');

/*-----------------------------------------------------------------------------------*/
/*	DOCUMENT DOWNLOAD HOME 5
/*-----------------------------------------------------------------------------------*/
function cryptoland_docitem5( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"bgimg" => '',
	"img" => '',
	"title" => '',
	"doctype" => '',
	"bgclrtype" => '',
	"bgclr" => '',
	"bgclr1" => '',
	"bgclr2" => '',
	//animation
	"addanim" => '',
	"anim" => '',
	"delay" => '',
	"link" => '',
	//custom style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	), $atts) );

	$delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
	$anim = $addanim == true && $anim != '' ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';
	$bgimg  = wp_get_attachment_url( $bgimg,'full' );

	$bgclr = $bgclr != '' ? $bgclr : '#40a4f3';
	$bgclr1 = $bgclr1 != '' ? $bgclr1 : '#704ffd ';
	$bgclr2 = $bgclr2 != '' ? $bgclr2 : '#40a4f3 ';

	$bgclrtype = $bgclrtype != 'one' ? ' style=" background-image: -webkit-linear-gradient(309deg,'.$bgclr1.' 0,'.$bgclr2.' 100%);background-image: -o-linear-gradient(309deg,'.$bgclr1.' 0,'.$bgclr2.' 100%);background-image: linear-gradient(-219deg,'.$bgclr1.' 0,'.$bgclr2.' 100%);"' : ' style="background-color:'.$bgclr.';"';


	$link	= ( $link == '||' ) ? '' : $link;
	$link	= vc_build_link( $link );
	$atitle	= $link['title'];
	$target	= $link['target'];
	$href	= $link['url'];
	$target = $target != '' ? ' target="'.$target.'"' : '';

	$out = '';
	$out .= '<div class="doc__wrap"'.$anim.'>';
	$out .= '<a href="'.$href.'"'.$target.' title="'.$atitle.'" class="doc" style="background-image:url('.esc_url($bgimg).');">';
		$out .= '<div class="doc__bg"'.$bgclrtype.'></div>';
		$out .= '<div class="doc__content">';
			$out .= cryptoland_img( $img, $imgclass='', $imgwidth='', $imgheight='', $data='' );
			$out .= cryptoland_element( $title, $tag='div', $class='doc__title', $tclr, $tsz, $tlh, $anim='' );
		$out .= '</div>';
	$out .= '</a>';
	$out .= '</div>';
	return $out;
}
add_shortcode('cryptoland_docitem5_shortcode', 'cryptoland_docitem5');

/*-----------------------------------------------------------------------------------*/
/*	CASES ITEM HOME 3
/*-----------------------------------------------------------------------------------*/
function cryptoland_cases3( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"img" => '',
	"title" => '',
	"desc" => '',
	//animation
	"addanim" => '',
	"anim" => '',
	"delay" => '',
	//custom style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	), $atts) );

	$delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
	$addanim = $addanim == true ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';

	$out = '';
		$out .= '<div class="cases__item"'.$addanim.'>';
			$out .= cryptoland_img( $img, $imgclass='cases__item-icon', $imgwidth='', $imgheight='', $data='' );
			$out .= '<div class="cases__item-content">';
				$out .= cryptoland_element( $title, $tag='div', $class='cases__item-title', $tclr, $tsz, $tlh, $anim='' );
				$out .= cryptoland_element( $desc, $tag='P', $class='cases__item-text', $pclr, $psz, $plh, $anim='' );
			$out .= '</div>';
		$out .= '</div>';
	return $out;
}
add_shortcode('cryptoland_cases3_shortcode', 'cryptoland_cases3');

/*-----------------------------------------------------------------------------------*/
/*	VIDEO POPUP HOME 2
/*-----------------------------------------------------------------------------------*/
function cryptoland_popupvideo( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"playimg" => '',
	"bgimg" => '',
	"vidurl" => '',
	"addanim" => '',
	"anim" => '',
	"delay" => '',
	"css" => '',
	), $atts) );
	$delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
	$addanim = $addanim == true && $anim != ''  ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';
	$vidurl = $vidurl != '' ? esc_url($vidurl) : '';
	$bgimg  = wp_get_attachment_url( $bgimg,'full' );
	$bgimg  = $bgimg != ''? ' style="background-image:url('.esc_url($bgimg).');"' : '';
	$out = '';
	$out .= '<div class="video"'.$bgimg.'>';
		$out .= '<a href="'.$vidurl.'" class="popup-youtube video__btn">';
		$out .= cryptoland_img( $playimg, $imgclass='', $imgwidth='', $imgheight='', $data='' );
		$out .= '</a>';
	$out .= '</div>';


	return $out;
}
add_shortcode('cryptoland_popupvideo_shortcode', 'cryptoland_popupvideo');


/*-----------------------------------------------------------------------------------*/
/*	VIDEO POPUP HOME 1-4
/*-----------------------------------------------------------------------------------*/
function cryptoland_popupvideo4( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"bgimg" => '',
	"playimg" => '',
	"vidurl" => '',
	"bgclr" => '',
	"addanim" => '',
	"anim" => '',
	"delay" => '',
	), $atts) );
	$delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
	$anim = $addanim == true && $anim != ''  ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';
	$vidurl = $vidurl != '' ? esc_url($vidurl) : '';
	$out = '';

	$out .= '<div class="video"'.$anim.'>';
		$out .= cryptoland_img( $bgimg, $imgclass='img-responsive video__bg', $imgwidth='', $imgheight='', $data='' );
		$out .= '<a href="'.esc_url($vidurl).'" class="popup-youtube video__btn">';
			$out .= cryptoland_img( $playimg, $imgclass='', $imgwidth='', $imgheight='', $data='' );
		$out .= '</a>';
	$out .= '</div>';

	return $out;
}
add_shortcode('cryptoland_popupvideo4_shortcode', 'cryptoland_popupvideo4');


/*-----------------------------------------------------------------------------------*/
/*	ADVISOR STYLE PAGE  1
/*-----------------------------------------------------------------------------------*/
function cryptoland_advisor( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"bgimg" => '',
	"img" => '',
	"iconimg" => '',
	"job" => '',
	//animation
	"addanim" => '',
	"anim" => '',
	"delay" => '',
	"btnlink" => '',
	//custom style
	"nclr" => '',
	"nsz" => '',
	"nlh" => '',
	"jclr" => '',
	"jsz" => '',
	"jlh" => '',
	"css" => '',
	), $atts) );

	$delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
	$addanim = $addanim == true ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';

	$btnlink	= ( $btnlink == '||' ) ? '' : $btnlink;
	$btnlink	= vc_build_link( $btnlink );
	$btn_title	= $btnlink['title'];
	$btn_target	= $btnlink['target'];
	$btn_href	= $btnlink['url'];
	$btn_target = $btn_target != '' ? ' target="'.$btn_target.'"' : '';

	$out = '';
		$out .= '<a href="'.$btn_href.'"'.$btn_target.' title="'.$btn_title.'" class="advisor '.cryptoland_vc_custom_css_class($css).'"'.$addanim.'>';
			$out .= '<div class="advisor__avatar">';
				$out .= cryptoland_img( $bgimg, $imgclass='advisor__bg', $imgwidth='', $imgheight='', $data='' );
				$out .= cryptoland_img( $img, $imgclass='advisor__img', $imgwidth='', $imgheight='', $data='' );
			$out .= '</div>';
			$out .= cryptoland_element( $btn_title, $tag='div', $class='advisor__name', $nclr, $nsz, $nlh, $anim='' );
			$out .= cryptoland_element( $job, $tag='div', $class='advisor__post', $jclr, $jsz, $jlh, $anim='' );
			$out .= '<div class="advisor_social">';
			$out .= cryptoland_img( $iconimg, $imgclass='', $imgwidth='', $imgheight='', $data='' );
			$out .= '</div>';
		$out .= '</a>';
	return $out;
}
add_shortcode('cryptoland_advisor_shortcode', 'cryptoland_advisor');


/*-----------------------------------------------------------------------------------*/
/*	ADVISOR HOME 2-3-5-6
/*-----------------------------------------------------------------------------------*/
function cryptoland_advisor2( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"img" => '',
	"icon" => '',
	"name" => '',
	"job" => '',
	"desc" => '',
	//animation
	"addanim" => '',
	"anim" => '',
	"delay" => '',
	"link" => '',
	//custom style
	"nclr" => '',
	"nsz" => '',
	"nlh" => '',
	"jclr" => '',
	"jsz" => '',
	"jlh" => '',
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	"css" => '',
	), $atts) );

	$delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
	$addanim = $addanim == true ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';

	$link	= ( $link == '||' ) ? '' : $link;
	$link	= vc_build_link( $link );
	$title	= $link['title'];
	$target	= $link['target'];
	$href	= $link['url'];
	$target = $target != '' ? ' target="'.$target.'"' : '';

	$out = '';

		$out .= '<div class="advisor"'.$addanim.'>';
			$out .= '<a href="'.$href.'"'.$target.' title="'.$title.'" class="advisor__img">';
			$out .= '<div class="advisor__bg '.cryptoland_vc_custom_css_class($css).'"></div>';
				$out .= cryptoland_img( $img, $imgclass='advisor__img', $imgwidth='', $imgheight='', $data='' );

				$out .= '<div class="advisor__sn">';
					$out .= cryptoland_img( $icon, $imgclass='', $imgwidth='', $imgheight='', $data='' );
				$out .= '</div>';
			$out .= '</a>';
			$out .= '<div class="advisor__content">';
				$out .= cryptoland_element( $name, $tag='div', $class='advisor__title', $nclr, $nsz, $nlh, $anim='' );
				$out .= cryptoland_element( $job, $tag='div', $class='advisor__post', $jclr, $jsz, $jlh, $anim='' );
				$out .= cryptoland_element( $desc, $tag='p', $class='advisor__text', $pclr, $psz, $plh, $anim='' );
			$out .= '</div>';
		$out .= '</div>';

	return $out;
}
add_shortcode('cryptoland_advisor2_shortcode', 'cryptoland_advisor2');


/*-----------------------------------------------------------------------------------*/
/*	TEAM STYLE PAGE 1
/*-----------------------------------------------------------------------------------*/
function cryptoland_teamitem( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"img" => '',
	"iconimg" => '',
	"name" => '',
	"job" => '',
	//animation
	"addanim" => '',
	"anim" => '',
	"delay" => '',
	"link" => '',
	//custom style
	"nclr" => '',
	"nsz" => '',
	"nlh" => '',
	"jclr" => '',
	"jsz" => '',
	"jlh" => '',
	"css" => '',
	), $atts) );

	$delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
	$addanim = $addanim == true ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';

	$link	= ( $link == '||' ) ? '' : $link;
	$link	= vc_build_link( $link );
	$title	= $link['title'];
	$target	= $link['target'];
	$href	= $link['url'];
	$target = $target != '' ? ' target="'.$target.'"' : '';

	$out = '';

		$out .= '<a href="'.$href.'"'.$target.' title="'.$title.'" class="team-member '.cryptoland_vc_custom_css_class($css).'"'.$addanim.'>';
			$out .= '<div class="team_social">';
			$out .= cryptoland_img( $iconimg, $imgclass='', $imgwidth='', $imgheight='', $data='' );
			$out .= '</div>';
			$out .= '<div class="team-member__avatar">';
				$out .= cryptoland_img( $img, $imgclass='advisor__img', $imgwidth='', $imgheight='', $data='' );
			$out .= '</div>';
			$out .= cryptoland_element( $name, $tag='div', $class='team-member__name', $nclr, $nsz, $nlh, $anim='' );
			$out .= cryptoland_element( $job, $tag='div', $class='team-member__post', $jclr, $jsz, $jlh, $anim='' );
		$out .= '</a>';

	return $out;
}
add_shortcode('cryptoland_teamitem_shortcode', 'cryptoland_teamitem');


/*-----------------------------------------------------------------------------------*/
/*	TEAM STYLE PAGE 2
/*-----------------------------------------------------------------------------------*/
function cryptoland_teamitem2( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"img" => '',
	"name" => '',
	"job" => '',
	//loop social
	"social" => '',
	"icon" => '',
	"link" => '',
	//animation
	"addanim" => '',
	"anim" => '',
	"delay" => '',
	//custom style
	"nclr" => '',
	"nsz" => '',
	"nlh" => '',
	"jclr" => '',
	"jsz" => '',
	"jlh" => '',
	"css" => '',
	), $atts) );

	$delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
	$addanim = $addanim == true ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';
	$loop = (array) vc_param_group_parse_atts($social);

	$out = '';

		$out .= '<div class="team-member-style-2 team-member '.cryptoland_vc_custom_css_class($css).'"'.$addanim.'>';
			$out .= cryptoland_img( $img, $imgclass='team-member__avatar', $imgwidth='', $imgheight='', $data='' );
			$out .= '<div class="team-member__content">';
				$out .= cryptoland_element( $name, $tag='div', $class='team-member__name', $nclr, $nsz, $nlh, $anim='' );
				$out .= cryptoland_element( $job, $tag='div', $class='team-member__post', $jclr, $jsz, $jlh, $anim='' );
				$out .= '<ul class="team-member__social">';
				foreach ( $loop as $item ) {
					if ( isset ( $item['icon'] ) != '' ) {
					$link = ! empty ( $item['link'] ) ? $item['link'] : '';
					$link = vc_build_link( $link );
					$title = $link != '' ? $link['title'] : '';
					$target = $link != '' ? $link['target'] : '';
					$href = $link != '' ? $link['url'] : '';
					$out .= '<li>';
						$out .= '<a href="'.$href.'"'.$target.' title="'.$title.'">';
							$out .= cryptoland_img( $item['icon'], $imgclass='', $imgwidth='', $imgheight='', $data='' );
						$out .= '</a>';
					$out .= '</li>';
					}
				}
				$out .= '</ul>';
			$out .= '</div>';
		$out .= '</div>';

	return $out;
}
add_shortcode('cryptoland_teamitem2_shortcode', 'cryptoland_teamitem2');



/*-----------------------------------------------------------------------------------*/
/*	TEAM STYLE 3
/*-----------------------------------------------------------------------------------*/
function cryptoland_teamitem3( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"img" => '',
	"name" => '',
	"job" => '',
	"desc" => '',
	//loop social
	"icon" => '',
	"link" => '',
	//animation
	"addanim" => '',
	"anim" => '',
	"delay" => '',
	//custom style
	"nclr" => '',
	"nsz" => '',
	"nlh" => '',
	"jclr" => '',
	"jsz" => '',
	"jlh" => '',
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	"css" => '',
	), $atts) );

	$delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
	$addanim = $addanim == true ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';
	$icon  = wp_get_attachment_url( $icon,'full' );
	$link	= ( $link == '||' ) ? '' : $link;
	$link	= vc_build_link( $link );
	$title	= $link['title'];
	$target	= $link['target'];
	$href	= $link['url'];
	$target = $target != '' ? ' target="'.$target.'"' : '';

	$out = '';
		$out .= '<div class="team-member-style-3 '.cryptoland_vc_custom_css_class($css).'"'.$addanim.'>';
			$out .= '<a class="team-member" href="'.$href.'"'.$target.' title="'.$title.'">';
				$out .= '<div class="team-member__ava">';
					$out .= cryptoland_img( $img, $imgclass='team-member__avatar', $imgwidth='', $imgheight='', $data='' );
					$out .= '<div class="team-member__social-link" style="background-image:url('.$icon.');"></div>';
				$out .= '</div>';
				$out .= cryptoland_element( $name, $tag='div', $class='team-member__name', $nclr, $nsz, $nlh, $anim='' );
				$out .= cryptoland_element( $job, $tag='div', $class='team-member__post', $jclr, $jsz, $jlh, $anim='' );
				$out .= cryptoland_element( $desc, $tag='p', $class='team-member__desc', $pclr, $psz, $plh, $anim='' );
			$out .= '</a>';
		$out .= '</div>';

	return $out;
}
add_shortcode('cryptoland_teamitem3_shortcode', 'cryptoland_teamitem3');

/*-----------------------------------------------------------------------------------*/
/*	TEAM STYLE 4
/*-----------------------------------------------------------------------------------*/
function cryptoland_teamitem4( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"img" => '',
	"name" => '',
	"job" => '',
	"desc" => '',
	//loop social
	"social" => '',
	"icon" => '',
	"link" => '',
	//animation
	"addanim" => '',
	"anim" => '',
	"delay" => '',
	//custom style
	//custom style
	"nclr" => '',
	"nsz" => '',
	"nlh" => '',
	"jclr" => '',
	"jsz" => '',
	"jlh" => '',
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	"css" => '',
	), $atts) );

	$delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
	$addanim = $addanim == true ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';
	$loop = (array) vc_param_group_parse_atts($social);

	$out = '';

		$out .= '<div class="team-member team-member--white '.cryptoland_vc_custom_css_class($css).'"'.$addanim.'>';
			$out .= '<div class="team-member__ava">';
				$out .= cryptoland_img( $img, $imgclass='', $imgwidth='', $imgheight='', $data='' );
			$out .= '</div>';
				$out .= cryptoland_element( $name, $tag='div', $class='team-member__name', $nclr, $nsz, $nlh, $anim='' );
				$out .= cryptoland_element( $job, $tag='div', $class='team-member__post', $jclr, $jsz, $jlh, $anim='' );
				$out .= cryptoland_element( $desc, $tag='p', $class='team-member__desc', $pclr, $psz, $plh, $anim='' );
			$out .= '<ul class="team-member__social-list">';
				foreach ( $loop as $item ) {
					if ( isset ( $item['icon'] ) != '' ) {
					$link = ! empty ( $item['link'] ) ? $item['link'] : '';
					$link = vc_build_link( $link );
					$title = $link != '' ? $link['title'] : '';
					$target = $link != '' ? $link['target'] : '';
					$href = $link != '' ? $link['url'] : '';
					$out .= '<li>';
						$out .= '<a href="'.$href.'"'.$target.' title="'.$title.'">';
							$out .= cryptoland_img( $item['icon'], $imgclass='', $imgwidth='', $imgheight='', $data='' );
						$out .= '</a>';
					$out .= '</li>';
					}
				}
			$out .= '</ul>';
		$out .= '</div>';
	return $out;
}
add_shortcode('cryptoland_teamitem4_shortcode', 'cryptoland_teamitem4');

/*-----------------------------------------------------------------------------------*/
/*	PRESS ABOUT CAROUSEL STYLE PAGE 1
/*-----------------------------------------------------------------------------------*/
function cryptoland_press_carousel( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"about" => '',
	"img" => '',
	"desc" => '',
	"linkcheck" => '',
	"link" => '',
	"autoplay" => '',
	"sldloop" => '',
	"dotclr" => '',
	"actdotclr" => '',
	//custom style
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	), $atts) );
    $uniq_id = 'carousel_'.uniqid();
	$loop = (array) vc_param_group_parse_atts($about);
	$autoplay = $autoplay == 'yes' ? ' data-autoplay="'.$autoplay .'"': '';
	$dotclr = $dotclr != '' ? ' data-dotclr="'.$dotclr .'"': '';
	$actdotclr = $actdotclr != '' ? ' data-active-dotclr="'.$actdotclr .'"': '';
	$sldloop = $sldloop == 'yes' ? ' data-loop="'.$sldloop .'"': '';
	$out = '';
	if ( $loop ){
		$out .= '<div class="press-carousel owl-carousel '.$uniq_id.'"'.$autoplay.$sldloop.$dotclr.$actdotclr.'>';
		foreach ( $loop as $item ) {
			$out .= '<div class="press-carousel__item">';
				$out .= cryptoland_element( $item['desc'], $tag='p', $class='', $pclr, $psz, $plh, $anim='' );
			if ( isset ( $item['linkcheck'] ) =='yes' ) {
				$link = ( $item['link'] == '||' ) ? '' : $item['link'];
				$link = vc_build_link( $link );
				$title = $link['title'];
				$target = $link['target'];
				$href = $link['url'];
				$out .= '<a href="'.$href .'" title="'.$title.'" class="press-carousel__item-logo">';
					$out .= cryptoland_img( $item['img'], $imgclass='', $imgwidth='', $imgheight='', $data='' );
				$out .= '</a>';
			}else{
				$out .= '<div class="press-carousel__item-logo">';
					$out .= cryptoland_img( $item['img'], $imgclass='', $imgwidth='', $imgheight='', $data='' );
				$out .= '</div>';
			}
			$out .= '</div>';
		}
		$out .= '</div>';
        if ( function_exists( 'vc_is_inline' ) && vc_is_inline() ) {
            $aplay = $autoplay == 'yes' ? 'true': 'false';
            $sloop = $sldloop == true ? 'true': 'false';
            $out .= '<script type="text/javascript">jQuery(document).ready(function($){$(".'.$uniq_id.'").owlCarousel({items:2,loop:'.$sloop.',autoplay:'.$aplay.',dots:true,dotClass:\'owl-dot\',responsive:{0:{items:1},768:{items:2}}});});</script>';
        }
	}
	return $out;
}
add_shortcode('cryptoland_press_carousel_shortcode', 'cryptoland_press_carousel');

/*-----------------------------------------------------------------------------------*/
/*	PRESS ABOUT CAROUSEL STYLE PAGE 2
/*-----------------------------------------------------------------------------------*/
function cryptoland_press_carousel2( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"about" => '',
	"img" => '',
	"desc" => '',
	"linkcheck" => '',
	"link" => '',
	"autoplay" => '',
	"sldloop" => '',
	"dotclr" => '',
	"actdotclr" => '',
	//custom style
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	), $atts) );
    $uniq_id = 'carousel_'.uniqid();
	$loop = (array) vc_param_group_parse_atts($about);
	$autoplay = $autoplay == true ? ' data-autoplay="yes"': ' data-autoplay="no"';
	$dotclr = $dotclr != '' ? ' data-dotclr="'.$dotclr .'"': '';
	$actdotclr = $actdotclr != '' ? ' data-active-dotclr="'.$actdotclr .'"': '';
	$sldloop = $sldloop == 'yes' ? ' data-loop="'.$sldloop .'"': '';

	$out = '';
	if ( $loop ){
		$out .= '<div class="press-carousel2 owl-carousel '.$uniq_id.'"'.$autoplay.$sldloop.$dotclr.$actdotclr.'>';
		foreach ( $loop as $item ) {
			$out .= '<div class="press-carousel__item">';
				$out .= '<div class="col-12 col-lg-4">';
					if ( isset ( $item['linkcheck'] ) =='yes' ) {
						$link = ( $item['link'] == '||' ) ? '' : $item['link'];
						$link = vc_build_link( $link );
						$title = $link['title'];
						$target = $link['target'];
						$href = $link['url'];
						$out .= '<a href="'.$href .'" title="'.$title.'" class="press-carousel__item-logo">';
							$out .= cryptoland_img( $item['img'], $imgclass='', $imgwidth='', $imgheight='', $data='' );
						$out .= '</a>';
					}else{
						$out .= '<div class="press-carousel__item-logo">';
							$out .= cryptoland_img( $item['img'], $imgclass='', $imgwidth='', $imgheight='', $data='' );
						$out .= '</div>';
					}
				$out .= '</div>';
				$out .= '<div class="col-lg-8">';
					$out .= cryptoland_element( $item['desc'], $tag='p', $class='', $pclr, $psz, $plh, $anim='' );
				$out .= '</div>';
			$out .= '</div>';
		}
		$out .= '</div>';
        if ( function_exists( 'vc_is_inline' ) && vc_is_inline() ) {
            $aplay = $autoplay == 'yes' ? 'true': 'false';
            $sloop = $sldloop == true ? 'true': 'false';
            $out .= '<script type="text/javascript">if (typeof window.jQuery !== "undefined"){jQuery(document).ready(function($){$(".'.$uniq_id.'").owlCarousel({items:1,loop:'.$sloop.',autoplay:'.$aplay.',dots:true,dotClass:\'owl-dot\'});});}</script>';
        }
	}
	return $out;
}
add_shortcode('cryptoland_press_carousel2_shortcode', 'cryptoland_press_carousel2');

/*-----------------------------------------------------------------------------------*/
/*	PRESS ABOUT HOME 4
/*-----------------------------------------------------------------------------------*/
function cryptoland_press_carousel4( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"img" => '',
	"desc" => '',
	"addanim" => '',
	"anim" => '',
    "href" => '',
    "target" => '',
	"delay" => '',
	//custom style
	"href" => '',
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	), $atts) );
	$delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
	$anim = $addanim == true ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';
	$out = '';
	$out .= '<div class="press__item home4"'.$anim.'>';
		$out .= '<div class="press__item-logo">';
        if($href){
    		$out .= '<a href="'.esc_attr($href).'" target="'.esc_attr($target).'">';
        }
    		$out .= cryptoland_img( $img, $imgclass='', $imgwidth='', $imgheight='', $data='' );
        if($href){
            $out .= '<a>';
        }
		$out .= '</div>';
		$out .= cryptoland_element( $desc, $tag='p', $class='', $pclr, $psz, $plh, $anim='' );
	$out .= '</div>';
	return $out;
}
add_shortcode('cryptoland_press_carousel4_shortcode', 'cryptoland_press_carousel4');

/*-----------------------------------------------------------------------------------*/
/*	PRESS ABOUT HOME 6
/*-----------------------------------------------------------------------------------*/
function cryptoland_press_about6( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"img" => '',
	"addanim" => '',
	"anim" => '',
	"delay" => '',
	"link" => '',
	//custom style
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	), $atts) );

	$link = ( $link == '||' ) ? '' : $link;
	$link = vc_build_link( $link );
	$title = $link['title'];
	$target = $link['target'];
	$href = $link['url'];

	$href = $href   != '' ? ' href="'.$href.'"' : '';
	$target = $target != '' ? ' target="'.$target.'"' : '';
	$dtitle = $title  != '' ? ' title="'.$href.'"' : '';
	$delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
	$anim = $addanim == true ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';
	$out = '';
	$out .= '<a'.$href.$target.$dtitle.' class="press__item"'.$anim.'>';
		$out .= '<div class="press__item-bg"></div>';
		$out .= cryptoland_img( $img, $imgclass='', $imgwidth='', $imgheight='', $data='' );
	$out .= '</a>';


	return $out;
}
add_shortcode('cryptoland_press_about6_shortcode', 'cryptoland_press_about6');

/*-----------------------------------------------------------------------------------*/
/*	NEWS EVENT HOME 4
/*-----------------------------------------------------------------------------------*/
function cryptoland_new_event4( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"img" => '',
	"title" => '',
	"date" => '',
	"link" => '',
	"addanim" => '',
	"anim" => '',
	"delay" => '',
	//title style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	//date style
	"pclr" => '',
	"psz" => '',
	"plh" => '',
	), $atts) );
	$delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
	$anim = $addanim == true ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';
	$link	= ( $link == '||' ) ? '' : $link;
	$link	= vc_build_link( $link );
	$atitle	= $link['title'];
	$target	= $link['target'];
	$href	= $link['url'];
	$target = $target != '' ? ' target="'.$target.'"' : '';
	$atitle = $atitle != '' ? ' title="'.$atitle.'"' : '';
	$out = '';
	$out .= '<a href="'.$href.'"'.$target.$atitle.' class="new"'.$anim.'>';
		$out .= '<div class="new__img">';
		$out .= cryptoland_img( $img, $imgclass='', $imgwidth='', $imgheight='', $data='' );
		$out .= '</div>';
		$out .= cryptoland_element( $title, $tag='div', $class='new__title', $pclr, $psz, $plh, $anim='' );
		$out .= cryptoland_element( $date, $tag='div', $class='new__data', $pclr, $psz, $plh, $anim='' );
	$out .= '</a>';

	return $out;
}
add_shortcode('cryptoland_new_event4_shortcode', 'cryptoland_new_event4');

/*-----------------------------------------------------------------------------------*/
/*	BUTTON INLINE-BLOCK
/*-----------------------------------------------------------------------------------*/
function cryptoland_btn_inline( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"btntype" => '',
	"btnloop" => '',
	"title" => '',
	"imgtype" => '',
	"icon" => '',
	"img" => '',
	"btndef" => '',
	"btnchvr" => '',
	"btncbg" => '',
	"link" => '',
	//btn style
	"btnbg" => '',
	"btnsz" => '',
	//custom style
	"tclr" => '',
	"tsz" => '',
	"tlh" => '',
	//icon
	"iclr" => '',
	"ihvrclr" => '',
	"isz" => '',
	), $atts) );

	$loop = (array) vc_param_group_parse_atts($btnloop);
	$loop2 = (array) vc_param_group_parse_atts($btndef);

	$out = '';


		if ($btntype == 'text' ){
		$out .= '<div class="promo__links-wrap">';
			foreach ( $loop as $item ) {
				$link = ! empty ( $item['link'] ) ? $item['link'] : '';
				$link = vc_build_link( $link );
				$atitle = $link != '' ? $link['title'] : '';
				$target = $link != '' ? $link['target'] : '';
				$href = $link != '' ? $link['url'] : '';
				$href = isset($item['addvideo']) == true ? esc_url($item['videourl']) : $href;
				$videoclass = isset($item['addvideo']) == true ? ' popup-youtube' : '';
				$out .= '<a href="'.$href .'" class="promo__link'.$videoclass.'" title="'.$atitle.'" >';
					if( $item['imgtype'] == 'icon' ){
					$out .= '<i data-hvrbg=""  data-hvrclr="'.$ihvrclr.'" style="font-size:'.$isz.';color:'.$iclr.';">';
					$out .= '<span class="'.$item['icon'].'"></span>';
					$out .= '</i>';
					}else{
					$out .= cryptoland_img( $item['img'], $imgclass='', $imgwidth='', $imgheight='', $data='' );
					}
					$out .= cryptoland_element( $item['title'], $tag='span', $class='', $tclr, $tsz, $tlh, $anim='' );
				$out .= '</a>';
			}
			$out .= '</div>';
		}else{
			$link = ! empty ( $item['link'] ) ? $item['link'] : '';
			$link = vc_build_link( $link );
			$atitle = $link != '' ? $link['title'] : '';
			$target = $link != '' ? $link['target'] : '';
			$href = $link != '' ? $link['url'] : '';
			$href = isset($item['addvideo']) == true ? esc_url($item['videourl']) : $href;
			$videoclass = isset($item['addvideo']) == true ? ' popup-youtube' : '';

			$out .= '<div class="promo__btns-wrap">';
			foreach ( $loop2 as $item ) {
				$link = ! empty ( $item['link'] ) ? $item['link'] : '';
				$link = vc_build_link( $link );
				$atitle = $link != '' ? $link['title'] : '';
				$target = $link != '' ? $link['target'] : '';
				$href = $link != '' ? $link['url'] : '';
				$href = isset($item['addvideo']) == true ? esc_url($item['videourl']) : $href;
				$videoclass = isset($item['addvideo']) == true ? ' popup-youtube' : '';

				$btnsz = isset($item['btnsz']) != '' ? $item['btnsz'] : 'btn--medium';
				$btnbg = (isset($item['btnbg']) != '' ) || (isset($item['btnbg']) != 'custom') ? $item['btnbg'] : 'btn--white';
				$btnhvrbg = isset($item['btnbg']) == 'custom' && isset($item['btnchvr']) != '' ? ' data-hvrbg="'.$item['btnchvr'].'"' : '';
				$btncbg = isset($item['btnbg']) == 'custom' && isset($item['btncbg']) != '' ? ' data-bgclr="'.$item['btncbg'].'"' : '';

				$out .= '<a href="'.$href .'"'.$btnhvrbg.$btncbg.' class="btn '.$btnsz.' '.$btnbg.$videoclass.'" title="'.$title.'">'.$item['title'].'</a>';
			}
			$out .= '</div>';
		}
	return $out;
}
add_shortcode('cryptoland_btn_inline_shortcode', 'cryptoland_btn_inline');

/*-----------------------------------------------------------------------------------*/
/*	BUTTON HOME 3
/*-----------------------------------------------------------------------------------*/
function cryptoland_button3( $atts, $content = null ) {
   extract( shortcode_atts(array(
   	//buton
   	"link" => '',
   	"btnbg" => '',
   	"btnsize" => '',
   	"btntxt" => '',
   	"btnpos" => '',
   	"mdbtnpos" => '',
   	"smbtnpos" => '',
   	"xsbtnpos" => '',
   	"addanim" => '',
   	"anim" => '',
   	"delay" => '',
	//btn customize
	"btncbg" => '',
	"btnchvrbg" => '',
	"btnclr" => '',
	"btnchvrclr" => '',
	"grad1" => '',
	"grad2" => '',
	//css design
	"css" => '',

	), $atts) );

	$delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
	$anim = $addanim == true ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';

	$btnbg = $btnbg != ''  ? ' '.$btnbg : ' btn--orange';
	$btnsize = $btnsize != ''  ? ' '.$btnsize : ' btn--medium';
	$btntxt = $btntxt != ''  ? ' '.$btntxt : '';
	$btnpos = $btnpos != ''  ? ' '.$btnpos  : ' btn--left';
	$mdbtnpos = $mdbtnpos != ''  ? ' '.$mdbtnpos : '';
	$smbtnpos = $smbtnpos != ''  ? ' '.$smbtnpos : '';
	$xsbtnpos = $xsbtnpos != ''  ? ' '.$xsbtnpos : '';
	//btn customize
	$btnbg = $btnbg == ' custom' ? ' custom-clr' : $btnbg;
	$btnbg = $grad1 !='' && $grad2 !='' ? '' : $btnbg;
	$btncbg = $btncbg != '' ? ' data-bg="'.$btncbg.'"' : '';
	$btnchvrbg = $btnchvrbg  != '' ? ' data-hvrbg="'.$btnchvrbg.'"' : '';
	$btnclr = $btnclr != '' ? ' data-clr="'.$btnclr.'"' : '';
	$btnchvrclr = $btnchvrclr != '' ? ' data-hvrclr="'.$btnchvrclr.'"' : '';

	$btndata = $btncbg . $btnchvrbg . $btnclr . $btnchvrclr;
    $gradiantbg = '';
    if( $grad1 !='' && $grad2 !='' ){
        $gradiantbg = 'style="background: linear-gradient(90deg, '.$grad1.' 0%, '.$grad2.' 100%);"';
    }

	// btn
	$link = ( $link == '||' ) ? '' : $link;
	$link = vc_build_link( $link );
	$href = $link['url'];
	$title = $link['title'];
	$target = $link['target'];
	$rel = $link['rel'];
	$target = $target != '' ? ' target="'.$target.'" ' :'';
	$rel = $rel != '' ? ' rel="'.$rel.'" ' :'';
	$href = $href != '' ? ' href="'.$href.'" ' :'';

	$out = '';
	if ( $title !='' ){
		$out .= '<div class="btn-wrapper'.$btnpos.$mdbtnpos.$smbtnpos.$xsbtnpos.'"'.$anim.'>';
		$out .= '<a '.$href.$target.$rel.' class="btn btn-cases '.$btnbg.$btnsize.$btntxt.'"'.$btndata.' '.$gradiantbg.'><span>'.$title.'</span></a>';
		$out .= '</div>';
	}
	return $out;
}
add_shortcode('cryptoland_button3_shortcode', 'cryptoland_button3');


/*-----------------------------------------------------------------------------------*/
/*	BLOG CAROUSEL STYLE PAGE  1
/*-----------------------------------------------------------------------------------*/
function cryptoland_blog( $atts, $content = null ) {
    extract( shortcode_atts(array(
        'build_query'  => '',
        'excerptsz'  => '25',
        'posts_per_page'=> '',
        //control options
        'hidetitle'  => '',
        'hidetax'  => '',
        'hidedate'  => '',
        'hidetext'  => '',
        //animation options
        'addanim'  => '',
        'anim'  => '',
        'delay'  => '',
        //slider options
        "autoplay" => '',
        "sldloop" => '',
        "itemsmd" => '',
        "dotclr" => '',
        "actdotclr" => '',
        //title style
        'tclr' => '',
        'tsz' => '',
        'tlh' => '',
        //category style
        'cclr' => '',
        'csz' => '',
        'clh' => '',
        //date style
        'dtclr' => '',
        'dtsz' => '',
        'dtlh' => '',
        //content style
        'pclr' => '',
        'psz' => '',
        'plh' => '',
    ), $atts));

    $uniq_id = 'carousel_'.uniqid();

    global $post,$wp_query;

    list($args) = vc_build_loop_query($build_query);

    $autoplay = $autoplay == 'yes' ? ' data-autoplay="'.$autoplay .'"': '';
    $itemsmd = $itemsmd != '' ? ' data-show-itemsmd="'.$itemsmd.'"': '';
    $dotclr = $dotclr != '' ? ' data-dotclr="'.$dotclr.'"': '';
    $actdotclr = $actdotclr != '' ? ' data-active-dotclr="'.$actdotclr .'"': '';
    $sldloop = $sldloop == true ? ' data-loop="'.$sldloop.'"': ' data-loop="false"';
    $showpost = isset($args['posts_per_page']) != '' ? ' data-post-per-page="'.$args['posts_per_page'].'"': ' data-post-per-page="All"';

    if(is_front_page()){
        $args['paged'] = (get_query_var('page')) ? get_query_var('page') : 1;
    } else {
        $args['paged'] = (get_query_var('paged')) ? get_query_var('paged') : 1;
    }

    $out = '';

    query_posts( $args );
    $delayaos = 0;
    if( have_posts() ) {
        $out .= '<div class="news-carousel owl-carousel '.$uniq_id.'"'.$autoplay.$sldloop.$dotclr.$actdotclr.$showpost.$itemsmd.'>';
            while ( have_posts() ) {
                the_post();

                $taxonomy = strip_tags( get_the_term_list($post->ID, 'category', '', ' / ', '') );
                $category_id = get_cat_ID( $taxonomy );

                $delay = $delay != '' ? ' data-aos-delay="'.esc_attr(((int)$delay)+((int)$delayaos)).'"' : '';
                $addanim = $addanim == true ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';

                $out .= '<a href="'.get_permalink().'" class="news-carousel__item"'.$addanim.'>';
                if ( $hidetax != true ){
                    $out .= cryptoland_element( $taxonomy, $tag='div', $class='news-carousel__item-subtitle', $cclr, $csz, $clh, $anim='' );
                }
                if ( $hidetitle != true ){
                    $out .= cryptoland_element( get_the_title(), $tag='h3', $class='news-carousel__item-title', $tclr, $tsz, $tlh, $anim='' );
                }
                if ( $hidetext != true ){
                    $out .= cryptoland_element( cryptoland_excerpt_limit($excerptsz+1), $tag='p', $class='', $pclr, $psz, $plh, $anim='' );
                }
                if ( $hidedate != true ){
                    $out .= cryptoland_element( get_the_time('F j, Y'), $tag='div', $class='news-carousel__item-data', $dtclr, $dtsz, $dtlh, $anim='' );
                }
                $out .= '</a>';
            }
            wp_reset_query();
        $out.='</div>';
        if ( function_exists( 'vc_is_inline' ) && vc_is_inline() ) {
            $aplay = $autoplay == 'yes' ? 'true': 'false';
            $sloop = $sldloop == true ? 'true': 'false';
            $items = $itemsmd != '' ? $itemsmd : '3';
            $out .= '<script type="text/javascript">if (typeof window.jQuery !== "undefined"){jQuery(document).ready(function($){$(".'.$uniq_id.'").owlCarousel({items:'.$items.',margin:30,loop:'.$sloop.',autoplay:'.$aplay.',dots:true,dotClass:\'owl-dot\',responsive:{0:{items:1},768:{items:2},992:{items:'.$items.'}}});});}</script>';
        }
    }
    return $out;
}
add_shortcode('cryptoland_blog_shortcode', 'cryptoland_blog');

/*-----------------------------------------------------------------------------------*/
/*	BLOG CAROUSEL STYLE PAGE  2
/*-----------------------------------------------------------------------------------*/
function cryptoland_blog2( $atts, $content = null ) {
    extract( shortcode_atts(array(
        'build_query'  => '',
        'excerptsz'  => '25',
        //control options
        'hidetitle'  => '',
        'hidetax'  => '',
        'hidedate'  => '',
        'hidetext'  => '',
        //animation options
        'addanim'  => '',
        'anim'  => '',
        'delay'  => '',
        //thumb options
        'hidethumb'  => '',
        'imgw'  => '',
        'imgh'  => '',
        //slider options
        "autoplay" => '',
        "sldloop" => '',
        "itemsmd" => '',
        "dotclr" => '',
        "actdotclr" => '',
        //title style
        'tclr' => '',
        'tsz' => '',
        'tlh' => '',
        //category style
        'cclr' => '',
        'csz' => '',
        'clh' => '',
        //date style
        'dtclr' => '',
        'dtsz' => '',
        'dtlh' => '',
        //content style
        'pclr' => '',
        'psz' => '',
        'plh' => '',
    ), $atts));

    $uniq_id = 'carousel_'.uniqid();

    global $post,$wp_query;

    list($args) = vc_build_loop_query($build_query);

    $autoplay = $autoplay == 'yes' ? ' data-autoplay="'.$autoplay .'"': '';
    $dotclr = $dotclr != '' ? ' data-dotclr="'.$dotclr.'"': '';
    $actdotclr = $actdotclr != '' ? ' data-active-dotclr="'.$actdotclr .'"': '';
    $itemsmd = $itemsmd != '' ? ' data-show-itemsmd="'.$itemsmd.'"': '';
    $sldloop = $sldloop == true ? ' data-loop="'.$sldloop.'"': ' data-loop="false"';
    $showpost = isset($args['posts_per_page']) != '' ? ' data-post-per-page="'.$args['posts_per_page'].'"': ' data-post-per-page="All"';

    if(is_front_page()){
        $args['paged'] = (get_query_var('page')) ? get_query_var('page') : 1;
    } else {
        $args['paged'] = (get_query_var('paged')) ? get_query_var('paged') : 1;
    }

    $out = '';

    query_posts( $args );
    $delayaos = 0;
    if( have_posts() ) {
        $out .= '<div class="news-carousel owl-carousel '.$uniq_id.'"'.$autoplay.$sldloop.$dotclr.$actdotclr.$showpost.$itemsmd.'>';
        while ( have_posts() ) {
            the_post();

            $taxonomy = strip_tags( get_the_term_list($post->ID, 'category', '', ' / ', '') );
            $category_id = get_cat_ID( $taxonomy );

            $delay = $delay != '' ? ' data-aos-delay="'.esc_attr(((int)$delay)+((int)$delayaos)).'"' : '';
            $addanim = $addanim == true ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';

            $out .= '<a href="'.get_permalink().'" class="news-carousel__item"'.$addanim.'>';
                if ( has_post_thumbnail() && $hidethumb != true ) {
                    $img_w = $imgw ? $imgw : 450;
                    $img_h = $imgh ? $imgh : 350;

                    $img_url_out = wp_get_attachment_url( get_post_thumbnail_id(), 'full' );
                    $img_url = cryptoland_aq_resize( $img_url_out, $img_w, $img_h , true, true, true );
                    $out .= '<img class="news-carousel__item-img" src="'.esc_url($img_url).'" alt="'.get_the_title().'">';
                }
                $out .= '<div class="news-carousel__item-body">';
                    if ( $hidetax != true ) {
                        $out .= cryptoland_element( $taxonomy, $tag='div', $class='news-carousel__item-subtitle', $cclr, $csz, $clh, $anim='' );
                    }
                    if ( $hidetitle != true ) {
                        $out .= cryptoland_element( get_the_title(), $tag='h3', $class='news-carousel__item-title', $tclr, $tsz, $tlh, $anim='' );
                    }
                    if ( $hidetext != true ) {
                        $out .= cryptoland_element( cryptoland_excerpt_limit($excerptsz+1), $tag='p', $class='', $pclr, $psz, $plh, $anim='' );
                    }
                    if ($hidedate != true ) {
                        $out .= cryptoland_element( get_the_time('F j, Y'), $tag='div', $class='news-carousel__item-data', $dtclr, $dtsz, $dtlh, $anim='' );
                    }
                $out.='</div>';
            $out .= '</a>';
            //$delayaos =+ $delay;
        }
        wp_reset_query();
        $out.='</div>';
    }
    if ( function_exists( 'vc_is_inline' ) && vc_is_inline() ) {
        $aplay = $autoplay == 'yes' ? 'true': 'false';
        $sloop = $sldloop == true ? 'true': 'false';
        $items = $itemsmd != '' ? $itemsmd : '3';
        $out .= '<script type="text/javascript">if (typeof window.jQuery !== "undefined"){jQuery(document).ready(function($){$(".'.$uniq_id.'").owlCarousel({items:'.$items.',margin:30,loop:'.$sloop.',autoplay:'.$aplay.',dots:true,dotClass:\'owl-dot\',responsive:{0:{items:1},768:{items:2},992:{items:'.$items.'}}});});}</script>';
    }
    return $out;
}
add_shortcode('cryptoland_blog2_shortcode', 'cryptoland_blog2');

/*-----------------------------------------------------------------------------------*/
/*	SOCIAL ICON PAGE STYLE 1
/*-----------------------------------------------------------------------------------*/

function cryptoland_social( $atts, $content = null ) {
    extract( shortcode_atts(array(
        "social" => '',
        "icon" => '',
        "link" => '',
        'clr' => '',
        'hvrclr' => '',
        'bgclr' => '',
        'hvrbg' => '',
        'isz' => '',
    ), $atts) );

    $loop = (array) vc_param_group_parse_atts($social);

    $clr = $clr != '' ? ' color:'.$clr.';border-color:'.$clr.';' : '';
    $bgclr = $bgclr != '' ? ' background-color:'.$bgclr.';' : '';
    $hvrclr = $hvrclr != '' ? ' data-hvrclr="'.$hvrclr.'"' : '';
    $hvrbg = $hvrbg != '' ? ' data-hvrbg="'.$hvrbg.'"' : '';
    $style = $clr != '' || $bgclr != '' ? ' style="'.$clr.$bgclr.'"' : '';

    $out = '';
    $out .= '<ul class="social-list">';
        foreach ( $loop as $item ) {
            $link = ( $item['link'] == '||' ) ? '' : $item['link'];
            $link = vc_build_link( $link );
            $title = $link['title'];
            $target = $link['target'] ? ' target='.$link['target'].'"' : '';
            $href = $link['url'];
            $out .= '<li class="social-list__item">';
                $out .= '<a href="'.$href.'" title="'.$title.'"'.$target.' class="social-list__link"'.$style.$hvrclr.$hvrbg.'>';
                    $out .= '<i class="font-icon '.$item['icon'].'"></i>';
                $out .= '</a>';
            $out .= '</li>';
        }
    $out .= '</ul>';
    return $out;
}
add_shortcode('cryptoland_social_shortcode', 'cryptoland_social');

/*-----------------------------------------------------------------------------------*/
/*	CHART HOME 2
/*-----------------------------------------------------------------------------------*/

function cryptoland_chart( $atts, $content = null ) {
    extract( shortcode_atts(array(
        "chart" => '',
        "clr" => '',
        "label" => '',
        "value" => '',
        "desc" => '',
        "chartbrd" => '',
        "chartbrdclr" => '',
        "chartcutout" => '',
    //custom style
    ), $atts) );

    $loop = (array) vc_param_group_parse_atts($chart);
    $chartbrdclr = $chartbrdclr != '' ? ' data-chart-brdclr="'.esc_attr($chartbrdclr).'"' : '';
    $chartbrd = $chartbrd != '' ? ' data-chart-brd="'.(int)esc_attr($chartbrd).'"'.$chartbrdclr : ' data-chart-brd="0"';
    $chartcutout = $chartcutout != '' ? ' data-chart-cutout="'.(int)esc_attr($chartcutout).'"' : '';
    $out = '';

    $out .= '<div class="chart">';
        $out .= '<div class="chart__wrap"'.$chartbrd.$chartcutout.'>';
            $out .= '<canvas id="myChart" class="myChart" width="400" height="400"></canvas>';
        $out .= '</div>';
        $out .= '<ul class="chart__legend">';
            foreach ( $loop as $item ) {
                $out .= '<li data-chart-label="'.$item['label'].'" data-chart-color="'.$item['clr'].'" data-chart-value="'.$item['value'].'">';
                    $out .= '<span style="background: '.$item['clr'].'"></span>';
                    $out .= ''.$item['value'].'% '.$item['desc'].'';
                $out .= '</li>';
            }
        $out .= '</ul>';
    $out .= '</div>';
    if ( function_exists( 'vc_is_inline' ) && vc_is_inline() ) {
        $out .= '<script type="text/javascript">jQuery(document).ready(function($){var chartdef = $("#myChart").length;if(chartdef > 0){var chart_count = $(".chart__legend li").size(),lbl = [],clr = [],val = [];for (var i = 1; i < chart_count+1; i++) {var get_label = $(".chart__legend li:nth-child("+i+")").attr("data-chart-label");var get_color = $(".chart__legend li:nth-child("+i+")").attr("data-chart-color");var get_value = $(".chart__legend li:nth-child("+i+")").attr("data-chart-value");lbl.push(get_label);clr.push(get_color);val.push(get_value);}var get_cutout = $(".chart__wrap").attr("data-chart-cutout");var get_cutout = (typeof get_cutout !== typeof undefined && get_cutout !== false) ? parseInt(get_cutout): "";var get_brd =  $(".chart__wrap").attr("data-chart-brd");var get_brd = (typeof get_brd !== typeof undefined && get_brd !== false ) ? parseInt(get_brd) : 0;var get_brdclr = $(".chart__wrap").attr("data-chart-brdclr");var get_brdclr = (typeof get_brdclr !== typeof undefined && get_brdclr !== false) ? get_brdclr : \'transparent\';var ctx = document.getElementById("myChart");var myChart = new Chart(ctx, {type:\'doughnut\',data:{labels:lbl,datasets:[{label:\'# of Votes\',data: val,backgroundColor:clr,borderWidth:parseInt(get_brd),borderColor:\'+get_brdclr+\'}]},options: {legend: {display: false},cutoutPercentage:get_cutout}});}})</script>';
    }
    return $out;

}
add_shortcode('cryptoland_chart_shortcode', 'cryptoland_chart');

/*-----------------------------------------------------------------------------------*/
/*	CHART HOME 3
/*-----------------------------------------------------------------------------------*/

function cryptoland_chart3( $atts, $content = null ) {
  extract( shortcode_atts(array(
    "btitle" => '',
    "subtitle" => '',
    "title" => '',
	//chart
    "chartbg" => '',
    "chartbrd" => '',
    "chartbrdclr" => '',
    "chartcutout" => '',
    "charttitle" => '',
    "chartdesc" => '',
	//chart loop
    "chart" => '',
    "clr" => '',
    "label" => '',
    "value" => '',
    "desc" => '',
    "barclr" => '',
    "custombarclr1" => '',
    "custombarclr2" => '',
	//bigtitle style
	'btclr' => '',
	'btsz' => '',
	'btlh' => '',
	//subtitle style
	'stclr' => '',
	'stsz' => '',
	'stlh' => '',
	//title style
	'tclr' => '',
	'tsz' => '',
	'tlh' => '',
	//chart title style
	'chtclr' => '',
	'chtsz' => '',
	'chtlh' => '',
	//chart desc style
	'chpclr' => '',
	'chpsz' => '',
	'chplh' => '',
	//content style
	'chlclr' => '',
	'chlsz' => '',
	'chllh' => '',
	), $atts) );

	$loop = (array) vc_param_group_parse_atts($chart);
	$chartbrdclr = $chartbrdclr != '' ? ' data-chart-brdclr="'.esc_attr($chartbrdclr).'"' : '';
	$chartbrd = $chartbrd != '' ? ' data-chart-brd="'.(int)esc_attr($chartbrd).'"'.$chartbrdclr : ' data-chart-brd="0"';
	$chartcutout = $chartcutout != '' ? ' data-chart-cutout="'.(int)esc_attr($chartcutout).'"' : '';

	$out = '';

		$out .= '<div class="data-token-section">';
			$out .= '<div class="container">';
				$out .= '<div class="row">';
					$out .= '<div class="col">';
						$out .= '<div class="section-header section-header--animated section-header--medium-margin section-header--center">';
							$out .= cryptoland_element( $subtitle, $tag='h4', $class='', $stclr, $stsz, $stlh, $anim='' );
							$out .= cryptoland_element( $title, $tag='h2', $class='', $tclr, $tsz, $tlh, $anim='' );
							$out .= cryptoland_element( $btitle, $tag='div', $class='bg-title', $btclr, $btsz, $btlh, $anim='' );
						$out .= '</div>';
					$out .= '</div>';
				$out .= '</div>';
				$out .= '<div class="row chart__row align-items-center">';
					$out .= '<div class="col-lg-6">';
						$out .= '<div class="chart">';
							$out .= cryptoland_img( $chartbg, $imgclass='chart__bg', $imgwidth='', $imgheight='', $data='' );
							$out .= '<div class="chart__wrap"'.$chartbrd.$chartcutout.'>';
								$out .= '<canvas id="myChart" class="myChart" width="400" height="400"></canvas>';
							$out .= '</div>';
						$out .= '</div>';
					$out .= '</div>';
					$out .= '<div data-aos="fade-left" class="col-lg-6 token-data__animated-content">';
						$out .= cryptoland_element( $charttitle, $tag='div', $class='chart__title', $chtclr, $chtsz, $chtlh, $anim='' );
						$out .= cryptoland_element( $chartdesc, $tag='p', $class='chart__text', $chpclr, $chpsz, $chplh, $anim='' );
						$out .= '<ul class="chart__legend">';
						foreach ( $loop as $item ) {
							if(isset( $item['value']) != '' ){

								$customclr = (isset($item['barclr']) == true)&& (isset($item['custombarclr1']) != '' && isset($item['custombarclr2']) != '') ? 'background-image: -webkit-gradient(linear,left top,right top,from('.$item['custombarclr1'].'),to('.$item['custombarclr2'].'));background-image: -webkit-linear-gradient(left,'.$item['custombarclr1'].' 0,'.$item['custombarclr2'].' 100%);background-image: -o-linear-gradient(left,'.$item['custombarclr1'].' 0,'.$item['custombarclr2'].' 100%);background-image: linear-gradient(to right,'.$item['custombarclr1'].' 0,'.$item['custombarclr2'].' 100%);' : '';

								$customfinal= $customclr != '' ? $customclr : ' background:'.$item['clr'];
							$out .= '<li data-chart-label="'.$item['label'].'" data-chart-color="'.$item['clr'].'" data-chart-value="'.$item['value'].'">';
								$out .= '<span style="width: '.$item['value'].'%;'.$customfinal.'"></span>';
								$out .= cryptoland_element( $item['value'].'% '.$item['desc'], $tag='', $class='', $chlclr, $chlsz, $chllh, $anim='' );
							$out .= '</li>';
							}
						}

						$out .= '</ul>';
					$out .= '</div>';
				$out .= '</div>';
			$out .= '</div>';
		$out .= '</div>';
        if ( function_exists( 'vc_is_inline' ) && vc_is_inline() ) {
            $out .= '<script type="text/javascript">jQuery(document).ready(function($){var chartdef = $("#myChart").length;if(chartdef > 0){var chart_count = $(".chart__legend li").size(),lbl = [],clr = [],val = [];for (var i = 1; i < chart_count+1; i++) {var get_label = $(".chart__legend li:nth-child("+i+")").attr("data-chart-label");var get_color = $(".chart__legend li:nth-child("+i+")").attr("data-chart-color");var get_value = $(".chart__legend li:nth-child("+i+")").attr("data-chart-value");lbl.push(get_label);clr.push(get_color);val.push(get_value);}var get_cutout = $(".chart__wrap").attr("data-chart-cutout");var get_cutout = (typeof get_cutout !== typeof undefined && get_cutout !== false) ? parseInt(get_cutout): "";var get_brd =  $(".chart__wrap").attr("data-chart-brd");var get_brd = (typeof get_brd !== typeof undefined && get_brd !== false ) ? parseInt(get_brd) : 0;var get_brdclr = $(".chart__wrap").attr("data-chart-brdclr");var get_brdclr = (typeof get_brdclr !== typeof undefined && get_brdclr !== false) ? get_brdclr : \'transparent\';var ctx = document.getElementById("myChart");var myChart = new Chart(ctx, {type:\'doughnut\',data:{labels:lbl,datasets:[{label:\'# of Votes\',data: val,backgroundColor:clr,borderWidth:parseInt(get_brd),borderColor:\'+get_brdclr+\'}]},options: {legend: {display: false},cutoutPercentage:get_cutout}});}})</script>';
        }
	return $out;
}
add_shortcode('cryptoland_chart3_shortcode', 'cryptoland_chart3');

/*-----------------------------------------------------------------------------------*/
/*	CHART HOME 4
/*-----------------------------------------------------------------------------------*/

function cryptoland_chart4( $atts, $content = null ) {
  extract( shortcode_atts(array(
	"subtitle" => '',
    "thintitle" => '',
    "title" => '',
	//chart
    "chartbrd" => '',
    "chartbrdclr" => '',
    "chartcutout" => '',
    "charttitle" => '',
	//chart loop
    "chart" => '',
    "clr" => '',
    "label" => '',
    "value" => '',
    "desc" => '',
	//animation
    "addanim" => '',
    "anim" => '',
    "delay" => '',
    "addanim2" => '',
    "anim2" => '',
    "delay2" => '',
	//subtitle style
	'stclr' => '',
	'stsz' => '',
	'stlh' => '',
	//title style
	'tclr' => '',
	'tsz' => '',
	'tlh' => '',
	//chart title style
	'chtclr' => '',
	'chtsz' => '',
	'chtlh' => '',
	//chart desc style
	'chpclr' => '',
	'chpsz' => '',
	'chplh' => '',
	), $atts) );

	$loop = (array) vc_param_group_parse_atts($chart);
	$chartbrdclr = $chartbrdclr != '' ? ' data-chart-brdclr="'.esc_attr($chartbrdclr).'"' : '';
	$chartbrd = $chartbrd != '' ? ' data-chart-brd="'.(int)esc_attr($chartbrd).'"'.$chartbrdclr : ' data-chart-brd="0"';
	$chartcutout = $chartcutout != '' ? ' data-chart-cutout="'.(int)esc_attr($chartcutout).'"' : '';

	$delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
	$anim = $addanim == true && $anim != '' ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';

	$delay2 = $delay2 != '' ? ' data-aos-delay="'.esc_attr($delay2).'"' : '';
	$addanim2 = $addanim2 == true && $anim2 != '' ? ' data-aos="'.esc_attr($anim2).'"'.$delay2 : '';

	$out = '';
		$out .= '<div class="section distribution">';
			$out .= '<div class="container">';

				$out .= '<div class="row">';
					$out .= '<div class="col">';
						$out .= '<div class="section-header section-header--animated section-header--white section-header--center section-header--big-margin">';
							$out .= cryptoland_element( $subtitle, $tag='h4', $class='', $stclr, $stsz, $stlh, $anim='' );
							$out .= cryptoland_element( '<span>'.$thintitle.'</span> '.$title, $tag='h2', $class='', $tclr, $tsz, $tlh, $anim='' );
						$out .= '</div>';
					$out .= '</div>';
				$out .= '</div>';

				$out .= '<div class="row align-items-center">';
					$out .= '<div '.$anim.' class="col-lg-5">';
						$out .= '<div class="chart-big-wrap">';
							$out .= '<div class="chart">';
								$out .= cryptoland_element( $charttitle, $tag='div', $class='chart__bg', $chtclr, $chtsz, $chtlh, $anim='' );
								$out .= '<div class="chart__wrap"'.$chartbrd.$chartcutout.'>';
									$out .= '<canvas id="myChart" class="myChart"  width="400" height="400"></canvas>';
								$out .= '</div>';
							$out .= '</div>';
						$out .= '</div>';
					$out .= '</div>';

					$out .= '<div class="col-lg-7 ">';
						$out .= '<ul class="chart__legend"'.$addanim2.'>';
						foreach ( $loop as $item ) {
							if(isset( $item['value']) != '' ){
							$out .= '<li data-chart-label="'.$item['label'].'" data-chart-color="'.$item['clr'].'" data-chart-value="'.$item['value'].'">';
								$out .= '<span style="background-color: '.$item['clr'].'"></span>';
								if(isset( $item['desc']) != '' ){
								$out .= cryptoland_element( $item['desc'], $tag='', $class='', $chpclr, $chpsz, $chplh, $anim='' );
								}
							$out .= '</li>';
							}
						}
						$out .= '</ul>';
					$out .= '</div>';
				$out .= '</div>';
			$out .= '</div>';
		$out .= '</div>';
        if ( function_exists( 'vc_is_inline' ) && vc_is_inline() ) {
            $out .= '<script type="text/javascript">jQuery(document).ready(function($){var chartdef = $("#myChart").length;if(chartdef > 0){var chart_count = $(".chart__legend li").size(),lbl = [],clr = [],val = [];for (var i = 1; i < chart_count+1; i++) {var get_label = $(".chart__legend li:nth-child("+i+")").attr("data-chart-label");var get_color = $(".chart__legend li:nth-child("+i+")").attr("data-chart-color");var get_value = $(".chart__legend li:nth-child("+i+")").attr("data-chart-value");lbl.push(get_label);clr.push(get_color);val.push(get_value);}var get_cutout = $(".chart__wrap").attr("data-chart-cutout");var get_cutout = (typeof get_cutout !== typeof undefined && get_cutout !== false) ? parseInt(get_cutout): "";var get_brd =  $(".chart__wrap").attr("data-chart-brd");var get_brd = (typeof get_brd !== typeof undefined && get_brd !== false ) ? parseInt(get_brd) : 0;var get_brdclr = $(".chart__wrap").attr("data-chart-brdclr");var get_brdclr = (typeof get_brdclr !== typeof undefined && get_brdclr !== false) ? get_brdclr : \'transparent\';var ctx = document.getElementById("myChart");var myChart = new Chart(ctx, {type:\'doughnut\',data:{labels:lbl,datasets:[{label:\'# of Votes\',data: val,backgroundColor:clr,borderWidth:parseInt(get_brd),borderColor:\'+get_brdclr+\'}]},options: {legend: {display: false},cutoutPercentage:get_cutout}});}})</script>';
        }
	return $out;
}
add_shortcode('cryptoland_chart4_shortcode', 'cryptoland_chart4');

/*-----------------------------------------------------------------------------------*/
/*	CHART HOME 5
/*-----------------------------------------------------------------------------------*/

function cryptoland_chart5( $atts, $content = null ) {
    extract( shortcode_atts(array(
        "addanim" => '',
        "anim" => '',
        "delay" => '',
        "chartbrd" => '',
        "chartbrdclr" => '',
        "chartcutout" => '',
        "chart" => '',
        "clr" => '',
        "label" => '',
        "value" => '',
        "desc" => '',
    	'chpclr' => '',
    	'chpsz' => '',
    	'chplh' => '',
	), $atts) );

	$loop = (array) vc_param_group_parse_atts($chart);
	$chartbrdclr = $chartbrdclr != '' ? ' data-chart-brdclr="'.esc_attr($chartbrdclr).'"' : '';
	$chartbrd = $chartbrd != '' ? ' data-chart-brd="'.(int)esc_attr($chartbrd).'"'.$chartbrdclr : ' data-chart-brd="0"';
	$chartcutout = $chartcutout != '' ? ' data-chart-cutout="'.(int)esc_attr($chartcutout).'"' : '';
	$delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
	$anim = $addanim == true && $anim != '' ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';
	$out = '';

	$out .= '<div class="chart"'.$anim.'>';
		$out .= '<div class="chart__wrap"'.$chartbrd.$chartcutout.'>';
			$out .= '<canvas id="myChart" class="myChart"  width="400" height="400"></canvas>';
		$out .= '</div>';
		$out .= '<ul class="chart__legend">';
		foreach ( $loop as $item ) {
			if(isset( $item['value']) != '' ){
			$out .= '<li data-chart-label="'.$item['label'].'" data-chart-color="'.$item['clr'].'" data-chart-value="'.$item['value'].'">';
				$out .= '<span style="background-color: '.$item['clr'].'"></span>';
				if(isset( $item['desc']) != '' ){
				$out .= cryptoland_element( $item['value'].'% '.$item['desc'], $tag='', $class='', $chpclr, $chpsz, $chplh, $anim='' );
				}
			$out .= '</li>';
			}
		}
		$out .= '</ul>';
	$out .= '</div>';
    if ( function_exists( 'vc_is_inline' ) && vc_is_inline() ) {
        $out .= '<script type="text/javascript">jQuery(document).ready(function($){var chartdef = $("#myChart").length;if(chartdef > 0){var chart_count = $(".chart__legend li").size(),lbl = [],clr = [],val = [];for (var i = 1; i < chart_count+1; i++) {var get_label = $(".chart__legend li:nth-child("+i+")").attr("data-chart-label");var get_color = $(".chart__legend li:nth-child("+i+")").attr("data-chart-color");var get_value = $(".chart__legend li:nth-child("+i+")").attr("data-chart-value");lbl.push(get_label);clr.push(get_color);val.push(get_value);}var get_cutout = $(".chart__wrap").attr("data-chart-cutout");var get_cutout = (typeof get_cutout !== typeof undefined && get_cutout !== false) ? parseInt(get_cutout): "";var get_brd =  $(".chart__wrap").attr("data-chart-brd");var get_brd = (typeof get_brd !== typeof undefined && get_brd !== false ) ? parseInt(get_brd) : 0;var get_brdclr = $(".chart__wrap").attr("data-chart-brdclr");var get_brdclr = (typeof get_brdclr !== typeof undefined && get_brdclr !== false) ? get_brdclr : \'transparent\';var ctx = document.getElementById("myChart");var myChart = new Chart(ctx, {type:\'doughnut\',data:{labels:lbl,datasets:[{label:\'# of Votes\',data: val,backgroundColor:clr,borderWidth:parseInt(get_brd),borderColor:\'+get_brdclr+\'}]},options: {legend: {display: false},cutoutPercentage:get_cutout}});}})</script>';
    }
	return $out;
}
add_shortcode('cryptoland_chart5_shortcode', 'cryptoland_chart5');

/*-----------------------------------------------------------------------------------*/
/*	TOKEN LIST HOME 2
/*-----------------------------------------------------------------------------------*/
function cryptoland_tokenlist( $atts, $content = null ) {
    extract( shortcode_atts(array(
        "tokenlist" => '',
        "label" => '',
        "desc" => '',
        'tclr' => '',
        'tsz' => '',
        'tlh' => '',
        'pclr' => '',
        'psz' => '',
        'plh' => '',
    ), $atts) );

    $loop = (array) vc_param_group_parse_atts($tokenlist);
    $pclr = $pclr != '' ? ' color:'.$pclr.';' : '';
    $psz = $psz != '' ? ' font-size:'.$psz.';' : '';
    $plh = $plh != '' ? ' line-height:'.$plh.';' : '';
    $style = $pclr != '' || $psz != '' || $plh != '' ? ' style="'.$pclr.$psz.$plh.'"' : '';
    $out = '';
    $out .= '<ul class="token__info-list">';
        foreach ( $loop as $item ) {
            $out .= '<li'.$style.'>';
                $out .= cryptoland_element( $item['label'], $tag='span', $class='', $tclr, $tsz, $tlh, $anim='' );
                $out .= $item['desc'];
            $out .= '</li>';
        }
    $out .= '</ul>';
    return $out;
}
add_shortcode('cryptoland_tokenlist_shortcode', 'cryptoland_tokenlist');

/*-----------------------------------------------------------------------------------*/
/*	TOKEN LIST HOME 4
/*-----------------------------------------------------------------------------------*/
function cryptoland_tokenlist4( $atts, $content = null ) {
    extract( shortcode_atts(array(
        "tokenlist" => '',
        "label" => '',
        "desc" => '',
        'tclr' => '',
        'tsz' => '',
        'tlh' => '',
        'pclr' => '',
        'psz' => '',
        'plh' => '',
    ), $atts) );

    $loop = (array) vc_param_group_parse_atts($tokenlist);
    $out = '';
    $out .= '<div class="prices__table">';
        foreach ( $loop as $item ) {
            $out .= '<div class="prices__table-row">';
                $out .= cryptoland_element( $item['label'], $tag='div', $class='prices__table-cell', $tclr, $tsz, $tlh, $anim='' );
                $out .= cryptoland_element( $item['desc'], $tag='div', $class='prices__table-cell', $pclr, $psz, $plh, $anim='' );
            $out .= '</div>';
            }
    $out .= '</div>';
    return $out;
}
add_shortcode('cryptoland_tokenlist4_shortcode', 'cryptoland_tokenlist4');

/*-----------------------------------------------------------------------------------*/
/*	TOKEN LIST HOME 5
/*-----------------------------------------------------------------------------------*/
function cryptoland_tokenlist5( $atts, $content = null ) {
    extract( shortcode_atts(array(
        "liststyle" => '',
        "tokenlist" => '',
        "tokenlist2" => '',
        "label" => '',
        "desc" => '',
        "color" => '',
        'tclr' => '',
        'tsz' => '',
        'tlh' => '',
        'pclr' => '',
        'psz' => '',
        'plh' => '',
    ), $atts) );

    $loop = (array) vc_param_group_parse_atts($tokenlist);
    $loop2 = (array) vc_param_group_parse_atts($tokenlist2);
    $tclr = $tclr != '' ? ' color:'.$tclr.';' : '';
    $tsz = $tsz != '' ? ' font-size:'.$tsz.';' : '';
    $tlh = $tlh != '' ? ' line-height:'.$tlh.';' : '';
    $lstyle = $tclr != '' || $tsz != '' || $tlh != '' ? ' style="'.$tclr.$tsz.$tlh.'"' : '';

    $pclr = $pclr != '' ? ' color:'.$pclr.';' : '';
    $psz = $psz != '' ? ' font-size:'.$psz.';' : '';
    $plh = $plh != '' ? ' line-height:'.$plh.';' : '';
    $style = $pclr != '' || $psz != '' || $plh != '' ? ' style="'.$pclr.$psz.$plh.'"' : '';
    $out = '';

    if ( $liststyle != '2' ){
        $out .= '<ul class="data__list">';
            foreach ( $loop as $item ) {
                $out .= '<li'.$style.'><b'.$lstyle.'>'.$item['label'].'</b>'.$item['desc'].'</li>';
            }
        $out .= '</ul>';
    } else {
        $out .= '<div class="data__info">';
            foreach ( $loop2 as $item ) {
                $out .= '<div class="data__info-item">'.$item['label'].'<b style="color:'.$item['color'].';">'.$item['desc'].'</b></div>';
            }
        $out .= '</div>';
    }
    return $out;
}
add_shortcode('cryptoland_tokenlist5_shortcode', 'cryptoland_tokenlist5');

/*-----------------------------------------------------------------------------------*/
/*	DATANUM HOME 2
/*-----------------------------------------------------------------------------------*/

function cryptoland_datanum( $atts, $content = null ) {
    extract( shortcode_atts(array(
        "num" => '',
        "symbl" => '',
        "symbl2" => '',
        "desc" => '',
        'tclr' => '',
        'tsz' => '',
        'tlh' => '',
        'sclr' => '',
        'ssz' => '',
        'slh' => '',
        'pclr' => '',
        'psz' => '',
        'plh' => '',
    ), $atts) );

    $out = '';
    $out .= '<div class="data__num">';
        $out .= '<div class="data__num-row">';
            $out .= cryptoland_element( $num, $tag='div', $class='data__num-value', $tclr, $tsz, $tlh, $anim='' );
            $out .= '<div class="data__num-name">';
                $out .= cryptoland_element( $symbl, $tag='div', $class='', $sclr, $ssz, $slh, $anim='' );
                $out .= cryptoland_element( $symbl2, $tag='div', $class='', $sclr, $ssz, $slh, $anim='' );
            $out .= '</div>';
        $out .= '</div>';
        $out .= '<div class="data__num-row">';
            $out .= cryptoland_element( $desc, $tag='p', $class='', $pclr, $psz, $plh, $anim='' );
        $out .= '</div>';
    $out .= '</div>';
    return $out;
}
add_shortcode('cryptoland_datanum_shortcode', 'cryptoland_datanum');

/*-----------------------------------------------------------------------------------*/
/*	DATANUM HOME 4
/*-----------------------------------------------------------------------------------*/

function cryptoland_datanum4( $atts, $content = null ) {
    extract( shortcode_atts(array(
        "num" => '',
        "numname" => '',
        "numimg" => '',
        "anim" => '',
        "delay" => '',
        'tclr' => '',
        'tsz' => '',
        'tlh' => '',
        'sclr' => '',
        'ssz' => '',
        'slh' => '',
    ), $atts) );
    $delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
    $anim = $anim != '' ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';
    $tclr = $tclr != '' ? ' color:'.esc_attr($tclr).';' : '';
    $tsz = $tsz != '' ? ' font-size:'.esc_attr($tsz).';' : '';
    $tlh = $tlh != '' ? ' line-height:'.esc_attr($tlh).';' : '';
    $style = $tclr != '' || $tsz != '' || $tlh != ''? ' style="'.$tclr.$tsz.$tlh.'"' : '';
    $out = '';
    $out .= '<div class="data__num"'.$anim.'>';
        $out .= '<div class="data__num-value"'.$style.'>'.$num.'';
            $out .= cryptoland_img( $numimg, $imgclass='data__num-img', $imgwidth='', $imgheight='', $data='' );
        $out .= '</div>';
        $out .= '<div class="data__num-name">';
            $out .= cryptoland_element( $numname, $tag='div', $class='', $sclr, $ssz, $slh, $anim='' );
        $out .= '</div>';
    $out .= '</div>';
    return $out;
}
add_shortcode('cryptoland_datanum4_shortcode', 'cryptoland_datanum4');

/*-----------------------------------------------------------------------------------*/
/*	INFODATA HOME 5-6
/*-----------------------------------------------------------------------------------*/
function cryptoland_infodata56( $atts, $content = null ) {
    extract( shortcode_atts(array(
    "infodata" => '',
    "title" => '',
    "number" => '',
    "color" => '',
    "addanim" => '',
    "anim" => '',
    "delay" => '',
    "tclr" => '',
    "tsz" => '',
    "tlh" => '',
    ), $atts) );

    $tclr = $tclr !='' ? ' color:'.$tclr.';' : '';
    $tsz = $tsz !='' ? ' font-size:'.$tsz.';' : '';
    $tlh = $tlh !='' ? ' font-size:'.$tlh.';' : '';
    $style = $tclr !='' || $tsz !='' || $tlh !='' ? ' style="'.$tclr.$tsz.$tlh.'"' : '';
    $loop = (array) vc_param_group_parse_atts($infodata);
    $out = '';
    $out .= '<div class="infoblock__list">';
        foreach ( $loop as $item ) {
            $color = isset( $item['color'] ) != '' ? $item['color'] : '#f3d667';
            $delay = isset( $item['delay'] ) != '' ? ' data-aos-delay="'.esc_attr($item['delay']).'"' : '';
            $addanim = isset( $item['addanim'] ) == 'yes' && isset( $item['anim'] ) != '' ? ' data-aos="'.esc_attr( $item['anim'] ).'"'.$delay : '';
            $out .= '<div class="infoblock__item" data-aos="fade-up" data-aos-delay="100">';
                if ( isset($item['title'] ) != '' ) { $out .= '<p'.$style.'>'.$item['title'].'</p>'; }
                if ( isset($item['number'] ) != '' ) { $out .= '<span style="color: '.$color.';">'.$item['number'].'</span>'; }
            $out .= '</div>';
        }
    $out .= '</div>';

    return $out;
}
add_shortcode('cryptoland_infodata56_shortcode', 'cryptoland_infodata56');

/*-----------------------------------------------------------------------------------*/
/*	PROGRESSBAR HOME 6
/*-----------------------------------------------------------------------------------*/
function cryptoland_progressbar6( $atts, $content = null ) {
    extract( shortcode_atts(array(
        "addanim" => '',
        "anim" => '',
        "delay" => '',
        "icobar" => '',
        "markertitle" => '',
        "markervalue" => '',
        "barclr" => '',
        "tclr" => '',
        "tsz" => '',
        "tlh" => '',
    ), $atts) );

    $loop = (array) vc_param_group_parse_atts($icobar);

    //animation
    $delay = $delay != '' ? ' data-aos-delay="'.esc_attr($delay).'"' : '';
    $anim = $addanim == true ? ' data-aos="'.esc_attr($anim).'"'.$delay : '';

    $out = '';
    if ($loop){
        $out .= '<div class="data-progress"'.$anim.'>';
            foreach ( $loop as $item ) {
                $markertitle = isset($item['markertitle']) != '' ? $item['markertitle'] : '';
                $markervalue = isset($item['markervalue']) != '' ? $item['markervalue'] : '';
                $barclr = isset($item['barclr']) != '' ? $item['barclr'] : '';
                if (isset($markervalue)  != ''){
                    $out .= '<div class="data-progress__item" style="box-shadow: 0 0 15px '.$barclr.'; background-color: '.$barclr.'; width: '.$markervalue.'%;">';
                        $out .= cryptoland_element( '<span>'.$markervalue.'%</span>'.$markertitle, $tag='p', $class='', $tclr, $tsz, $tlh, $anim='' );
                    $out .= '</div>';
                }
            }
        $out .= '</div>';
    }
    return $out;
}
add_shortcode('cryptoland_progressbar6_shortcode', 'cryptoland_progressbar6');

/*-----------------------------------------------------------------------------------*/
/*	FOOTER LOGO 3-4
/*-----------------------------------------------------------------------------------*/
function cryptoland_footerlogo( $atts, $content = null ) {
    extract( shortcode_atts(array(
        "logo" => '',
        "textlogo" => '',
        "imgwidth" => '',
        "imgheight" => '',
        'tclr' => '',
        'tsz' => '',
        'tlh' => '',
    ), $atts) );

    $out = '';
    $imgwidth = $imgwidth !='' ? ' width:'.$imgwidth.';' : '';
    $imgheight = $imgheight !='' ? ' height:'.$imgheight.';' : '';
    $style = $imgwidth !='' || $imgheight !='' ? ' style="'.$imgwidth.$imgheight.'"' : '';
    $out .= '<a href="'.esc_url( home_url( '/' ) ).'" class="footer-logo logo"'.$style.'>';
        $out .= cryptoland_img( $logo, $imgclass='logo__img--big', $imgwidth='', $imgheight='', $data='' );
        $out .= cryptoland_element( $textlogo, $tag='div', $class='logo__title', $tclr, $tsz, $tlh, $anim='' );
    $out .= '</a>';
    return $out;
}
add_shortcode('cryptoland_footerlogo_shortcode', 'cryptoland_footerlogo');

/*-----------------------------------------------------------------------------------*/
/*	HERO CANVAS
/*-----------------------------------------------------------------------------------*/
function cryptoland_herocanvas( $atts, $content = null ) {
    extract( shortcode_atts(array(
        "height" => '',
    ), $atts) );

    $height = $height != '' ? $height : 40;
    $out = '';
    $out .= '<div class="scene--wrap" id="start-screen" style="height:'.$height.';">';
        $out .= '<canvas class="scene scene--full" id="scene"></canvas>';
    $out .= '</div>';
    return $out;
}
add_shortcode('cryptoland_herocanvas_shortcode', 'cryptoland_herocanvas');
