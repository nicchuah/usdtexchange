<?php

/*
Plugin Name: Cryptoland Shortcodes
Plugin URI: http://themeforest.net/user/Ninetheme
Description: Shortcodes for Ninetheme WordPress Themes - Cryptoland Theme
Version: 1.2.7
Author: Ninetheme
Author URI: http://themeforest.net/user/Ninetheme
*/


require_once('inc/nt-theme-shortcodes.php');
require_once('inc/aq_resizer.php');


if ( ! function_exists( 'cryptolandshortcode' ) ) {
    function cryptolandshortcode() {
    }
}


add_action( 'plugins_loaded', 'cryptoland_shortcode_textdomain' );
function cryptoland_shortcode_textdomain() {
    load_plugin_textdomain( 'nt-theme-shortcodes', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
