<?php
if (!defined('ABSPATH')) {
  exit;
}
add_filter('post_updated_messages', 'user_forms_form_updated_message');
add_action('add_meta_boxes_user-forms', 'user_forms_add_meta_boxes');
add_action('add_meta_boxes_user_resetpassword', 'user_resetforms_add_meta_boxes');
add_action('add_meta_boxes_user_logins', 'user_logins_add_meta_boxes');
add_action('add_meta_boxes_user_registrations', 'user_registrations_add_meta_boxes');
add_action('wp_ajax_user-form_add_el', 'user_forms_ajax_post_add_field');
add_action( 'wp_ajax_nopriv_user-form_add_el', array( $this, 'user_forms_ajax_post_add_field' ) );
add_action('user_form_buttons_post', 'add_extra_button');
add_action('user_form_buttons_custom', 'add_custom_button');

add_action('save_post', 'user_forms_save_form', 1, 2);
function user_forms_form_updated_message($messages) {
  $message = array(
    0 => '',
    1 => __('Form updated.', 'frontend_user_pro') ,
    2 => __('Custom field updated.', 'frontend_user_pro') ,
    3 => __('Custom field deleted.', 'frontend_user_pro') ,
    4 => __('Form updated.', 'frontend_user_pro') ,
    5 => isset($_GET['revision']) ? sprintf(__('Form restored to revision from %s', 'frontend_user_pro') , wp_post_revision_title((int)$_GET['revision'], false)) : false,
    6 => __('Form published.', 'frontend_user_pro') ,
    7 => __('Form saved.', 'frontend_user_pro') ,
    8 => __('Form submitted.', 'frontend_user_pro') ,
    9 => '',
    10 => __('Form draft updated.', 'frontend_user_pro') ,
  );
  $messages['user-forms'] = $message;
  $messages['user_logins'] = $message;
  $messages['user_registrations'] = $message;
  $messages['user_resetpassword'] = $message;
  return $messages;
}
function user_forms_add_meta_boxes() {
  global $post;
  add_meta_box('user-metabox-editor', __('Mife Form Editor', 'frontend_user_pro') , 'user_forms_metabox', 'user-forms', 'normal', 'high');
  add_meta_box('user-metabox-save', __('Publish', 'frontend_user_pro') , 'user_forms_form_elements_save', 'user-forms', 'side', 'core');
  add_meta_box('user-metabox-fields', __('Add a Field', 'frontend_user_pro') , 'user_forms_elements_callback', 'user-forms', 'side', 'core');
  add_meta_box( 
    'my-meta-box',
    __('Shortcode', 'frontend_user_pro'),
    'user_forms_form_elements_sc',
    'user-forms',
    'side',
    'core'
    );
  do_action('user_add_custom_meta_boxes', get_the_ID());
  remove_meta_box('submitdiv', 'user-forms', 'side');
  remove_meta_box('slugdiv', 'user-forms', 'normal');
}
function user_resetforms_add_meta_boxes() {
 global $post;
 add_meta_box('user-metabox-editor', __('Reset Password Form Editor', 'frontend_user_pro') , 'user_forms_metabox', 'user_resetpassword', 'normal', 'high');
 add_meta_box('user-metabox-save', __('Publish', 'frontend_user_pro') , 'user_forms_form_elements_save', 'user_resetpassword', 'side', 'core');
 add_meta_box('user-metabox-fields', __('Add a Field', 'frontend_user_pro') , 'user_forms_form_elements_registration', 'user_resetpassword', 'side', 'core');
 add_meta_box( 
  'my-meta-box',
  __('Shortcode', 'frontend_user_pro'),
  'user_forms_form_elements_sc',
  'user_resetpassword',
  'side',
  'core'
  );
 do_action('user_add_custom_meta_boxes', get_the_ID());
 remove_meta_box('submitdiv', 'user_resetpassword', 'side');
 remove_meta_box('slugdiv', 'user_resetpassword', 'normal');
}
function user_logins_add_meta_boxes() {
  global $post;
  add_meta_box('user-metabox-editor', __('Login Form Editor', 'frontend_user_pro') , 'user_forms_metabox', 'user_logins', 'normal', 'high');
  add_meta_box('user-metabox-save', __('Publish', 'frontend_user_pro') , 'user_forms_form_elements_save', 'user_logins', 'side', 'core');
  add_meta_box('user-metabox-fields', __('Add a Field', 'frontend_user_pro') , 'user_forms_elements_callback', 'user_logins', 'side', 'core');
  add_meta_box( 
        'my-meta-box',
        __('Add Form to a Page', 'frontend_user_pro'),
        'user_forms_form_elements_sc',
        'user_logins',
        'side',
        'core'
    );
  do_action('user_add_custom_meta_boxes', get_the_ID());
  remove_meta_box('submitdiv', 'user_logins', 'side');
  remove_meta_box('slugdiv', 'user_logins', 'normal');
}
function user_registrations_add_meta_boxes() {
  global $post;
  add_meta_box('user-metabox-editor', __('Registration Form Editor', 'frontend_user_pro') , 'user_forms_metabox', 'user_registrations', 'normal', 'high');
  add_meta_box('user-metabox-save', __('Publish', 'frontend_user_pro') , 'user_forms_form_elements_save', 'user_registrations', 'side', 'core');
  add_meta_box('user-metabox-fields', __('Add a Field', 'frontend_user_pro') , 'user_forms_form_elements_registration', 'user_registrations', 'side', 'core');
  add_meta_box( 
        'my-meta-box',
        __('Add Form to a Page', 'frontend_user_pro'),
        'user_forms_form_elements_sc',
        'user_registrations',
        'side',
        'core'
    );
  do_action('user_add_custom_meta_boxes', get_the_ID());
  remove_meta_box('submitdiv', 'user_registrations', 'side');
  remove_meta_box('slugdiv', 'user_registrations', 'normal');
}
function user_forms_publish_button() {
  global $post, $pagenow;
  ?> 
    <div id="minor-publishing-actions">
      <center>
      <?php if( !array_key_exists('post', $_GET)) { ?>
      <input type="hidden" value="Publish" id="original_publish" name="original_publish">      
      <input type="submit" value="Publish" class="button button-primary button-large" id="publish" name="publish">
      <?php } else { ?>
      <input name="original_publish" type="hidden" id="original_publish" value="<?php esc_attr_e('Update') ?>" />
      <input name="save" type="submit" class="button button-primary button-large" id="publish" accesskey="p" value="<?php esc_attr_e('Update') ?>" />
      <?php } ?> 
      <span class="spinner"></span>
      </center>
    </div>
    <div class="user-clear"></div>
  <?php
}
function user_forms_metabox($post) {  
  if( $post->post_type == 'user_registrations' ) {
    $title = __('USER Registration Form Editor', 'frontend_user_pro'); 
    $title = apply_filters('user_forms_metabox_title', $title, get_the_ID());  
  } elseif( $post->post_type == 'user-forms' ) {
    $title = __('USER Mi Fe Form Editor', 'frontend_user_pro');
    $title = apply_filters('user_forms_metabox_title', $title, get_the_ID()); 
  } elseif( $post->post_type == 'user_logins' ) {
    $title = __('USER Login Form Editor', 'frontend_user_pro');
    $title = apply_filters('user_forms_metabox_title', $title, get_the_ID());
  }elseif( $post->post_type == 'user_resetpassword' ) {
    $title = __('USER Reset Password Form Editor', 'frontend_user_pro');
    $title = apply_filters('user_forms_metabox_title', $title, get_the_ID());
    ?>
    <style type="text/css">
  .wrap .page-title-action {
    display: none;
  }
    </style>
    <?php
  }       
  ?><style type="text/css">#post-body-content {display: block;}</style><?php
  ?>
  <h1><?php //echo $title; ?></h1>
  <h2 class="nav-tab-wrapper">
            <a href="#user-metabox" class="nav-tab" id="user_general-tab"><?php _e( 'Form Editor', 'frontend_user_pro' ); ?></a>
            <a href="#user-metabox-settings" class="nav-tab" id="user_dashboard-tab"><?php _e( 'Settings', 'frontend_user_pro' ); ?></a>
            <?php
             if( $post->post_type == 'user_registrations' ) { ?>
            <a href="#user-metabox-notification" class="nav-tab" id="user_notification-tab"><?php _e( 'Notification', 'frontend_user_pro' ); ?></a>
            <?php } ?>
            <a href="#user-metabox-form_layout" class="nav-tab" id="user-form_layout-tab"><?php _e( 'Layout & Styles', 'frontend_user_pro' ); ?></a>
            <?php do_action( 'user_profile_form_tab' ); ?>
        </h2> 
  <div class="tab-content">
      <div id="user-metabox" class="group">
           <?php user_forms_edit_form_area(); ?>
      </div> 
      <div id="user-metabox-settings" class="group">
        <?php user_form_settings_area(); ?>
      </div>
      <?php
      if( $post->post_type == 'user_registrations' ) 
        { ?>
      <div id="user-metabox-notification" class="group">
        <?php user_form_notification_area(); ?>
      </div>
      <?php } ?>
      <div id="user-metabox-form_layout" class="group">
        <?php form_settings_form_layout(); ?>
      </div>
  </div>
  <?php
}
function form_settings_form_layout(){
  global $post;
  $id = $post->ID;
  $feup_f_col = get_post_meta( $id, 'user_form_settings', true );
  $clss = '';
  $style_name ='';
  $style_name_r ='';
  if (is_array($feup_f_col) && array_key_exists('feup_f_col', $feup_f_col ) ) {
    $clss = $feup_f_col['feup_f_col'];
  }
  if (is_array($feup_f_col) && array_key_exists('style_name', $feup_f_col ) ) {
    $style_name = $feup_f_col['style_name'];
  }
  if (is_array($feup_f_col) && array_key_exists('style_name_r', $feup_f_col ) ) {
    $style_name_r = $feup_f_col['style_name_r'];
  }
  $all_st_name = array('Akira', 'Haruki', 'Hoshi', 'Ichiro', 'Isao', 'Jiro', 'Juro', 'Kuro', 'Kyo', 'Madoka', 'Minor', 'Ruri', 'Yoko');
  $rp = '';
  $p_type = get_post_type($id);
  ?>
  <div style="margin-bottom: 30px; margin-top: 30px;">
    <div style="display: inline-block; width: 50%; vertical-align: top;">
      <input type="text" value="<?php echo $style_name; ?>" name="user_settings[style_name]" placeholder="Enter style name" <?php echo $rp; ?>>
      <input type="hidden" value="<?php echo $style_name_r; ?>" name="user_settings[style_name_r]" placeholder="Enter style name" <?php echo $rp; ?>>
      <button class="button button-primary style_name" type="submit">Save Name</button>    
    </div>
    <div style="text-align: right; display: inline-block; width: 49%; vertical-align: top;">
      Select Predefine Styles 
      <select>
        <option value="">Default</option>
        <option value="Akira" <?php if($style_name == 'Akira') { echo 'selected'; } ?> >Akira</option>
        <option value="Haruki" <?php if($style_name == 'Haruki') { echo 'selected'; } ?>>Haruki</option>
        <option value="Hoshi" <?php if($style_name == 'Hoshi') { echo 'selected'; } ?>>Hoshi</option>
        <option value="Ichiro" <?php if($style_name == 'Ichiro') { echo 'selected'; } ?>>Ichiro</option>
        <option value="Isao" <?php if($style_name == 'Isao') { echo 'selected'; } ?>>Isao</option>
        <option value="Jiro" <?php if($style_name == 'Jiro') { echo 'selected'; } ?>>Jiro</option>
        <option value="Juro" <?php if($style_name == 'Juro') { echo 'selected'; } ?>>Juro</option>
        <option value="Kuro" <?php if($style_name == 'Kuro') { echo 'selected'; } ?>>Kuro</option>
        <option value="Kyo" <?php if($style_name == 'Kyo') { echo 'selected'; } ?>>Kyo</option>
        <option value="Madoka" <?php if($style_name == 'Madoka') { echo 'selected'; } ?>>Madoka</option>
        <option value="Minor" <?php if($style_name == 'Minor') { echo 'selected'; } ?>>Minor</option>
        <option value="Ruri" <?php if($style_name == 'Ruri') { echo 'selected'; } ?>>Ruri</option>
        <option value="Yoko" <?php if($style_name == 'Yoko') { echo 'selected'; } ?>>Yoko</option>
      </select>
      <button class="button button-primary load_style" type="submit">Load Layout</button>
    </div>    
  </div>
  <script type="text/javascript">
  (function($){
    $(document).ready(function(){
      $('select[name="user_settings[feup_f_col]"]').on('change',function(){
        var val = $(this).val();
      });
      $('button.load_style').on('click',function(e){
        e.preventDefault();
        var sn = $(this).prev('select').val();
        $.ajax({
         method: "POST",
         url: "<?php echo admin_url('admin-ajax.php'); ?>",
         data: { action: 'load_style', id: '<?php echo $id; ?>', name : sn }
       }).done(function(res) {
        $('input[name="user_settings[style_name]"]').val(sn);
        $('input[name="user_settings[style_name_r]"]').val(sn);
        $('.tab-content.frontend_style_css_wrap.general-settings.full-width').empty();
        $('button.style_name').trigger('click');
      });
     });
      $.ajax({
       method: "POST",
       url: "<?php echo admin_url('admin-ajax.php'); ?>",
       data: {action: 'get_current_form', id: '<?php echo $id; ?>'}
     }).done(function(res) {
      $('.tab_right_form_wrap #submitpost').append(res);
    });
   });
  })(jQuery);
  </script>
  <div class="custom_style">
    <?php
    if (is_array($_GET) && array_key_exists('post', $_GET) ) {
      $post_id = $_GET['post'];
    }else if($post->ID){
      $post_id = $post->ID;
    }
    wp_nonce_field( 'feup_a_style_style_customizer_inner_custom_box', 'feup_a_style_customizer_custom_box_nonce' );
    $bbb =  get_post_meta( $post->ID, 'feua_style_custom_styles', true );
    $setting_array = unserialize($bbb);
    $result_manual = get_post_meta( $post->ID, 'feua_style_manual_style', true ); 
    ?>
    <h2 class="nav-tab-wrapper1 custom_nav_tav1">
      <a class="nav-tab nav-tab-active" href="#container_styles">container styles</a>
      <a class="nav-tab" href="#title_styles">Title styles</a>
      <a class="nav-tab" href="#row_styles">Row styles</a>
      <a class="nav-tab" href="#custom_css">Custom CSS</a>
    </h2>
  <div class="tab-content frontend_style_css_wrap general-settings full-width">
    <?php 
    foreach( feua_style_general_settings_array() as $key=>$settings) {  
      $hd = '';
      if($key != 'container_styles' ) {
        $hd = 'style="display:none"';   
      }?>
      <li class="custom-field"  id="<?php echo $key; ?>" <?php echo $hd; ?>>
        <div title="Click and Drag to rearrange" class="user-legend">
          <div class="user-actions">
            Show Advanced CSS <input type="checkbox" class="show_advance_css" value="1">
          </div>
        </div> 
        <div class="user-form-holder">
          <table>
            <?php
            foreach( $settings as $setting ){
              $current_val = ( !empty( $setting_array ) ) ? $setting_array[ strtolower( str_replace( " ", "-", $setting["label"] ) ) ] : "";
              $current_option = ( $setting["type"] == "selectbox" ) ? $setting["value"] : "";
              $current_title = ( isset( $setting["title"] ) ) ? $setting["title"] : "";
              $current_class = ( isset( $setting["class"] ) ) ? $setting["class"] : "";
              feua_style_render_settings( $setting["type"], $setting["label"], $current_option, $current_val, $setting["description"], $current_title, $current_class );
            } ?>
          </table>
        </div>
      </li>
      <?php } ?>
      <li class="custom-field"  id="custom_css" style="display:none;">
        <div class="general-settings full-width">
          <h3><?php _e( "CSS Editor", "feuastyle" ); ?></h3>
          <p>You can easily find CSS elements by using your browser inspector or Firebug, or view a quick guide <a href="http://sixrevisions.com/tools/firebug-guide-web-designers/" target="_blank" title="Firebug guide">here</a>.</p>
          <label for="manual-style">
            <textarea name="manual-style" id="manual-style" cols="83" rows="15"><?php echo $result_manual; ?></textarea>
          </label>
        </div>
      </li>
    </div><!-- /.general-settings -->
  </div>
  <script type="text/javascript">
  (function($){
    $(document).ready(function(){
      $('.show_advance_css').on('click',function(e){
        if($(this).is(':checked')) {
          $(this).closest('li').find('tr.advance_input').show();
        }
        else {
          $(this).closest('li').find('tr.advance_input').hide();
        }
      });
      $('.general-settings li .user-legend .user-actions a.user-toggle').on('click',function(e){
        e.preventDefault();
        $(this).closest('.user-legend').next().toggle();
      });
      $('.custom_nav_tav1 a').on('click',function(e){
        e.preventDefault();
        var cls = $(this).attr('href');
        $(this).siblings().removeClass('nav-tab-active');
        $(this).addClass('nav-tab-active');
        $('.tab-content.general-settings li').css('display','none');
        $(cls).fadeIn();
      });
    });
  })(jQuery);
  </script>
  <div class="clear"></div>
  <?php
}
function user_forms_form_elements_sc() 
{
     global $post;
     $id = $post->ID;
     $render_to_page = '';
     if(isset($_GET['post'])) {
      $render_form_to_page = get_post_meta( $id, 'user_form_settings', true );
      if(array_key_exists('render_form_to_page', $render_form_to_page)) {
       $render_to_page = $render_form_to_page['render_form_to_page'];
     }
   }
   ?>
    <div class="submitbox" id="submitpost">
      <div id="minor-publishing">
        <div class="user-loading hide"></div>
            Add form to this page
            <select name="user_settings[render_form_to_page]">
                <?php
                $pages = get_posts(  array( 'numberposts' => -1, 'post_type' => 'page') );
                echo '<option value="">None</option>';
                foreach ($pages as $page) {
                    printf('<option value="%s"%s>%s</option>', $page->ID, selected( $render_to_page, $page->ID, false ), esc_attr( $page->post_title ) );
                }
                ?>
            </select>
            <p><strong>OR</strong></p>
        <?php 
          if (get_post_type() == 'user_logins' ) 
          { ?>
            <p>Use this shortcode</p>
            <input type="text" name="sc" value="<?php echo "[wpfeup-login id='".get_the_ID()."']" ;  ?>" style="width: 100%;" readonly>
            <p>to display this form inside a post, page or text widget.</p>
            <?php
          } else if (get_post_type() == 'user_registrations' ) 
          { ?>
            <p>Use this shortcode</p>
            <p>Registration:</p>
            <input type="text" name="sc" value="<?php echo "[wpfeup-register type='registration' id='".get_the_ID()."']" ;  ?>" style="width: 100%;" readonly>
            <p>Edit Profile:</p>
            <input type="text" name="sc" value="<?php echo "[wpfeup-register type='profile' id='".get_the_ID()."']" ;  ?>" style="width: 100%;" readonly>
            <p>to display this form inside a post, page or text widget.</p>
          <?php
          }   if (get_post_type() == 'user_resetpassword' ) 
          {   ?>
            <p>Use this shortcode</p>
            <p>Reset password:</p>
            <input type="text" name="sc" value="<?php echo "[wpfeup-resetpassword]" ;  ?>" style="width: 100%;" readonly>
          <?php 
          } ?>
    </div>
  </div>
   <?php
}
function user_forms_form_elements_save() {
  ?>
  <div class="submitbox" id="submitpost">
    <div id="minor-publishing">
      <?php user_forms_publish_button(); ?>
    </div>
  </div>
  <?php
}
function add_extra_button() {
  $title = esc_attr(__('Click to add to the editor', 'frontend_user_pro'));
  ?>
  <h2><?php _e( 'Form Fields', 'frontend_user_pro' ); ?></h2>
  <div class="user-form-buttons">
    <button class="user-button button" data-name="first_name" data-type="textarea" title="<?php echo $title; ?>"><?php _e('First Name', 'frontend_user_pro'); ?></button>
    <button class="user-button button" data-name="last_name" data-type="textarea" title="<?php echo $title; ?>"><?php _e('Last Name', 'frontend_user_pro'); ?></button>
    <button class="user-button button" data-name="nickname" data-type="text" title="<?php echo $title; ?>"><?php _e('Nickname', 'frontend_user_pro'); ?></button>
    <button class="user-button button" data-name="user_bio" data-type="textarea" title="<?php echo $title; ?>"><?php _e('Biography', 'frontend_user_pro'); ?></button>
    <button class="user-button button" data-name="user_url" data-type="text" title="<?php echo $title; ?>"><?php _e('Website', 'frontend_user_pro'); ?></button>
    <button class="user-button button" data-name="password1" data-type="password1" title="<?php echo $title; ?>"><?php _e('Password', 'frontend_user_pro'); ?></button>
    
    <button class="user-button button" data-name="custom_country" data-type="country" title="<?php echo $title; ?>"><?php _e('Country', 'frontend_user_pro'); ?></button>
  </div>
  <?php
}
function user_forms_elements_callback() {
  $title = esc_attr(__('Click to add to the editor', 'frontend_user_pro'));
  ?>
    <style type="text/css">
    .custom-field .enable_conditional_input_check, 
    .user_bio .enable_conditional_input_check {
    display: none;
      }
  </style>
  <div class="user-loading hide"></div>
  <div class="user-form-buttons">
  <button class="user-button button" data-name="first_name" data-type="textarea" title="<?php echo $title; ?>"><?php _e('First Name', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="last_name" data-type="textarea" title="<?php echo $title; ?>"><?php _e('Last Name', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="user_login" data-type="text" title="<?php echo $title; ?>"><?php _e('Username', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="password" data-type="password" title="<?php echo $title; ?>"><?php _e('Password', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="user_email" data-type="email" title="<?php echo $title; ?>"><?php _e('Email', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="nickname" data-type="text" title="<?php echo $title; ?>"><?php _e('Nickname', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="display_name" data-type="text" title="<?php echo $title; ?>"><?php _e('Display Name', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="user_bio" data-type="textarea" title="<?php echo $title; ?>"><?php _e('Biography', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="user_url" data-type="text" title="<?php echo $title; ?>"><?php _e('Website', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_url" data-type="url" title="<?php echo $title; ?>"><?php _e('URL', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_text" data-type="text" title="<?php echo $title; ?>"><?php _e('Text', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="zipcode_text" data-type="zipcode" title="<?php echo $title; ?>"><?php _e('Zipcode', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_textarea" data-type="textarea" title="<?php echo $title; ?>"><?php _e('Textarea', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_select" data-type="select" title="<?php echo $title; ?>"><?php _e('Dropdown', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_multiselect" data-type="multiselect" title="<?php echo $title; ?>"><?php _e('Multi Select', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_date" data-type="date" title="<?php echo $title; ?>"><?php _e('Date', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_radio" data-type="radio" title="<?php echo $title; ?>"><?php _e('Radio', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_checkbox" data-type="checkbox" title="<?php echo $title; ?>"><?php _e('Checkbox', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_repeater" data-type="repeat" title="<?php echo $title; ?>"><?php _e('Repeat Field', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_file" data-type="file" title="<?php echo $title; ?>"><?php _e('File Upload', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_image" data-type="image" title="<?php echo $title; ?>"><?php _e( 'Image Upload', 'frontend_user_pro' ); ?></button>
  <button class="user-button button" data-name="custom_number" data-type="number" title="<?php echo $title; ?>"><?php _e( 'Number', 'frontend_user_pro' ); ?></button>
  <button class="user-button button" data-name="custom_email" data-type="email" title="<?php echo $title; ?>"><?php _e('Email', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_hidden" data-type="hidden" title="<?php echo $title; ?>"><?php _e('Hidden Field', 'frontend_user_pro'); ?></button>
  <!-- <button class="user-button button" data-name="recaptcha" data-type="captcha" title="<?php //echo $title; ?>"><?php// _e('Captcha', 'frontend_user_pro'); ?></button> -->
  <button class="user-button button" data-name="text" data-type="html" title="<?php echo $title; ?>"><?php _e('Display Text', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="social_icon" data-type="social_icon" title="<?php echo $title; ?>"><?php _e('Social Icon', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="section_break" data-type="break" title="<?php echo $title; ?>"><?php _e('Separator', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_multistep" data-type="custom_multistep" title="<?php echo $title; ?>"><?php _e('Multistep', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_html" data-type="html" title="<?php echo $title; ?>"><?php _e('HTML', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_css" data-type="custom_css1" title="<?php echo $title; ?>"><?php _e('Custom Css', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_js" data-type="js" title="<?php echo $title; ?>"><?php _e('Custom Js', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="action_hook" data-type="action" title="<?php echo $title; ?>"><?php _e('Do Action', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_country" data-type="country" title="<?php echo $title; ?>"><?php _e('Country', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="featured_image" data-type="featured_image" title="<?php echo $title; ?>"><?php _e('Image Upload', 'frontend_user_pro'); ?></button>
  <button class="user-button button" title="<?php echo $title; ?>" data-type="map" data-name="custom_map"><?php _e('Google Maps', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="text" data-type="html" title="<?php echo $title; ?>"><?php _e('Heading', 'frontend_user_pro'); ?></button>
  <?php
  if (FRONTEND_USER()->integrations->is_commissions_active()) { ?>
    <button class="user-button button" data-name="level" data-type="level"><?php _e('PayPal Email', 'frontend_user_pro'); ?></button>
    <button class="user-button button" data-name="toc" data-type="action" title="<?php echo $title; ?>" style="width: 233px;" ><?php _e('Accept Terms', 'frontend_user_pro'); ?></button>
  <?php
  } else { ?>
      <button class="user-button button" data-name="toc" data-type="action" title="<?php echo $title; ?>" style="width: 233px;" ><?php _e('Accept Terms', 'frontend_user_pro'); ?></button>
      <?php
  }
  do_action('user_custom_registration_button'); ?>
  <button class="user-button button" data-name="really_simple_captcha" data-type="rscaptcha" title="<?php echo $title; ?>" style="width: 233px;" ><?php _e( 'Really Simple Captcha', 'frontend_user_pro' ); ?></button>
  </div>
  <?php
}
function user_forms_form_elements_post() {
  $title = esc_attr(__('Click to add to the editor', 'frontend_user_pro'));
  ?>
  <div class="user-form-buttons">
  <button class="user-button button" data-name="post_title" data-type="post_title" title="<?php echo $title; ?>"><?php _e('Title', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="post_content" data-type="post_content" title="<?php echo $title; ?>"><?php _e('Description', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="featured_image" data-type="featured_image" title="<?php echo $title; ?>"><?php _e('Featured Image', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="post_category" data-type="post_category" title="<?php echo $title; ?>"><?php _e('Categories', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="post_tag" data-type="post_tag" title="<?php echo $title; ?>"><?php _e('Tags', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="post_types" data-type="post_types" title="<?php echo $title; ?>"><?php _e('Post Type', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="multiple_pricing" data-type="multiple_pricing" title="<?php echo $title; ?>"><?php _e('Prices and Files', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="post_excerpt" data-type="post_excerpt" title="<?php echo $title; ?>"><?php _e('Excerpt', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_text" data-type="text" title="<?php echo $title; ?>"><?php _e('Text', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_textarea" data-type="textarea" title="<?php echo $title; ?>"><?php _e('Textarea', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_select" data-type="select" title="<?php echo $title; ?>"><?php _e('Dropdown', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_date" data-type="date" title="<?php echo $title; ?>"><?php _e('Date', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_multiselect" data-type="multiselect" title="<?php echo $title; ?>"><?php _e('Multi Select', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_radio" data-type="radio" title="<?php echo $title; ?>"><?php _e('Radio', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_checkbox" data-type="checkbox" title="<?php echo $title; ?>"><?php _e('Checkbox', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_image" data-type="image" title="<?php echo $title; ?>"><?php _e( 'Image Upload', 'frontend_user_pro' ); ?></button>
  <button class="user-button button" data-name="custom_number" data-type="number" title="<?php echo $title; ?>"><?php _e( 'Number', 'frontend_user_pro' ); ?></button>
  <button class="user-button button" data-name="custom_file" data-type="file" title="<?php echo $title; ?>"><?php _e('File Upload', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_url" data-type="url" title="<?php echo $title; ?>"><?php _e('URL', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_email" data-type="email" title="<?php echo $title; ?>"><?php _e('Email', 'frontend_user_pro'); ?></button>
  
  <button class="user-button button" data-name="custom_repeater" data-type="repeat" title="<?php echo $title; ?>"><?php _e('Repeat Field', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_hidden" data-type="hidden" title="<?php echo $title; ?>"><?php _e('Hidden Field', 'frontend_user_pro'); ?></button>
  <!-- <button class="user-button button" data-name="recaptcha" data-type="captcha" title="<?php// echo $title; ?>"><?php //_e('Captcha', 'frontend_user_pro'); ?></button> -->
  <button class="user-button button" data-name="section_break" data-type="break" title="<?php echo $title; ?>"><?php _e('Separator', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_multistep" data-type="custom_multistep" title="<?php echo $title; ?>"><?php _e('Multistep', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_html" data-type="html" title="<?php echo $title; ?>"><?php _e('HTML', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_css" data-type="custom_css1" title="<?php echo $title; ?>"><?php _e('Custom Css', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_js" data-type="js" title="<?php echo $title; ?>"><?php _e('Custom Js', 'frontend_user_pro'); ?></button>
  <button class="user-button button" title="<?php echo $title; ?>" data-type="map" data-name="custom_map"><?php _e('Google Maps', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="action_hook" data-type="action" title="<?php echo $title; ?>"><?php _e('Do Action', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="toc" data-type="action" title="<?php echo $title; ?>" style="width: 233px;" ><?php _e('Accept Terms', 'frontend_user_pro'); ?></button>
  <?php
  do_action('user_custom_post_button', $title); ?>
  </div>
  <?php
}
function add_custom_button() {
  $title = esc_attr(__('Click to add to the editor', 'frontend_user_pro'));
   ?>
    <button class="user-button button" data-name="text" data-type="html" title="<?php echo $title; ?>"><?php _e('Heading', 'frontend_user_pro'); ?></button>
   <?php
}
function user_forms_form_elements_registration() {
  $title = esc_attr(__('Click to add to the editor', 'frontend_user_pro'));
  ?>
  <style type="text/css">
    .enable_conditional_input_check {
    display: none;
    }
  </style>
  <div class="user-loading hide"></div>
  <div class="user-form-buttons">
  <button class="user-button button" data-name="first_name" data-type="textarea" title="<?php echo $title; ?>"><?php _e('First Name', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="last_name" data-type="textarea" title="<?php echo $title; ?>"><?php _e('Last Name', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="user_login" data-type="text" title="<?php echo $title; ?>"><?php _e('Username', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="user_email" data-type="email" title="<?php echo $title; ?>"><?php _e('Email', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="zipcode_text" data-type="zipcode" title="<?php echo $title; ?>"><?php _e('Zipcode', 'frontend_user_pro'); ?></button>  
  <button class="user-button button" data-name="user_avatar" data-type="avatar" title="<?php echo $title; ?>"><?php _e('Profile Picture', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="nickname" data-type="text" title="<?php echo $title; ?>"><?php _e('Nickname', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="display_name" data-type="text" title="<?php echo $title; ?>"><?php _e('Display Name', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="password" data-type="password" title="<?php echo $title; ?>"><?php _e('Password', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="user_bio" data-type="textarea" title="<?php echo $title; ?>"><?php _e('Biography', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="user_url" data-type="text" title="<?php echo $title; ?>"><?php _e('Website', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_image" data-type="image" title="<?php echo $title; ?>"><?php _e( 'Image Upload', 'frontend_user_pro' ); ?></button>
  <button class="user-button button" data-name="custom_number" data-type="number" title="<?php echo $title; ?>"><?php _e( 'Number', 'frontend_user_pro' ); ?></button>
  <button class="user-button button" data-name="custom_file" data-type="file" title="<?php echo $title; ?>"><?php _e('File Upload', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_url" data-type="url" title="<?php echo $title; ?>"><?php _e('URL', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_text" data-type="text" title="<?php echo $title; ?>"><?php _e('Text', 'frontend_user_pro'); ?></button>
  
  <button class="user-button button" data-name="custom_textarea" data-type="textarea" title="<?php echo $title; ?>"><?php _e('Textarea', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_select" data-type="select" title="<?php echo $title; ?>"><?php _e('Dropdown', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_multiselect" data-type="multiselect" title="<?php echo $title; ?>"><?php _e('Multi Select', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_date" data-type="date" title="<?php echo $title; ?>"><?php _e('Date', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_radio" data-type="radio" title="<?php echo $title; ?>"><?php _e('Radio', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_checkbox" data-type="checkbox" title="<?php echo $title; ?>"><?php _e('Checkbox', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_repeater" data-type="repeat" title="<?php echo $title; ?>"><?php _e('Repeat Field', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_email" data-type="email" title="<?php echo $title; ?>"><?php _e('Email', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_hidden" data-type="hidden" title="<?php echo $title; ?>"><?php _e('Hidden Field', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="social_icon" data-type="social_icon" title="<?php echo $title; ?>"><?php _e('Social Icon', 'frontend_user_pro'); ?></button>
  <!-- <button class="user-button button" data-name="recaptcha" data-type="captcha" title="<?php// echo $title; ?>"><?php// _e('Captcha', 'frontend_user_pro'); ?></button> -->
  <button class="user-button button" data-name="text" data-type="html" title="<?php echo $title; ?>"><?php _e('Heading', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="section_break" data-type="break" title="<?php echo $title; ?>"><?php _e('Separator', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_multistep" data-type="custom_multistep" title="<?php echo $title; ?>"><?php _e('Multistep', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_html" data-type="html" title="<?php echo $title; ?>"><?php _e('HTML', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="action_hook" data-type="action" title="<?php echo $title; ?>"><?php _e('Do Action', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_country" data-type="country" title="<?php echo $title; ?>"><?php _e('Country', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_css" data-type="custom_css1" title="<?php echo $title; ?>"><?php _e('Custom Css', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_js" data-type="js" title="<?php echo $title; ?>"><?php _e('Custom Js', 'frontend_user_pro'); ?></button>
  <button class="user-button button" title="<?php echo $title; ?>" data-type="map" data-name="custom_map"><?php _e('Google Maps', 'frontend_user_pro'); ?></button>
  <button class="user-button button" data-name="custom_mail_user" data-type="custom_mail" title="<?php echo $title; ?>"><?php _e('Register Email', 'frontend_user_pro'); ?></button>
  <?php
  $paypal = get_option("paypal-on" , true);
  if (isset($paypal) ) {
    ?>
        <button class="user-button button" data-name="level" data-type="level"><?php _e('Level', 'frontend_user_pro'); ?></button>
    <?php
  }
  if (FRONTEND_USER()->integrations->is_commissions_active()) 
  { ?>
    <button class="user-button button" data-name="toc" data-type="action" title="<?php echo $title; ?>"><?php _e('Accept Terms', 'frontend_user_pro'); ?></button>
    <?php
  }
  ?>
    <button class="user-button button" data-name="toc" data-type="action" title="<?php echo $title; ?>" style="width: 233px;" ><?php _e('Accept Terms', 'frontend_user_pro'); ?></button>
    <?php do_action('user_custom_registration_button'); ?>
    <button class="user-button button" data-name="really_simple_captcha" data-type="rscaptcha" title="<?php echo $title; ?>" style="width: 233px;" ><?php _e( 'Really Simple Captcha', 'frontend_user_pro' ); ?></button>
  </div>
  <?php
}
function user_forms_save_form($post_id, $post) {
  if (!isset($_POST['user-form-editor'])) {
    return $post->ID;
  }
  
  if (!wp_verify_nonce($_POST['user-form-editor'], 'user-form-editor')) {
    return $post->ID;
  }
  // Is the user allowed to edit the post or page?
  if (!current_user_can('edit_post', $post->ID)) {
    return $post->ID;
  }
 /* echo "<pre>";
  print_r($_POST['user_settings']);
  echo "</pre>";*/
  update_post_meta($post->ID, 'user-form', $_POST['user_input']);
  update_post_meta($post->ID, 'user_form_settings', $_POST['user_settings']);
}
function user_forms_edit_form_area() {
  global $post, $pagenow;
  if ($post->post_type == 'user_logins' ) {
    $form = __('Your Login form has no fields', 'frontend_user_pro');
  } else if ($post->post_type == 'user_registrations' ) {
    $form = __('Your registration form has no fields', 'frontend_user_pro');
  } else {
    $form = __('Your submission form has no fields', 'frontend_user_pro');
  }
  $form = apply_filters('user_edit_form_area_no_fields', $form, get_the_ID());
  $form_inputs = get_post_meta($post->ID, 'user-form', true);
  $feup_f_col = get_post_meta( $post->ID, 'user_form_settings', true );
        $clss = '';
        if ( is_array($feup_f_col) && array_key_exists('feup_f_col', $feup_f_col ) ) {
            $clss = $feup_f_col['feup_f_col'];
        }
  ?>
  <input type="hidden" name="user-form-editor" id="user-form-editor" value="<?php echo wp_create_nonce("user-form-editor"); ?>" />
  <div style="margin-bottom: 10px; text-align: right; margin-top: 10px;ight;">
    Break the form in
    <select name="user_settings[feup_f_col]">
      <option value="feup_f_col-1" <?php if($clss == 'feup_f_col-1') { echo 'selected'; } ?>>1 Column</option>
      <option value="feup_f_col-2" <?php if($clss == 'feup_f_col-2') { echo 'selected'; } ?>>2 Column</option>
      <option value="feup_f_col-3" <?php if($clss == 'feup_f_col-3') { echo 'selected'; } ?>>3 Column</option>
      <option value="feup_f_col-4" <?php if($clss == 'feup_f_col-4') { echo 'selected'; } ?>>4 Column</option>
      <option value="feup_f_col-5" <?php if($clss == 'feup_f_col-5') { echo 'selected'; } ?>>5 Column</option>
    </select>
    <script type="text/javascript">
    (function($){
        $(document).ready(function(){
            $('select[name="user_settings[feup_f_col]"]').on('change',function(){
                var val = $(this).val();
                jQuery("ul.user-form-editor").removeClass (function (index, css) {
                    return (css.match (/(^|\s)feup_f_col-\S+/g) || []).join(' ');
                });
                
                $('ul.user-form-editor').addClass(val);
            });
            $('select.label_position').on('change',function(){
              var val = $(this).val();
              if( val == 'feup_inside') {
                var n = $(this).closest('li').index();    
                var t = $('input[name="user_input['+n+'][label]"]').val();
                var p = $('input[name="user_input['+n+'][placeholder]"]').val();
                if(p == '') {
                    $('input[name="user_input['+n+'][placeholder]"]').val(t);
                }
              }
            });
        });
    })(jQuery);
    </script>
    <button class="button user-collapse"><?php _e('Toggle All Fields Open/Close', 'frontend_user_pro'); ?></button>
  </div>
  
  <?php
  if (empty($form_inputs)) { ?>
    <div class="user-updated">
      <p><?php echo $form; ?></p>
    </div>
    <?php
  } ?>
  
  <ul id="user-form-editor" class="user-form-editor unstyled <?php echo $clss; ?>">
  <?php
  if ($form_inputs) {
    $count = 0;
    foreach ($form_inputs as $order => $input_field) 
    {
      $name = ucwords(str_replace('_', ' ', $input_field['template']));
      $is_custom = apply_filters( 'user_formbuilder_custom_field', false, $input_field );
      if ( $is_custom ){
         $name = $input_field['input_type'];
         do_action('user_admin_field_' . $name, $count, $name, $input_field );
         $count++;
       }else if($form_inputs) 
       {
       // $count = 0;
        // foreach ($form_inputs as $order => $input_field) {
          $name = ucwords( str_replace( '_', ' ', $input_field['template'] ) );
          if ( $input_field['template'] == 'taxonomy') {
            $bb = $input_field['template'];
            USER_Formbuilder_Templates::$bb( $count, $name, $input_field['name'], $input_field );
          } else {
            $bb = $input_field['template'];
            USER_Formbuilder_Templates::$bb( $count, $name, $input_field );
          }
          $count++;
        // }
      }
    }
  } 
  ?>
  </ul>
  <?php
}
function user_form_notification_area() {
  global $post;
  $user  = wp_get_current_user();
    $new_mail_body = "Hi Admin,\r\n";
    $new_mail_body .= "A new registration has been created in your site BLOG_TITLE (BLOG_URL).\r\n\r\n";
    $mail_body = "Here is the details:\r\n";
    $mail_body .= "User Name: USERNAME\r\n";
    $mail_body .= "Role: ROLE\r\n";
    $user_mail_body1 = "Hi USERNAME,
         Your registration for BLOG_TITLE .
         You can log in, using your username and password that you created when registering for our website, at the following URL: LOGINLINK
         If you have any questions, or problems, then please do not hesitate to contact us.
         Name,
         Company,
         Contact details";
    $form_settings = get_post_meta( $post->ID, 'user_form_settings', true );
    
// echo '<pre>';
// print_r($form_settings);
// echo '</pre>'



    if( $post->post_type == 'user_registrations' ) { 
      $new_subject = isset( $form_settings['notification']['new_subject'] ) ? $form_settings['notification']['new_subject'] : __( 'New account has been created', 'frontend_user_pro' );
    }
    else {
      $new_subject = isset( $form_settings['notification']['new_subject'] ) ? $form_settings['notification']['new_subject'] : __( 'New post created', 'frontend_user_pro' ); 
    }
    $new_notificaton = isset( $form_settings['notification']['new'] ) ? $form_settings['notification']['new'] : 'on';
    $new_to = isset( $form_settings['notification']['new_to'] ) ? $form_settings['notification']['new_to'] : get_option( 'admin_email' );
    /*echo "<pre>";
    print_r($form_settings['notification']['new_body']);
    echo "</pre>";*/
    $new_body = isset( $form_settings['notification']['new_body'] ) ? $form_settings['notification']['new_body'] : $new_mail_body . $mail_body;
    $user_mail_body = isset( $form_settings['notification']['user_email'] ) ? $form_settings['notification']['user_email'] : $user_mail_body1;
    $user_email_subject = isset( $form_settings['notification']['user_email_subject'] ) ? $form_settings['notification']['user_email_subject'] : 'Your account has been created';
    $user_email_from = isset( $form_settings['notification']['user_email_from'] ) ? $form_settings['notification']['user_email_from'] : get_option( 'admin_email' );
    $user_email_from_name = isset( $form_settings['notification']['user_email_from_name'] ) ? $form_settings['notification']['user_email_from_name'] : 'Admin';
    $edit_notificaton = isset( $form_settings['notification']['edit'] ) ? $form_settings['notification']['edit'] : 'off';
    $edit_to = isset( $form_settings['notification']['edit_to'] ) ? $form_settings['notification']['edit_to'] : get_option( 'admin_email' );
    $edit_subject = isset( $form_settings['notification']['edit_subject'] ) ? $form_settings['notification']['edit_subject'] : __( 'A post has been edited', 'frontend_user_pro' );
    $edit_body = isset( $form_settings['notification']['edit_body'] ) ? $form_settings['notification']['edit_body'] : $new_mail_body . $mail_body;
    
    ?>
    <h3><?php _e( 'New Registration Notification', 'frontend_user_pro' ); ?></h3>
    <table class="form-table">
      <tr>
        <th><?php _e( 'Notification', 'frontend_user_pro' ); ?></th>
        <td>
          <label>
            <input type="hidden" name="user_settings[notification][new]" value="off">
            <input type="checkbox" name="user_settings[notification][new]" value="on"<?php checked( $new_notificaton, 'on' ); ?>>
            <?php _e( 'Enable registration notification', 'frontend_user_pro' ); ?>
          </label>
        </td>
      </tr>
      <tr>
        <th><?php _e( 'To', 'frontend_user_pro' ); ?></th>
        <td>
          <input type="text" name="user_settings[notification][new_to]" class="regular-text" value="<?php echo esc_attr( $new_to ) ?>">
          <div class="description" style="font-style:italic;">Who should this email be sent to?</div>
        </td>
      </tr>
      <tr>
        <th><?php _e( 'Subject', 'frontend_user_pro' ); ?></th>
        <td><input type="text" name="user_settings[notification][new_subject]" class="regular-text" value="<?php echo esc_attr( $new_subject ) ?>">
          <div class="description" style="font-style:italic;">This will be the subject of the email.</div>
        </td>
      </tr>
      <tr>
        <th><?php _e( 'Message', 'frontend_user_pro' ); ?></th>
        <td>
            <?php 
                $new_body = $new_body ; 
                $editor_id = 'user_settings_new_body';
                $settings = array( 
                                'wpautop' => true,
                                'media_buttons' => true,
                                'forced_root_block' => true,
                                'force_br_newlines' => false,
                                'force_p_newlines' => true,
                                'textarea_name' => 'user_settings[notification][new_body]',
                                'textarea_rows' => 6,
                                'editor_class'  => 'form_editor_class'
                                 );

                wp_editor( $new_body, $editor_id, $settings );



            ?>
        </td>
      </tr>
<tr>
<td colspan="2">
<h3><?php _e( 'User Registration Notification', 'frontend_user_pro' ); ?></h3>
</td>
</tr>
  <tr>
    <th><?php _e( 'From', 'frontend_user_pro' ); ?></th>
    <td>
      <input type="text" name="user_settings[notification][user_email_from]" class="regular-text" value="<?php echo esc_attr( $user_email_from ); ?>">
      <div class="description" style="font-style:italic;">Email will appear to be from this email address.</div>
    </td>
  </tr>
  <tr>
    <th><?php _e( 'From Name', 'frontend_user_pro' ); ?></th>
    <td>
      <input type="text" name="user_settings[notification][user_email_from_name]" class="regular-text" value="<?php echo esc_attr( $user_email_from_name ); ?>">
      <div class="description" style="font-style:italic;">Email will appear to be from this name.</div>
    </td>
  </tr>
  <tr>
    <tr>
      <th><?php _e( 'Subject', 'frontend_user_pro' ); ?></th>
      <td>
        <input type="text" name="user_settings[notification][user_email_subject]" class="regular-text" value="<?php echo esc_attr( $user_email_subject ); ?>">
        <div class="description" style="font-style:italic;">This will be the subject of the email.</div>
      </td>
    </tr>
  <tr>
  <th><?php _e( 'Message', 'frontend_user_pro' ); ?></th>
  <td>
  <?php 

      $user_mail_body = $user_mail_body; 
      $editor_id = 'user_settings_user_mail_body';
      $settings = array(

                'wpautop' => true,
                'media_buttons' => true,
                'forced_root_block' => true,
                'force_br_newlines' => false,
                'force_p_newlines' => true,
                'textarea_name' => 'user_settings[notification][user_email]',
                'textarea_rows' => 6,
                'editor_class'  => 'form_editor_class'
                                        
              );                       
      wp_editor( $user_mail_body, $editor_id, $settings );
  ?>
 </td>
</tr> 
</table>
<h3><?php _e( 'You may use in message:', 'frontend_user_pro' ); ?></h3>
        <p>
            <code>%post_title%</code>, <code>%post_content%</code>, <code>%post_excerpt%</code>, <code>%tags%</code>, <code>%category%</code>,
            <code>%author%</code>, <code>%sitename%</code>, <code>%siteurl%</code>, <code>%permalink%</code>, <code>%editlink%</code>
            <br><code>%custom_{NAME_OF_CUSTOM_FIELD}%</code> e.g: <code>%custom_website_url%</code> for <code>website_url</code> meta field
        </p>
    <?php
}
function user_form_settings_area() {
  global $post;
        $form_settings = get_post_meta( $post->ID, 'user_form_settings', true );
        $admin_email = get_option('admin_email');
        $email_body1 = 'Hi USERNAME,
        Congratulations! You are almost Register on BLOG_TITLE
        Please verify your account by clicking the button below.
        LOGINLINK';
        $role_selected = isset( $form_settings['role'] ) ? $form_settings['role'] : 'subscriber';
        $login_role = isset( $form_settings['login_role'] ) ? $form_settings['login_role'] : array('all');
        $login_user_redirect = isset( $form_settings['login_user_redirect'] ) ? $form_settings['login_user_redirect'] : array('all');
        $login_role_msg = isset( $form_settings['login_role_msg'] ) ? $form_settings['login_role_msg'] : 'You are not authorised to login here!';
        $redirect_to = isset( $form_settings['redirect_to'] ) ? $form_settings['redirect_to'] : 'page';
        $display_form = isset( $form_settings['display_form'] ) ? $form_settings['display_form'] : 'post';
          if( $post->post_type == 'user_registrations' ) { 
            $message = isset( $form_settings['message'] ) ? $form_settings['message'] : __( 'Registration successful', 'frontend_user_pro');
            $lostpassword_message = isset( $form_settings['lostpassword_message'] ) ? $form_settings['lostpassword_message'] : __( 'Please Enter User Email Id', 'frontend_user_pro');        
          } elseif( $post->post_type == 'user_logins' ) {                 
            $message = isset( $form_settings['message'] ) ? $form_settings['message'] : __( 'Login successful', 'frontend_user_pro');
            $lostpassword_message = isset( $form_settings['lostpassword_message'] ) ? $form_settings['lostpassword_message'] : __( 'Please Enter User Email Id', 'frontend_user_pro');
          }else{
            $message = isset( $form_settings['message'] ) ? $form_settings['message'] : __( '', 'frontend_user_pro');
          }
        
        $update_message = isset( $form_settings['update_message'] ) ? $form_settings['update_message'] : __( 'Form updated successfully', 'frontend_user_pro');
        $page_id = isset( $form_settings['page_id'] ) ? $form_settings['page_id'] : 0;
        $page_id_l = isset( $form_settings['page_id_l'] ) ? $form_settings['page_id_l'] : 0;
        $url = isset( $form_settings['url'] ) ? $form_settings['url'] : '';
        $submit_text = isset( $form_settings['submit_text'] ) ? $form_settings['submit_text'] : __( 'Submit', 'frontend_user_pro');
        $update_text = isset( $form_settings['update_text'] ) ? $form_settings['update_text'] : __( 'Update', 'frontend_user_pro');
        $email = isset( $form_settings['email_verify'] ) ? $form_settings['email_verify'] : 'no';
        $form_popup = isset( $form_settings['form_popup'] ) ? $form_settings['form_popup'] : 'no';
        $register_page_url = isset( $form_settings['register_page'] ) ? $form_settings['register_page'] : __( 'Registration Url', 'frontend_user_pro');
        $email_verify_content = isset( $form_settings['email_verify_content'] ) ? $form_settings['email_verify_content'] : $email_body1;
        $email_verify_content_sub = isset( $form_settings['email_verify_content_sub'] ) ? $form_settings['email_verify_content_sub'] : 'Confirmation Email';
        $email_verify_content_to = isset( $form_settings['email_verify_content_to'] ) ? $form_settings['email_verify_content_to'] : $admin_email;
        $email_verify_content_from = isset( $form_settings['email_verify_content_from'] ) ? $form_settings['email_verify_content_from'] : $admin_email;
        $email_verify_content_from_name = isset( $form_settings['email_verify_content_from_name'] ) ? $form_settings['email_verify_content_from_name'] : $admin_email;
        $progressbar_line = isset( $form_settings['progressbar_line'] ) ? $form_settings['progressbar_line'] : 'true';
        ?>
        <table class="form-table">
            <?php if( $post->post_type == 'user_registrations' ) { ?>
            <tr class="user-post-type">
                <th><?php _e( 'New User Role/Level', 'frontend_user_pro'); ?></th>
                <td>
                    <select name="user_settings[role]">
                        <?php
                        $user_roles = user_get_user_roles();
                        foreach ($user_roles as $role => $label) {
                            printf('<option value="%s"%s>%s</option>', $role, selected( $role_selected, $role, false ), $label );
                        }
                        ?>
                    </select>
                    <div class="description" style="font-style:italic;">
                        To Add new level <a href="<?php echo admin_url(); ?>admin.php?page=user-members&new_level=true"> Click here</a>
                    </div>
                </td>
            </tr>
            <?php } ?>
             <?php if( $post->post_type == 'user_registrations' || $post->post_type == 'user_resetpassword') { ?>
            <tr class="user-redirect-to">
                <th><?php _e( 'Redirect To', 'frontend_user_pro'); ?></th>
                <td>
                    <select name="user_settings[redirect_to]">
                        <?php
                        $redirect_options = array(
                            'same' => __( 'Same Page', 'wpuf' ),
                            'page' => __( 'To a page', 'wpuf' ),
                            'url' => __( 'To a custom URL', 'wpuf' )
                        );
                        foreach ($redirect_options as $to => $label) {
                            printf('<option value="%s"%s>%s</option>', $to, selected( $redirect_to, $to, false ), $label );
                        }
                        ?>
                    </select>
                    <div class="description" style="font-style:italic;">
                        <?php
                        if ($post->post_type == 'user_resetpassword') {
                            _e( 'After successful Reset Password, where the page will redirect to', 'wpuf' );
                        }else{
                         _e( 'After successful submit, where the page will redirect to', 'wpuf' ) ;
                        }
                          ?>
                    </div>
                </td>
            </tr>
 
            <tr class="user-page-id">
                <th><?php _e( 'Page', 'frontend_user_pro'); ?></th>
                <td>
                    <select name="user_settings[page_id]">
                        <?php
                        $pages = get_posts(  array( 'numberposts' => -1, 'post_type' => 'page') );
                        foreach ($pages as $page) {
                            printf('<option value="%s"%s>%s</option>', $page->ID, selected( $page_id, $page->ID, false ), esc_attr( $page->post_title ) );
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr class="user-url">
                <th><?php _e( 'Custom URL', 'frontend_user_pro'); ?></th>
                <td>
                    <input type="url" name="user_settings[url]" value="<?php echo esc_attr( $url ); ?>">
                </td>
            </tr>
            <?php } 
             if( $post->post_type == 'user_logins' ) { 
              $c = 0;
              if( !is_array($login_role) ) {
                $login_role = array('all');
              }
              foreach ($login_role as $key => $v) { ?>
              <tr class="user-post-type login_role_fr">
                <th><?php _e( 'Which level/Role User can login from this form', 'frontend_user_pro'); ?></th>
                <td>
                  if user is 
                  <select name="user_settings[login_role][0]" class="select_role_s">
                    <?php
                    $user_roles = user_get_user_roles();
                    printf('<option value="%s"%s>%s</option>', 'all', selected( $v, 'all', false ), 'All' ); 
                    foreach ($user_roles as $role => $label) {
                      printf('<option value="%s"%s>%s</option>', $role, selected( $v, $role, false ), $label );
                    }
                    ?>
                  </select>
                  then after login 
                  <p>redirect to
                    <select name="user_settings[page_id][0]">
                      <?php
                      $pages = get_posts(  array( 'numberposts' => -1, 'post_type' => 'page') );
                      foreach ($pages as $page) {
                        printf('<option value="%s"%s>%s</option>', $page->ID, selected( $page_id[$c], $page->ID, false ), esc_attr( $page->post_title ) );
                      }
                      ?>
                    </select>
                    <a class="rk_add" href="#">+</a><a class="rk_remove rv<?php echo $c; ?>" href="#">-</a>
                    <div style="font-style:italic;" class="description">
                        You can allow more then one level/role to login from this form. Click on plus button to add more level/role.
                    </div>
                  </p>
                </td>
              </tr>
       
                  <?php
                $c++;
              }
              ?>
                  <input type="hidden" name="user_settings[redirect_to]" value="page">
                  <script type="text/javascript">
                  (function($){
                    $(document).ready(function(){
                        $(document).on('click','.rk_add', function(e){
                          e.preventDefault();
                          var $div = $(this).closest('tr');
                          var ll = $('.login_role_fr').length;
                          var ul = $('.red_post_type').length;
                          var $clone = $div.clone();
                          if($clone.hasClass('login_role_fr')) {
                            $clone.find('[name*="user_settings[login_role]["]').attr('name', 'user_settings[login_role]['+ll+']');
                            $clone.find('[name*="user_settings[page_id]["]').attr('name', 'user_settings[page_id]['+ll+']');
                          }

                          if($clone.hasClass('red_post_type')) {
                            $clone.find('[name*="user_settings[login_user_redirect]["]').attr('name', 'user_settings[login_user_redirect]['+ul+']');
                            $clone.find('[name*="user_settings[page_id_l]["]').attr('name', 'user_settings[page_id_l]['+ul+']');
                          }
                          $div.after($clone);
                        });
                      $(document).on('click','.rk_remove', function(e){
                        e.preventDefault();
                        var i = $(this).closest('table').find('tr.user-post-type').length;
                        if( i > 1 ) {
                          $(this).closest('tr').remove();  
                        }
                      });
                      $(document).on('click','.rk_remove_l', function(e){
                        e.preventDefault();
                        var i = $(this).closest('table').find('tr.user-post-type-l').length;
                        if( i > 1 ) {
                          $(this).closest('tr').remove();  
                        }
                        
                      });
                    })
                  })(jQuery);
                  </script>
                  <tr class="">
                    <th><?php _e( 'If user is not allowed to login from this form then show this message', 'frontend_user_pro' ); ?></th>
                    <td>
                      <?php 
                          $login_role_msg = /*esc_textarea(*/ $login_role_msg /*)*/; 
                          $editor_id = 'user_settings_login_role_msg';
                          $settings = array( 
                                          'textarea_name' => 'user_settings[login_role_msg]',
                                          'textarea_rows' => 3,
                                          'editor_class'  => 'form_editor_class'
                                           );
                          wp_editor( $login_role_msg, $editor_id, $settings );
                      ?>
                    </td>
                </tr>
              <?php  
               $c1 = 0;
              if( !is_array($login_user_redirect) ) {
                $login_user_redirect = array('all');
              }
              foreach ($login_user_redirect as $key => $v) { ?>
              <tr class="user-post-type-l red_post_type">
                  <th><?php _e( 'If User Logged in and visit to login page so it redirect to this page ', 'frontend_user_pro'); ?></th>
                  <td>
                      <select name="user_settings[login_user_redirect][0]">
                        <option value="-1" selected> --None--</option>
                          <?php
                          $user_roles = user_get_user_roles();
                          printf('<option value="%s"%s>%s</option>', 'all', selected( $v, 'all', false ), 'All' ); 
                          foreach ($user_roles as $role => $label) {
                              printf('<option value="%s"%s>%s</option>', $role, selected( $v, $role, false ), $label );
                          }
                          ?>
                      </select>
                  Then Redirect To A Page
                  <select name="user_settings[page_id_l][0]">
                    <?php
                    $pages = get_posts(  array( 'numberposts' => -1, 'post_type' => 'page') );
                    foreach ($pages as $page) {
                        printf('<option value="%s"%s>%s</option>', $page->ID, selected( $page_id_l[$c1], $page->ID, false ), esc_attr( $page->post_title ) );
                    }
                    ?>
                </select>
                  <a class="rk_add" href="#">+</a><a class="rk_remove_l rv<?php echo $c; ?>" href="#">-</a>
                  </td>
              </tr> 
              <?php $c1++; } } ?> 
             
            <tr class="user-same-page">
                <?php if( $post->post_type == 'user_registrations' ) { ?>
                <th><?php _e( 'Registration success message', 'frontend_user_pro'); ?></th>
                <?php } elseif( $post->post_type == 'user_logins' ) { ?>                
                <th><?php _e( 'Login success message', 'frontend_user_pro'); ?></th>
                <?php } elseif( $post->post_type == 'user_resetpassword' ) { ?>
                <th><?php _e( 'Message Display above Reset password form', 'frontend_user_pro'); ?></th>
                <?php } ?>
                <td>
                      <?php 
                          $message = /*esc_textarea(*/ $message /*)*/; 
                          $editor_id = 'user_settings_message';
                          $settings = array( 
                                          'textarea_name' => 'user_settings[message]',
                                          'textarea_rows' => 3,
                                          'editor_class'  => 'form_editor_class'
                                           );
                          wp_editor( $message, $editor_id, $settings );
                      ?>
                </td>
            </tr>
            <?php if( $post->post_type == 'user_registrations' ) { ?>
            <tr class="user-same-page">
                <th><?php _e( 'Update profile message', 'frontend_user_pro' ); ?></th>
                <td>
                    <?php 
                          $update_message = /*esc_textarea(*/ $update_message /*)*/; 
                          $editor_id = 'user_settings_update_message';
                          $settings = array( 
                                          'textarea_name' => 'user_settings[update_message]',
                                          'textarea_rows' => 3,
                                          'editor_class'  => 'form_editor_class'
                                           );
                          wp_editor( $update_message, $editor_id, $settings );
                      ?>
                </td>
            </tr>
            <?php } ?>
            <?php if( $post->post_type == 'user_logins' ) { ?>
            <tr class="user-same-page">
                <th><?php _e( 'Lost Password message', 'frontend_user_pro' ); ?></th>
                <td>
                    <?php 
                          $lostpassword_message = /*esc_textarea(*/ $lostpassword_message /*)*/; 
                          $editor_id = 'user_settings_lostpassword_message';
                          $settings = array( 
                                          'textarea_name' => 'user_settings[lostpassword_message]',
                                          'textarea_rows' => 3,
                                          'editor_class'  => 'form_editor_class'
                                           );
                          wp_editor( $lostpassword_message, $editor_id, $settings );
                      ?>
                </td>
            </tr>
            <?php } ?>
            <?php if ($post->post_type != 'user_resetpassword') {
              ?>
                <tr class="user-submit-text">
                  <th><?php _e( 'Submit Button text', 'frontend_user_pro'); ?></th>
                  <td>
                    <input type="text" name="user_settings[submit_text]" value="<?php echo esc_attr( $submit_text ); ?>">
                  </td>
                </tr>
              <?php
            } ?>
            <?php if( $post->post_type == 'user_registrations' ) { ?>
            <tr class="user-submit-text">
                <th><?php _e( 'Update Button text', 'frontend_user_pro'); ?></th>
                <td>
                    <input type="text" name="user_settings[update_text]" value="<?php echo esc_attr( $update_text ); ?>">
                </td>
            </tr>
            <?php } ?>
            <?php if( $post->post_type == 'user_registrations' ) { ?>
            <tr class="user-verification-text">
                <th><?php _e( 'Email Verification', 'frontend_user_pro'); ?></th>
                <td>
                    <select name="user_settings[email_verify]">
                      <option value="no" <?php echo selected( $email, 'no', false ); ?> >NO</option>
                      <option value="yes" <?php echo selected( $email, 'yes', false ); ?> >Yes</option>
                    </select>
                    <div class="description" style="font-style:italic;">
                        Send verification mail after Registration
                    </div>
                </td>
            </tr>           
         
            <?php if($email == 'no') {
              echo " <tr class='email_verify_content' style='display:none;'>";
            } else{
              echo " <tr class='email_verify_content' >";
            }?>
            <th><?php _e( 'From', 'frontend_user_pro'); ?></th>
            <td>
              <input type="text" name="user_settings[email_verify_content_from]" value="<?php echo esc_attr( $email_verify_content_from ); ?>">
              <div class="description" style="font-style:italic;">Email will appear to be from this email address.</div>
            </td>
          </tr>
          <?php if($email == 'no') {
              echo " <tr class='email_verify_content' style='display:none;'>";
            } else{
              echo " <tr class='email_verify_content' >";
            }?>
            <th><?php _e( 'From Name', 'frontend_user_pro'); ?></th>
            <td>
              <input type="text" name="user_settings[email_verify_content_from_name]" value="<?php echo esc_attr( $email_verify_content_from_name ); ?>">
              <div class="description" style="font-style:italic;">Email will appear to be from this email address.</div>
            </td>
          </tr>
            <?php 
            if($email == 'no') {
              echo " <tr class='email_verify_content' style='display:none;'>";
            } else{
              echo " <tr class='email_verify_content' >";
            }?>
              <th><?php _e( 'Subject', 'frontend_user_pro'); ?></th>
              <td>
                <input type="text" name="user_settings[email_verify_content_sub]" value="<?php echo esc_attr( $email_verify_content_sub ); ?>">
                <div class="description" style="font-style:italic;">This will be the subject of the email.</div>
              </td>
            </tr>
            <?php if($email == 'no') {
              echo " <tr class='email_verify_content' style='display:none;'>";
            } else{
              echo " <tr class='email_verify_content' >";
            }?>
              <th><?php _e( 'Email Message', 'frontend_user_pro'); ?></th>
              <td>
                <?php 
                    $email_verify_content = /*esc_textarea(*/ $email_verify_content /*)*/; 
                    $editor_id = 'user_settings_email_verify_content';
                    $settings = array( 
                                    'textarea_name' => 'user_settings[email_verify_content]',
                                    'textarea_rows' => 3,
                                    'editor_class'  => 'form_editor_class'
                                     );
                    wp_editor( $email_verify_content, $editor_id, $settings );
                ?>
                <div class="description" style="font-style:italic;">Email Message Must Contain the shortcode LOGINLINK</div>
              </td>
            </tr> 
            <?php } ?>
            <?php if( $post->post_type == 'user_logins' || $post->post_type == 'user_resetpassword') { ?>
            <tr class="user-page-url">
                <th><?php _e( 'Registration Page', 'frontend_user_pro'); ?></th>
                <td>
                    <select name="user_settings[register_page]">
                        <?php
                        $pages = get_posts(  array( 'numberposts' => -1, 'post_type' => 'page') );
                        foreach ($pages as $page) {
                            printf('<option value="%s"%s>%s</option>', $page->ID, selected( $register_page_url, $page->ID, false ), esc_attr( $page->post_title ) );
                        }
                        ?>
                    </select>
                    <div class="description" style="font-style:italic;">
                        Select Registration page for this particular login form
                    </div>
                </td>
            </tr>
            <?php } ?>
            <?php if ($post->post_type == 'user_resetpassword') 
            {
              ?>
              <tr class="user-page-url">
                <th><?php _e( 'Login Page', 'frontend_user_pro'); ?></th>
                <td>
                  <select name="user_settings[login_page]">
                    <?php
                    $pages = get_posts(  array( 'numberposts' => -1, 'post_type' => 'page') );
                    foreach ($pages as $page) {
                      printf('<option value="%s"%s>%s</option>', $page->ID, selected( $login_page_url, $page->ID, false ), esc_attr( $page->post_title ) );
                    }
                    ?>
                  </select>
                  <div class="description" style="font-style:italic;">
                    Select Login page for this particular Reset password form
                  </div>
                </td>
              </tr>
              <?php
            } ?>
            <tr>
                <th><?php _e( 'Show Progressbar', 'frontend_user_pro' ); ?></th>
                <td>
                    <label>
                        <input type="hidden" name="user_settings[progressbar_line]" value="false">
                        <input type="checkbox" name="user_settings[progressbar_line]" value="true" <?php checked( $progressbar_line, 'true' ); ?> />
                    </label>
                    <div class="description" style="font-style:italic;"><?php _e( 'This will work if you have enabled multi step form', 'frontend_user_pro' ); ?></div>
                </td>
            </tr>
<!--  mahy -->

            <tr class="user-form-popup-text">
                <th><?php _e( 'Do you want this form in popup', 'frontend_user_pro'); ?></th>
                <td>
                    <select name="user_settings[form_popup]">
                      <option value="no" <?php echo selected( $form_popup, 'no', false ); ?> >NO</option>
                      <option value="yes" <?php echo selected( $form_popup, 'yes', false ); ?> >Yes</option>
                    </select>
                </td>
            </tr> 
        </table>
                <script type="text/javascript">
        jQuery(document).ready(function()
        {
            var $ = jQuery;
            var red = "<?php echo $redirect_to; ?>";
            // console.log(red);
            if (red != 'page' ) 
            {
                jQuery('select[name="user_settings[page_id]"]').closest('tr').css('display','none');
            }
            if(red != 'url')
            {
                jQuery('input[name="user_settings[url]"]').closest('tr').css('display','none');
            }
            jQuery(document).on('change','select[name="user_settings[redirect_to]"]',function()
            {
                var bb = jQuery(this).val();
                if (bb == 'page') 
                {
                    $('select[name="user_settings[page_id]"]').closest('tr').css('display','table-row');
                    $('input[name="user_settings[url]"]').closest('tr').css('display','none');
                }else if(bb == 'url')
                {
                    $('input[name="user_settings[url]"]').closest('tr').css('display','table-row');
                    $('select[name="user_settings[page_id]"]').closest('tr').css('display','none');
                }else
                {
                    $('input[name="user_settings[url]"]').closest('tr').css('display','none');
                    $('select[name="user_settings[page_id]"]').closest('tr').css('display','none');
                };
            });
            jQuery('select[name="user_settings[email_verify]"]').on('change',function(){
                var ch = jQuery(this).val();
                console.log(ch);
                if(ch == 'yes') {
                  jQuery('.email_verify_content').show();
                }
                else {
                  jQuery('.email_verify_content').hide();
                }
            })
        });
        </script>
        <?php
      }
function user_get_user_roles() {
    global $wp_roles;
    if ( !isset( $wp_roles ) )
        $wp_roles = new WP_Roles();
    return $wp_roles->get_names();
}
function user_forms_ajax_post_add_field() {
  
  $name = $_POST['name'];
  $type = $_POST['type'];
  $field_id = $_POST['order'];
  
  switch ($name) {
     case 'post_tag':
      USER_Formbuilder_Templates::post_tags($field_id, __('Tags', 'frontend_user_pro'));
     break;
     case 'post_types':
      USER_Formbuilder_Templates::post_types($field_id, __('Post type', 'frontend_user_pro'));
     break;
     case 'post_title':
      USER_Formbuilder_Templates::post_title($field_id, __('Post Title', 'frontend_user_pro'));
     break;
    case 'post_content':
      USER_Formbuilder_Templates::post_content($field_id, __('Post Content', 'frontend_user_pro'));
      break;
    case 'post_excerpt':
      USER_Formbuilder_Templates::post_excerpt($field_id, __('Excerpt', 'frontend_user_pro'));
      break;
    case 'featured_image':
      USER_Formbuilder_Templates::featured_image($field_id, __('Featured Image', 'frontend_user_pro'));
      break;
      
    case 'multiple_pricing':
      USER_Formbuilder_Templates::multiple_pricing($field_id, __('Prices and Files', 'frontend_user_pro'));
      break;
    case 'user_email':
      USER_Formbuilder_Templates::user_email($field_id, __('E-Mail', 'frontend_user_pro'));
      break;
    case 'custom_text':
      USER_Formbuilder_Templates::text_field($field_id, __('Custom field: Text', 'frontend_user_pro'));
      break;
    case 'custom_textarea':
      USER_Formbuilder_Templates::textarea_field($field_id, __('Custom field: Textarea', 'frontend_user_pro'));
      break;
    case 'custom_select':
      USER_Formbuilder_Templates::dropdown_field($field_id, __('Custom field: Select', 'frontend_user_pro'));
      break;
    case 'custom_image':
      USER_Formbuilder_Templates::image_upload($field_id, __('Custom field: Image Upload', 'frontend_user_pro'));
      break; 
    case 'custom_number':
      USER_Formbuilder_Templates::number($field_id, __('Custom field: Number', 'frontend_user_pro'));
    break; 
    
    case 'custom_multiselect':
      USER_Formbuilder_Templates::multiple_select($field_id, __('Custom field: Multiselect', 'frontend_user_pro'));
      break;
    case 'custom_radio':
      USER_Formbuilder_Templates::radio_field($field_id, __('Custom field: Radio', 'frontend_user_pro'));
      break;
    case 'custom_checkbox':
      USER_Formbuilder_Templates::checkbox_field($field_id, __('Custom field: Checkbox', 'frontend_user_pro'));
      break;
    case 'custom_file':
      USER_Formbuilder_Templates::file_upload($field_id, __('Custom field: File Upload', 'frontend_user_pro'));
      break;
    case 'custom_url':
      USER_Formbuilder_Templates::website_url($field_id, __('Custom field: URL', 'frontend_user_pro'));
      break;
    case 'custom_email':
      USER_Formbuilder_Templates::email_address($field_id, __('Custom field: E-Mail', 'frontend_user_pro'));
      break;
    case 'custom_repeater':
      USER_Formbuilder_Templates::repeat_field($field_id, __('Custom field: Repeat Field', 'frontend_user_pro'));
      break;
    case 'custom_html':
      USER_Formbuilder_Templates::custom_html($field_id, __('HTML', 'frontend_user_pro'));
      break;
    case 'text':
    USER_Formbuilder_Templates::text($field_id, __('Heading', 'frontend_user_pro'));
    break;
    case 'social_icon':
    USER_Formbuilder_Templates::social_icon($field_id, __('Social Icon', 'frontend_user_pro'));
    break;
    case 'section_break':
      USER_Formbuilder_Templates::section_break($field_id, __('Separator', 'frontend_user_pro'));
      break;
    case 'custom_multistep':
      USER_Formbuilder_Templates::custom_multistep($field_id, __('Multistep', 'frontend_user_pro'));
      break;    
    case 'action_hook':
      USER_Formbuilder_Templates::action_hook($field_id, __('Action Hook', 'frontend_user_pro'));
      break;
    case 'user_avatar':
      USER_Formbuilder_Templates::avatar($field_id, __('Profile Picture', 'frontend_user_pro'));
      break;
    // case 'recaptcha':
    //   USER_Formbuilder_Templates::recaptcha($field_id, __('reCaptcha', 'frontend_user_pro'));
    //   break;
    case 'custom_date':
      USER_Formbuilder_Templates::date_field($field_id, __('Custom Field: Date', 'frontend_user_pro'));
      break;
    case 'custom_hidden':
      USER_Formbuilder_Templates::custom_hidden_field($field_id, __('Hidden Field', 'frontend_user_pro'));
      break;
    case 'custom_country':
      USER_Formbuilder_Templates::country($field_id, __('Country', 'frontend_user_pro'));
      break;
    case 'toc':
      USER_Formbuilder_Templates::toc($field_id,  __('TOC', 'frontend_user_pro'));
      break;
    case 'user_login':
      USER_Formbuilder_Templates::user_login($field_id, __('Username', 'frontend_user_pro'));
      break;
    case 'first_name':
      USER_Formbuilder_Templates::first_name($field_id, __('First Name', 'frontend_user_pro'));
      break;
    case 'custom_remember':
      USER_Formbuilder_Templates::remember($field_id, __('Remember me', 'frontend_user_pro'));
      break;      
    case 'last_name':
      USER_Formbuilder_Templates::last_name($field_id, __('Last Name', 'frontend_user_pro'));
      break;
    case 'nickname':
      USER_Formbuilder_Templates::nickname($field_id, __('Nickname', 'frontend_user_pro'));
      break;
    case 'display_name':
      USER_Formbuilder_Templates::display_name($field_id, __('Display Name', 'frontend_user_pro'));
      break;
    case 'user_email':
      USER_Formbuilder_Templates::user_email($field_id, __('E-mail', 'frontend_user_pro'));
      break;
    case 'user_url':
      USER_Formbuilder_Templates::user_url($field_id, __('Website', 'frontend_user_pro'));
      break;
    case 'user_bio':
      USER_Formbuilder_Templates::description($field_id, __('Biographical Info', 'frontend_user_pro'));
      break;
    case 'password':
      USER_Formbuilder_Templates::password($field_id, __('Password', 'frontend_user_pro'));
      break;
    case 'password1':
      USER_Formbuilder_Templates::password1($field_id, __('Password', 'frontend_user_pro'));
      break;
    case 'level':
      USER_Formbuilder_Templates::level($field_id, __('Level', 'frontend_user_pro'));
      break;
    case 'really_simple_captcha':
      USER_Formbuilder_Templates::really_simple_captcha( $field_id, __('Really Simple Captcha','frontend_user_pro'));
      break;
    case 'custom_map':
      USER_Formbuilder_Templates::google_map($field_id, __('Custom Field: Google Map', 'frontend_user_pro'));
      break;
    case 'custom_mail_user':
      USER_Formbuilder_Templates::custom_mail_user($field_id, __('Register Email', 'frontend_user_pro'));
      break;
    case 'custom_css':
      USER_Formbuilder_Templates::custom_css($field_id, __('Custom Css', 'frontend_user_pro'));
      break;
    case 'custom_js':
      USER_Formbuilder_Templates::custom_js($field_id, __('Custom Js', 'frontend_user_pro'));
      break;
    case 'category':
      USER_Formbuilder_Templates::taxonomy( $field_id, 'Category', $type );
      break;
    case 'taxonomy':
      USER_Formbuilder_Templates::taxonomy( $field_id, 'Taxonomy: ' . $type, $type );
      break;    
    case 'zipcode_text':
      USER_Formbuilder_Templates::zipcode($field_id, __('Custom field: Zipcode', 'frontend_user_pro'));
      break;
    
    default:
      do_action('user_admin_field_' . $name, $field_id);
      break;
  }
  
  exit;
}
