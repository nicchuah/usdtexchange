/*
jQuery functions for the Admin area
*/
function changeFont( value, $ ) {
	$(".google-fontos").remove();
	if( "none" != value && "undefined" != typeof value ) {
		$("head").append('<link class="google-fontos" rel="stylesheet" href="http://fonts.googleapis.com/css?family=' + value + ':100,200,300,400,500,600,700,800,900&subset=latin,latin-ext,cyrillic,cyrillic-ext,greek-ext,greek,vietnamese" />');
		$(".feua-style.preview-zone p").css("font-family", "'" + value + "', sans-serif" );
	}
}
jQuery ( document ).ready( function( $ ) {
	var selectVar = $('select[name=feua_style_font_selector]');
	// if( $("#manual-style").length > 0 ){
	// 	var editor = CodeMirror.fromTextArea( document.getElementById( "manual-style" ), {
	// 		lineNumbers: true,
	// 		theme: "default",
	// 		mode:  "text/css",
	// 		indentUnit: 4,
	// 		styleActiveLine: true
	// 	});
	// }
	$('.feua-style-color-field').wpColorPicker();

	$("#select_all").on("click", function(){
		$(".feuastyle_body_select_all input").prop('checked', ( $(this).is(":checked") ) ? true : false );
	});

	changeFont( selectVar.val(), $ );
	selectVar.on( "change", function(){
		changeFont( $( this ).val(), $ );
	});
	$('#access_type_member').on('change', function(){
		var sel_val = $(this).val();
		if( sel_val =='date_interval' || sel_val =='limited' || sel_val =='unlimited' ) 
		{
			$('#billing_type_2 option[value="bl_onetime"]').show();
			$("#billing_type_2 option[value='bl_onetime']").attr('selected','selected'); 
			$('#billing_type_2').css('pointer-events' , 'none');
		}
		else {
			$("#billing_type_2 option[value='bl_ongoing']").attr('selected','selected'); 
			$('#billing_type_2 option[value="bl_onetime"]').hide();
			$('#billing_type_2').css('pointer-events' , 'auto');

		}
		$('.access_type1').hide();
		$('.type_'+sel_val).show();
	});

	$('#payment_options').on('change', function(){
		var sel_val = $(this).val();
		$('.access_type2').hide();
		$('.type_'+sel_val).show();
	});
	$('#billing_type_2').on('change', function(){
		var sel_val = $(this).val();
		$('.access_type3').hide();
		$('.type_'+sel_val).show();
	});
	

});

