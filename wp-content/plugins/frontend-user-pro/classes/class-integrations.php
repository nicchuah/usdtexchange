<?php
if ( !defined( 'ABSPATH' ) ) {
	exit;
}
class USER_Integrations {
	function __construct() {
    add_filter( 'frontend_download_supports', array(  $this, 'enable_reviews') );
    add_action('user_register',  array($this ,'member_register'));
    add_action('init',  array($this ,'member_check'));
    add_action('init',  array($this ,'level_paypal_check'));
    remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10 );
    add_action('woocommerce_before_checkout_form', array($this, 'render_login_reg_form'));
    add_action( 'wp_ajax_feup_login_woo_ajax', array( $this, 'feup_login_woo_ajax_login') );
    add_action( 'wp_ajax_nopriv_feup_login_woo_ajax', array( $this, 'feup_login_woo_ajax_login' ) );
    add_action( 'wp_ajax_feup_reg_woo_ajax', array( $this, 'feup_reg_woo_ajax_reg') );
    add_action( 'wp_ajax_nopriv_feup_reg_woo_ajax', array( $this, 'feup_reg_woo_ajax_reg' ) );
	}

  function render_login_reg_form() 
  {
    $form_id = get_option('user-woocommcerce-login-url');
    $form_id_reg = get_option('user-woocommcerce-reg-url');
    $front_login = get_option('user-woocommcerce-login-replace');
    if ($front_login == 1 && !is_user_logged_in()) 
    {
      if ($form_id) 
      { ?>
        <div class="woocommerce-info x-alert x-alert-info x-alert-block">
          Returning customer? 
          <a class="showfrontendlogin" href="#">Click here to login</a>  
        </div>
        <div class='frontent_login'><?php echo do_shortcode('[wpfeup-login id="'.$form_id.'"]'); ?></div>
        <script type="text/javascript">
          var $ = jQuery;
          $(document).ready(function(){
            // $('.frontent_login').empty();
            $('.showfrontendlogin').on('click', function(e){
              e.preventDefault();
              var form_id = '<?php echo $form_id; ?>';
              $('.frontent_login').toggle();
            })
          });
        </script>
        <style type="text/css">
          .frontent_login{
            display: none;
          }
        </style>
        <?php
      }
      if ($form_id_reg) 
      {?>
        <div class="woocommerce-info x-alert x-alert-info x-alert-block">
          For Unregistered customer? 
          <a class="showfrontendreg" href="#">Click here to Register</a>  
        </div>
        <div class='frontent_reg_woo'><?php echo do_shortcode('[wpfeup-register type="registration" id="'.$form_id_reg.'"]'); ?></div>
        <script type="text/javascript">
          var $ = jQuery;
          $(document).ready(function(){
            // $('.frontent_reg_woo').empty();
            $('.showfrontendreg').on('click', function(e){
              e.preventDefault();
              var form_id = '<?php echo $form_id; ?>';
              $('.frontent_reg_woo').toggle();
            })
          });
        </script>
        <style type="text/css">
          .frontent_reg_woo{
            display: none;
          }
        </style> <?php
      }    
    }
  }

  public static function member_register($user_id) 
  {
      global $wp_roles;
      $u = new WP_User($user_id );
      $role = implode(',', $u->roles);
      $cur_time=date("Y-m-d H:i:s");
      add_user_meta($user_id , '_frontend_user_time', $cur_time);
  }
  public static function member_check() 
  {
    global $wpdb;
    $tb2 =$wpdb->prefix."feup_member_payment";
    $sql = $wpdb->get_results("select * from $tb2");
    foreach ($sql as $key1 => $value1) 
    {
      $tb = $wpdb->prefix."user_member_list";
      $tb1 = $wpdb->get_results("SELECT * FROM $tb WHERE id = '".$value1->level_id."' ;");
      $expire = $value1->paydate;
      $user_id = $value1->user_id;
      foreach ($tb1 as $tb2) 
      {
        $time = $tb2->access_limited_time_value;
        $new_role = $tb2->afterexpire_level;
        $role = $tb2->level_slug;
        if($expire )
        {
          if($tb2->access_type == 'limited')
          {
            if ($tb2->access_limited_time_type == 'W') 
            {
              $time_inter = '+'.$time.' weeks';
            }elseif ($tb2->access_limited_time_type == 'M') 
            {
              $time_inter = '+'.$time.' months';
            }elseif ($tb2->access_limited_time_type == 'Y') 
            {
              $time_inter = '+'.$time.' years';
            }else
            {
              $time_inter = '+'.$time.' days';
            }
            $expire = strtotime($expire);
            $new_time = strtotime($time_inter, $expire);
            $cur_time=date("Y-m-d H:i:s");
            $cur_time = strtotime($cur_time);
            if($new_time <= $cur_time)
            {
              $user_id = wp_update_user( array( 'ID' => $user_id, 'role' => $new_role ) );   
            }
          }elseif ($tb2->access_type == 'date_interval') 
          {
            $role = $tb2->level_slug;
            $start = $tb2->access_interval_start;
            $end   = $tb2->access_interval_end;
            $start = strtotime($start);
            $end = strtotime($end);
            $cur_time=date("Y-m-d H:i:s");
            $cur_time = strtotime($cur_time);
            if($start <= $cur_time)
            {
                  $user_id = wp_update_user( array( 'ID' => $user_id, 'role' => $role ) );   
            }
            if($end <= $cur_time)
            {
                  $user_id = wp_update_user( array( 'ID' => $user_id, 'role' => $new_role ) );   
            }
          }elseif($tb2->access_type == 'regular_period')
          {
            if ($tb2->access_limited_time_type == 'W') 
            {
              $time_inter = '+'.$time.' weeks';
            }elseif ($tb2->access_limited_time_type == 'M') 
            {
              $time_inter = '+'.$time.' months';
            }elseif ($tb2->access_limited_time_type == 'Y') 
            {
              $time_inter = '+'.$time.' years';
            }else
            {
              $time_inter = '+'.$time.' days';
            }
            $expire = strtotime($expire);
            $new_time = strtotime($time_inter, $expire);
            $cur_time=date("Y-m-d H:i:s");
            $cur_time = strtotime($cur_time);
            if($new_time <= $cur_time)
            {
                 $user_id = wp_update_user( array( 'ID' => $user_id, 'role' => $new_role ) );   
            }
          }
        }
      }
    }
  }
  public function enable_reviews( $supports ) 
  {
    return array_merge( $supports, array(        'reviews'         ) );
  }
  public static function is_commissions_active() {
    if ( !defined( 'FRONTENDC_PLUGIN_DIR' ) ) {
         return false;
    } else {
         return true;
    }
  }

  function level_paypal_check() 
  {
    if (isset($_GET['payment_ip_check']) && isset($_GET['id'])) 
    {
      $cur = get_option('paypal-currency');
      $curncy = ($cur) ? $cur : 'USD' ;
      $paypal_email = get_option('paypal-email');
      $paypal_redirect = get_option('paypal-redirect');
      $paypal_redirect = get_permalink($paypal_redirect);
      $id = $_GET['id'];
      $paypal_sandbox  = get_option('paypal-sandbox');
      global $wpdb;
      $tb = $wpdb->prefix."user_member_list";
      $qrys = $wpdb->get_results("SELECT * FROM $tb WHERE id = $id");
      foreach ($qrys as $qry) 
      {
        $level_price = (isset($qry->level_price)) ? $qry->level_price : '' ;
        $access_limited_time_type = (isset($qry->access_limited_time_type)) ? $qry->access_limited_time_type : '' ;
        $access_regular_time_value = (isset($qry->access_regular_time_value)) ? $qry->access_regular_time_value : '' ;
        $billing_limit_num = (isset($qry->billing_limit_num)) ? $qry->billing_limit_num : '' ;
        $access_type = (isset($qry->access_type)) ? $qry->access_type : 'unlimited' ;
        $access_limited_time_value = (isset($qry->access_limited_time_value)) ? $qry->access_limited_time_value : '' ;
        if($access_type == 'date_interval') {
          $access_interval_start = (isset($qry->access_interval_start)) ? $qry->access_interval_start : '' ;
          $access_interval_end = (isset($qry->access_interval_end)) ? $qry->access_interval_end : '' ;
          $subscr_date = $access_interval_start;
          $subscr_effective = $access_interval_start;
          $date1=date_create($access_interval_start);
          $date2=date_create($access_interval_end);
          $diff=date_diff($date1,$date2);
        }

        if($qry->payment_type) 
        {
          if ($qry->payment_type == "payment") 
          {  
            if (isset($paypal_sandbox)) 
            {
              if ($paypal_sandbox == 0) 
              {
                $url = "https://www.paypal.com/cgi-bin/webscr";
              }
              if ($paypal_sandbox == 1) 
              {
                $url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
              }
            }else
            {
              $url = "https://www.paypal.com/cgi-bin/webscr";
            }
            ?>
            <html>
            <head><title>Redirecting to Paypal...</title>
            <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.12.0.min.js"></script>
            </head>
            <body>
              <form action="<?php echo $url; ?>" method="post" name="_xclick" id="levelform">
                <?php
              }elseif ($qry->payment_type == "free") 
              { 
                $qry->level_price = 0;
                $action = add_query_arg(array( 'payment_type' => 'free', 'item_number' => $id ) , site_url() );
                ?>
                <html>
                <head><title>Redirecting to Paypal...</title></head>
                <body>
                  <form action="<?php echo $action; ?>" method="post" name="_xclick" id="levelform">
                    <?php
                  }
                  if($access_type == 'date_interval') {
                      ?>
                      <input type="hidden" name="cmd" value="_xclick" /> 
                      <input type="hidden" name="subscr_date" value="<?php echo $subscr_date; ?>" /> 
                      <input type="hidden" name="subscr_effective" value="<?php echo $subscr_effective; ?>" />
                      <input type="hidden" name="t3" value="D"/> <!-- billing cycle unit=month -->
                      <input type="hidden" name="p3" value="<?php echo $diff->days; ?>"/> <!-- billing cycle length -->
                      <input type="hidden" name="amount" value="<?php echo $level_price; ?>" />
                    <?php
                  }
                  if ($access_type == 'unlimited') {
                      ?>
                      <input type="hidden" name="cmd" value="_xclick" /> 
                      <input type="hidden" name="amount" value="<?php echo $level_price; ?>" />
                      <?php
                  }
                  if ($access_type == 'limited') {
                      ?>
                      <input type="hidden" name="cmd" value="_xclick" /> 
                      <input type="hidden" name="t3" value="<?php echo $access_limited_time_type; ?>"/> <!-- billing cycle unit=month -->
                      <input type="hidden" name="p3" value="<?php echo $access_limited_time_value; ?>"/> <!-- billing cycle length -->
                      <input type="hidden" name="amount" value="<?php echo $level_price; ?>" />
                      <?php
                  }
                  if($access_type == 'regular_period'){
                    $src = 1;
                    $sra = 1;
                    ?>
                    <input type="hidden" name="cmd" value="_xclick-subscriptions" /> 
                    <input type="hidden" name="t3" value="<?php echo $access_limited_time_type; ?>"/> <!-- billing cycle unit=month -->
                    <input type="hidden" name="p3" value="<?php echo $access_regular_time_value; ?>"/> <!-- billing cycle length -->
                    <input type="hidden" name="src" value="<?php echo $src; ?>"> <!-- recurring=yes -->
                    <input type="hidden" name="sra" value="1"> <!-- reattempt=yes -->
                    <input type="hidden" name="srt" value="<?php echo $billing_limit_num; ?>">
                    <input type="hidden" name="a3" value="<?php echo $level_price; ?>" />
                    <?php
                  }
                  ?>
                  
                  <input type="hidden" name="item_number" value="<?php echo $id; ?>" />
                  <input type="hidden" name="item_name" value="<?php echo $qry->level_label; ?>" /> 
                  <input type="hidden" name="business" value="<?php echo $paypal_email; ?>">
                  <input type="hidden" name="currency_code" value="<?php echo $curncy; ?>">
                  <input type="hidden" name="return" value="<?php echo $paypal_redirect; ?>">
                  <input type="hidden" name="cancel_return" value="<?php echo $paypal_redirect; ?>">
                  <input class="pbutton" type="hidden" value="Buy Now" />                        <!-- <input type="image" alt="Make payments with PayPal - it's fast, free and secure!" name="submit" src="http://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" /> -->
                </form>
                <script type="text/javascript">
                var $ = jQuery;
                $(document).ready(function(){
                  $('#levelform').submit();
                });
                </script>
              </body>
              </html>
              <?php
              exit(); 
            }
          }
    }
  }
}
