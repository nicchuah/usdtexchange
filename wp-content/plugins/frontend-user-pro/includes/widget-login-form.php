<?php
/**
 * Creates a widget that allows users to add a login form to a widget area.
 *
 * @package Feup_Members
 * @subpackage Includes
 */

/**
 * Login form widget class.
 *
 * @since 0.1.0
 */
class Feup_Members_Widget_Login extends WP_Widget {

	/**
	 * Set up the widget's unique name, ID, class, description, and other options.
	 *
	 * @since 0.1.0
	 */
	public function __construct() {

		/* Set up the widget options. */
		$widget_options = array(
			'classname' => 'login',
			'description' => esc_html__( 'A widget that allows users to log into your site.', 'feup_members' )
		);

		/* Set up the widget control options. */
		$control_options = array(
			'id_base' => 'feup_members-widget-login'
		);

		/* Create the widget. */
		parent::__construct( 'feup_members-widget-login', esc_attr__( 'Frontend User Login Form', 'feup_members' ), $widget_options, $control_options );
	}

	/**
	 * Outputs the widget based on the arguments input through the widget controls.
	 *
	 * @since 0.1.0
	 */
	function widget( $args, $instance ) {
		extract( $args );
		$title         = apply_filters( 'widget_title', $instance['title'] );
		// $message    = $instance['message'];

		echo $before_widget;

		if ( $title ) {
			echo $before_title . $title . $after_title;
		}
		if(is_user_logged_in()) {  
			$show_msg = $instance['show_msg'];
			if( $show_msg == 1 ) {
				echo '<div>'.$instance['message'].'</div>';
			}
			echo '<a href="'.wp_logout_url().'">Logout</a>';
		}
		else {
			$form_id    = $instance['form_id'];
			echo do_shortcode('[wpfeup-login id="'.$form_id.'"]');
		}
	}

	/**
	 * Updates the widget control options for the particular instance of the widget.
	 *
	 * @since 0.1.0
	 */
	function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['form_id'] = strip_tags( $new_instance['form_id'] );
        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['show_msg'] = strip_tags( $new_instance['show_msg'] );
        $instance['message'] = strip_tags( $new_instance['message'] );
        return $instance;
	}

	/**
	 * Displays the widget control options in the Widgets admin screen.
	 *
	 * @since 0.1.0
	 */
	function form( $instance ) {

		/* Set up the default form values. */
		$form_id = '';
		$show_msg = 0;
		$message = 'User allready logged in';
		// $title      = esc_attr( $instance['title'] );
		if(isset($instance['title'])) {
			$title = esc_attr( $instance['title'] );	
		}
		if(isset($instance['form_id'])) {
			$form_id = esc_attr( $instance['form_id'] );	
		}
		if(isset($instance['show_msg'])) {
			$show_msg = $instance['show_msg'];
		}
		if(isset($instance['message'])) {
			$message = $instance['message'];
		}
		?>
		<p>
        <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
    </p>

		<?php
		$type = 'user_logins';
		$args=array(
		  'post_type' => $type,
		  'post_status' => 'publish',
		  'posts_per_page' => -1
		);
		$posts = get_posts($args);
		if( $posts ) { ?>
		<p><label>Select Login Form</label>
		<select id="<?php echo $this->get_field_id('form_id'); ?>" name="<?php echo $this->get_field_name('form_id'); ?>">
		<?php foreach ($posts as $key => $value) { ?>
		    <option value="<?php echo $value->ID; ?>" <?php if( $value->ID == $form_id) { echo 'selected="selected"'; } ?>><?php echo $value->post_title; ?></option>
		<?php }
		 echo '</select></p>';
		} ?>
		<p>
			<input type="checkbox" id="<?php echo $this->get_field_id('show_msg'); ?>" name="<?php echo $this->get_field_name('show_msg'); ?>" value="1" <?php if( $show_msg == 1 ) {echo 'checked="checked"'; } ?>/>
			<label>Show Message for logged in user</label>
		</p>
		<p>	
			<label>Message:</label>
			<input class="widefat" type="text" id="<?php echo $this->get_field_id('message'); ?>" name="<?php echo $this->get_field_name('message'); ?>" value="<?php echo $message; ?>"/>
		</p>	
		<?php
	}
}