<?php
defined( 'ABSPATH' ) or die( "No script kiddies please!" );

$fupsl_settings = array();
$fupsl_settings['network_ordering'] = $_POST['network_ordering'];


//for facebook settings
foreach( $_POST['fupsl_facebook_settings'] as $key => $value ) {
    $$key = sanitize_text_field( $value );
}

$fupsl_facebook_enable = isset( $fupsl_facebook_enable ) ? $fupsl_facebook_enable : '';

$facebook_parameters = array(	'fupsl_facebook_enable' => $fupsl_facebook_enable,
								'fupsl_facebook_app_id' => $fupsl_facebook_app_id,
								'fupsl_facebook_app_secret' => $fupsl_facebook_app_secret,
								'fupsl_profile_image_width' => $fupsl_profile_image_width,
								'fupsl_profile_image_height' => $fupsl_profile_image_height
							);
$fupsl_settings['fupsl_facebook_settings'] = $facebook_parameters;

//for twitter settings
foreach( $_POST['fupsl_twitter_settings'] as $key => $value ) {
    $$key = sanitize_text_field( $value );
}
$fupsl_twitter_enable = isset( $fupsl_twitter_enable ) ? $fupsl_twitter_enable : '';

$twitter_parameters = array('fupsl_twitter_enable' => $fupsl_twitter_enable, 'fupsl_twitter_api_key' => $fupsl_twitter_api_key, 'fupsl_twitter_api_secret' => $fupsl_twitter_api_secret);

$fupsl_settings['fupsl_twitter_settings'] = $twitter_parameters;

//for google settings
foreach( $_POST['fupsl_google_settings'] as $key => $value ) {
    $$key = sanitize_text_field( $value );
}

$fupsl_google_enable = isset( $fupsl_google_enable ) ? $fupsl_google_enable : '';

$google_parameters = array('fupsl_google_enable' => $fupsl_google_enable, 'fupsl_google_client_id' => $fupsl_google_client_id, 'fupsl_google_client_secret' => $fupsl_google_client_secret);

$fupsl_settings['fupsl_google_settings'] = $google_parameters;

$fupsl_settings['fupsl_enable_disable_plugin'] = $_POST['fupsl_enable_disable_plugin'];

$display_options = array();
if( isset( $_POST['fupsl_display_options'] ) ) {
    foreach( $_POST['fupsl_display_options'] as $key => $value ) {
        $display_options[] = $value;
    }
}

$fupsl_settings['fupsl_display_options'] = $display_options;
$fupsl_settings['fupsl_icon_theme'] = $_POST['fupsl_icon_theme'];
$fupsl_settings['fupsl_title_text_field'] = sanitize_text_field( $_POST['fupsl_title_text_field'] );
$fupsl_settings['fupsl_custom_logout_redirect_options'] = sanitize_text_field( $_POST['fupsl_custom_logout_redirect_options'] );
$fupsl_settings['fupsl_custom_logout_redirect_link'] = sanitize_text_field( $_POST['fupsl_custom_logout_redirect_link'] );
$fupsl_settings['fupsl_custom_login_redirect_options'] = sanitize_text_field( $_POST['fupsl_custom_login_redirect_options'] );
$fupsl_settings['fupsl_custom_login_redirect_link'] = sanitize_text_field( $_POST['fupsl_custom_login_redirect_link'] );
$fupsl_settings['fupsl_user_avatar_options'] = $_POST['fupsl_user_avatar_options'];
$fupsl_settings['fupsl_send_email_notification_options'] = $_POST['fupsl_send_email_notification_options'];

//for saving the settings
update_option( SFUP_SETTINGS, $fupsl_settings );
$_SESSION['fupsl_message'] = __( 'Settings Saved Successfully.', 'frontend_user_pro' );
wp_redirect( admin_url() . 'admin.php?page=' . 'fup-social-login' );
exit;
