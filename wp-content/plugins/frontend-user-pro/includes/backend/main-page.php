<?php defined( 'ABSPATH' ) or die( "No script kiddies please!" ); ?>
<div class="wrap"><div class='fupsl-outer-wrapper'>

        <div class="fupsl-setting-header clearfix">

            <div class="fupsl-headerlogo" style="margin-top:15px;">
                <div class="logo-wrap"></div>
                <div class="logo-content"><?php esc_attr_e( 'Social Settings', 'frontend_user_pro' ); ?><br />
                    </div>
            </div>

            <div class="clear"></div>
        </div>
        <div class="clear"></div>

        <?php
$options = get_option( SFUP_SETTINGS );
?>

        <?php if( isset( $_SESSION['fupsl_message'] ) ) { ?>
            <div class="fupsl-message">
                <p> <?php echo $_SESSION['fupsl_message'];
    unset( $_SESSION['fupsl_message'] ); ?>
                </p>
            </div>
<?php
} ?>
        <div class='fupsl-networks'>
            <div class='fupsl-network-options'>
                <form method="post" action="<?php echo admin_url() . 'admin-post.php' ?>">
                    <input type="hidden" name="action" value="fupsl_save_options"/>
                    <div class='fupsl-settings-tabs-wrapper clearfix'>
                        <ul class='fupsl-tab-wrapper-fix clearfix'>
                            <li><a href='javascript: void(0);' id='fupsl-networks-settings' class='fupsl-settings-tab fupsl-active-tab' ><?php _e( 'General settings', 'frontend_user_pro' ) ?></a></li>
                            <li><a href='javascript: void(0);' id='fupsl-theme-settings' class='fupsl-settings-tab' ><?php _e( 'Other settings', 'frontend_user_pro' ) ?></a></li>
                            <!-- <li><a href='javascript: void(0);' id='fupsl-how-to-use' class='fupsl-settings-tab' ><?php _e( 'How to use', 'frontend_user_pro' ) ?></a></li> -->
                            
                        </ul>
                    </div>
                    <div class="clear"></div>
                    <div class='fupsl-setting-tabs-wrapper'>
                        <div class='fupsl-tab-contents' id='tab-fupsl-networks-settings'>

                            <div class='network-settings'>

<?php
/*echo "<pre>";
print_r($options['network_ordering']);
echo "</pre>";*/

?>

                                <?php if ($options['network_ordering']) {foreach( $options['network_ordering'] as $key => $value ): ?>
                                    <?php
    switch( $value ) {
        case 'facebook':
?>
                                            <div class='fupsl-settings fupsl-facebook-settings'>
                                                <!-- Facebook Settings -->
                                                <div class='fupsl-label'><?php _e( "Facebook", 'frontend_user_pro' ); ?><span class='fupsl_show_hide' id='fupsl_show_hide_<?php echo $value; ?>'><i class="fa fa-caret-down"></i></span> </div>
                                                <div class='fupsl_network_settings_wrapper' id='fupsl_network_settings_<?php echo $value; ?>' style='display:none'>
                                                    <div class='fupsl-enable-disable'>
                                                        <label><?php _e( 'Enable?', 'frontend_user_pro' ); ?></label>
                                                        <input type='hidden' name='network_ordering[]' value='facebook' />
                                                        <input type="checkbox" id='aspl-facbook-enable' value='enable' name='fupsl_facebook_settings[fupsl_facebook_enable]' <?php checked( 'enable', $options['fupsl_facebook_settings']['fupsl_facebook_enable'] ); ?>  />
                                                    </div>
                                                    <div class='fupsl-app-id-wrapper'>
                                                        <label><?php _e( 'App ID:', 'frontend_user_pro' ); ?></label><input type='text' id='fupsl-facebook-app-id' name='fupsl_facebook_settings[fupsl_facebook_app_id]' value='<?php
            if( isset( $options['fupsl_facebook_settings']['fupsl_facebook_app_id'] ) ) {
                echo $options['fupsl_facebook_settings']['fupsl_facebook_app_id'];
            }
?>' />
                                                    </div>
                                                    <div class='fupsl-app-secret-wrapper'>
                                                        <label><?php _e( 'App Secret:', 'frontend_user_pro' ); ?></label><input type='text' id='fupsl-facebook-app-secret' name='fupsl_facebook_settings[fupsl_facebook_app_secret]' value='<?php
            if( isset( $options['fupsl_facebook_settings']['fupsl_facebook_app_secret'] ) ) {
                echo $options['fupsl_facebook_settings']['fupsl_facebook_app_secret'];
            }
?>' />
                                                    </div>

                                                    <div class='fupsl-fb-profile-image-size'>
                                                        <label><?php _e( 'Profile picture image size', 'frontend_user_pro' ); ?></label><br />
                                                        <label for='fupsl-fb-profile-image-width'><?php _e( 'Width:', 'frontend_user_pro' ); ?></label>  <input type='number' name='fupsl_facebook_settings[fupsl_profile_image_width]' id='fupsl-fb-profile-image-width' value='<?php
                                                                                                                                                                                                if( isset( $options['fupsl_facebook_settings']['fupsl_profile_image_width'] ) ) {
                                                                                                                                                                                                    echo $options['fupsl_facebook_settings']['fupsl_profile_image_width'];
                                                                                                                                                                                                }
                                                                                                                                                                                    ?>' /> px
                                                        <br />
                                                        <label for='fupsl-fb-profile-image-height'><?php _e( 'Height:', 'frontend_user_pro' ); ?></label> <input type='number' name='fupsl_facebook_settings[fupsl_profile_image_height]' id='fupsl-fb-profile-image-height' value='<?php
                                                                                                                                                                                                if( isset( $options['fupsl_facebook_settings']['fupsl_profile_image_height'] ) ) {
                                                                                                                                                                                                    echo $options['fupsl_facebook_settings']['fupsl_profile_image_height'];
                                                                                                                                                                                                }
                                                                                                                                                                                    ?>' /> px
                                                    <div class='fupsl-info'>Please note that php version 5.4 is required.</div>                                                                                                                                
                                                    </div>
                                                    <div class='fupsl-info'>
                                                        <span class='fupsl-info-note'><?php _e( 'instructions:', 'frontend_user_pro' ); ?></span> <br />
                                                        <span class='fupsl-info-content'>You need to create a new facebook API Applitation to setup facebook login.</span>
                                                        <br />
                                                        <ul class='fupsl-info-lists'>
                                                          
                                                            <li>Go to <a href='https://developers.facebook.com/apps' target='_blank'>https://developers.facebook.com/apps</a>.</li>
                                                            <li>Click on 'Add a New App' button. A popup will open. Then choose website.</li>
                                                            <li>Now please enter the name of the app as you wish. And please click on “Create New Facebook App ID” button, Again a popup will appear</li>
                                                            <li>Please enter your Email and category as you need.</li>
                                                            <li>Now click on “Create App ID’ button.</li>
                                                            <li>Now the popup will disappear and please scroll down and there you will find a input field for your Site URL, there Please enter your site url and enter “Next” button. Now the app is created successfully.</li>
                                                            <li>Now go to <a href='https://developers.facebook.com/apps' target='_blank'>https://developers.facebook.com/apps</a> there you will find your newly created app and click on the app that you have created. You will be redirected to app dashboard.</li>
                                                            <li>In the landing page you will find the API version, App ID, App Secret. To view your App secret please click on “Show” button. Those are the required App ID and App Secret to be entered in our plugin settings.</li>
                                                            <li>After that please go to App Review link just below the alert link there you will find an option to make the app public and select YES. This is very important otherwise your app will not work for all users.</li>
                                                            <li>Site url: <input type='text' value='<?php echo site_url(); ?>' readonly='readonly' /></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
            <?php
        break; ?>

        <?php
        case 'twitter': ?>
                                            <div class='fupsl-settings fupsl-twitter-settings'>
                                                <!-- Twitter Settings -->
                                                <div class='fupsl-label'><?php _e( "Twitter", 'frontend_user_pro' ); ?> <span class='fupsl_show_hide' id='fupsl_show_hide_<?php echo $value; ?>'><i class="fa fa-caret-down"></i></span> </div>
                                                <div class='fupsl_network_settings_wrapper' id='fupsl_network_settings_<?php echo $value; ?>' style='display:none'>
                                                    <div class='fupsl-enable-disable'> 
                                                        <label><?php _e( 'Enable?', 'frontend_user_pro' ); ?></label>
                                                        <input type="checkbox" id='aspl-twitter-enable' value='enable' name='fupsl_twitter_settings[fupsl_twitter_enable]' <?php checked( 'enable', $options['fupsl_twitter_settings']['fupsl_twitter_enable'] ); ?>  />
                                                    </div>

                                                    <div class='fupsl-app-id-wrapper'>
                                                        <label><?php _e( 'Consumer Key (API Key):', 'frontend_user_pro' ); ?></label><input type='text' id='fupsl-twitter-app-id' name='fupsl_twitter_settings[fupsl_twitter_api_key]' value='<?php
            if( isset( $options['fupsl_twitter_settings']['fupsl_twitter_api_key'] ) ) {
                echo $options['fupsl_twitter_settings']['fupsl_twitter_api_key'];
            }
?>' />
                                                    </div>

                                                    <div class='fupsl-app-secret-wrapper'>
                                                        <label><?php _e( 'Consumer Secret (API Secret):', 'frontend_user_pro' ); ?></label><input type='text' id='fupsl-twitter-app-secret' name='fupsl_twitter_settings[fupsl_twitter_api_secret]' value='<?php
            if( isset( $options['fupsl_twitter_settings']['fupsl_twitter_api_secret'] ) ) {
                echo $options['fupsl_twitter_settings']['fupsl_twitter_api_secret'];
            }
?>' />
                                                    </div>

                                                    <input type='hidden' name='network_ordering[]' value='twitter' />
                                                    <div class='fupsl-info'>
                                                        <span class='fupsl-info-note'><?php _e( 'instructions:', 'frontend_user_pro' ); ?> <br /> </span>
                                                        <span class='fupsl-info-content'>You need to create new twitter API application to setup the twitter login.</span>
                                                        <ul class='fupsl-info-lists'>
                                                            <li>Go to <a href='https://apps.twitter.com/' target='_blank'>https://apps.twitter.com/</a></li>
                                                            <li>Click on Create New App button. A new application details form will appear. Please fill up the application details and click on "create your twitter application" button.</li>
                                                            <li>Please note that before creating twiiter API application, You must add your mobile phone to your Twitter profile.</li>
                                                            <li>After successful creation of the app. Please go to "Keys and Access Tokens" tabs and get Consumer key(API Key) and Consumer secret(API secret).</li>
                                                            <li>Website: <input type='text' value='<?php echo site_url(); ?>' readonly='readonly'/></li>
                                                            <li>Callback URL: <input type='text' value='<?php echo site_url(); ?>' readonly='readonly'/></li>
                                                        </ul>

                                                    </div>
                                                </div>  
                                            </div>
            <?php
        break;
        case 'google':
?>
                                            <div class='fupsl-settings fupsl-google-settings'>
                                                <!-- Google Settings -->
                                                <div class='fupsl-label'><?php _e( "Google", 'frontend_user_pro' ); ?> <span class='fupsl_show_hide' id='fupsl_show_hide_<?php echo $value; ?>'><i class="fa fa-caret-down"></i></span> </div>
                                                <div class='fupsl_network_settings_wrapper' id='fupsl_network_settings_<?php echo $value; ?>' style='display:none'>
                                                    <div class='fupsl-enable-disable'> 
                                                        <label><?php _e( 'Enable?', 'frontend_user_pro' ); ?></label>
                                                        <input type="checkbox" id='aspl-google-enable' value='enable' name='fupsl_google_settings[fupsl_google_enable]' <?php checked( 'enable', $options['fupsl_google_settings']['fupsl_google_enable'] ); ?>  />
                                                    </div>
                                                    <div class='fupsl-app-id-wrapper'>
                                                        <label><?php _e( 'Client ID:', 'frontend_user_pro' ); ?></label><input type='text' id='fupsl-google-client-id' name='fupsl_google_settings[fupsl_google_client_id]' value='<?php
            if( isset( $options['fupsl_google_settings']['fupsl_google_client_id'] ) ) {
                echo $options['fupsl_google_settings']['fupsl_google_client_id'];
            }
?>' />
                                                    </div>
                                                    <div class='fupsl-app-secret-wrapper'>
                                                        <label><?php _e( 'Client Secret:', 'frontend_user_pro' ); ?></label><input type='text' id='fupsl-google-client-secret' name='fupsl_google_settings[fupsl_google_client_secret]' value='<?php
            if( isset( $options['fupsl_google_settings']['fupsl_google_client_secret'] ) ) {
                echo $options['fupsl_google_settings']['fupsl_google_client_secret'];
            }
?>' />
                                                    </div>
                                                    <input type='hidden' name='network_ordering[]' value='google' />
                                                    <div class='fupsl-info'>
                                                        <span class='fupsl-info-note'><?php _e( 'instructions:', 'frontend_user_pro' ); ?></span> <br />
                                                        <span class='fupsl-info-content'>You need to create new google API application to setup the google login.</span>
                                                        <ul class='fupsl-info-lists'>
                                                            <li>Go to <a href='https://console.developers.google.com/project' target='_blank'>https://console.developers.google.com/project.</a> </li>
                                                            <li>Click on "Create Project" button. A popup will appear.</li>
                                                            <li>Please enter Project name and click on "Create" button.</li>
                                                            <li>A App will be created and a dashobard will appear.</li>
                                                            <li>In the blue box please click on Enable and manage APIs link. A new page will load.</li>
                                                            <li>Now In the Social APIs section click on Google+ API and click "Enable API" button. Then the Google+ API will be activated.</li>
                                                            <li>Now click on Credentials section and go to OAuth consent screen and enter the app details there.</li>
                                                            <li>Click on Credentials tab and click on "New credentials" or "Add credentials" if you have already created one, a selection will appear and click on "OAuth client ID".</li>
                                                            <li>A new page will load. Please select Application type to Web application and click "create" button. Further forms will loaded up and enter the details there.</li>
                                                            <li>In the authorized redirect URIs please enter the details provided in the note section from plugin and click save button.</li>
                                                            <li>In the popup you will get Client ID and client secret.</li>
                                                            <li>And please enter those credentials in the google setting in our plugin.</li>
                                                            <li>Rediret uri setup:<br />
                                                                Please use <input type='text' value='<?php echo site_url(); ?>/wp-login.php?fupsl_login_id=google_check' readonly='readonly'/> - for wordpress login page.<br />
                                                                Please use <input type='text' value='<?php echo site_url(); ?>/index.php?fupsl_login_id=google_check' readonly='readonly'/> - if you have used the shortcode or widget in frontend.
                                                            </li>
                                                            <li>
                                                                Please note: Make sure to check the protocol "http://" or "https://" as google checks protocol as well. Better to add both URL in the list if you site is https so that google social login work properly for both https and http browser.
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
            <?php
        break; ?>

        <?php
        default:
            echo "should not reach here";
        break;
    }
?>
<?php
endforeach; } ?>
                            </div>
                        </div>

                        <div class='fupsl-tab-contents' id='tab-fupsl-theme-settings' style="display:none">

                            <div class='fupsl-settings'>
                                <div class='fupsl-enable-disable-opt'>
                                    <div class="fupsl-label"><?php _e( 'Social login', 'frontend_user_pro' ); ?> <span class='fupsl_show_hide'><i class="fa fa-caret-down"></i></span> </div>
                                    <div class='fupsl_network_settings_wrapper' style='display:none'>
                                        <p class="social-login">
                                            <span><?php _e( 'Enable social login?', 'frontend_user_pro' ); ?></span>
                                            <input type='radio' id='fupsl_enable_plugin' name='fupsl_enable_disable_plugin' value='yes' <?php checked( $options['fupsl_enable_disable_plugin'], 'yes', 'true' ); ?> /> <label for='fupsl_enable_plugin'>Yes</label>
                                            <input type='radio' id='fupsl_disable_plugin' name='fupsl_enable_disable_plugin' value='no' <?php checked( $options['fupsl_enable_disable_plugin'], 'no', 'true' ); ?> /> <label for='fupsl_disable_plugin'>No</label>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class='fupsl-settings'>
                                <div class='fupsl-display-options'>
                                    <div class="fupsl-label"><?php _e( 'Display options', 'frontend_user_pro' ); ?> <span class='fupsl_show_hide'><i class="fa fa-caret-down"></i></span></div>
                                    <div class='fupsl_network_settings_wrapper' style='display:none'>
                                        <p><?php _e( 'Please choose the options where you want to display the social login form.', 'frontend_user_pro' ); ?></p>
                                        <p><input type="checkbox" id="fupsl_login_form" value="login_form" name="fupsl_display_options[]" <?php
if( in_array( "login_form", $options['fupsl_display_options'] ) ) {
    echo "checked='checked'";
}
?> ><label for="fupsl_login_form"><?php _e( 'Login Form', 'frontend_user_pro' ); ?> </label></p>
                                        <p><input type="checkbox" id="fupsl_register_form" value="register_form" name="fupsl_display_options[]" <?php
if( in_array( "register_form", $options['fupsl_display_options'] ) ) {
    echo "checked='checked'";
}
?> ><label for="fupsl_register_form"><?php _e( 'Register Form', 'frontend_user_pro' ); ?> </label></p>
                                        <p><input type="checkbox" id="fupsl_comment_form" value="comment_form" name="fupsl_display_options[]" <?php
if( in_array( "comment_form", $options['fupsl_display_options'] ) ) {
    echo "checked='checked'";
}
?> ><label for="fupsl_comment_form"><?php _e( 'Comments', 'frontend_user_pro' ); ?> </label></p>
                                    </div>
                                </div>
                            </div>

                            <div class='fupsl-settings'>
                                <div class='fupsl-themes-wrapper'>
                                    <div class="fupsl-label"><?php _e( 'Available icon themes', 'frontend_user_pro' ); ?> <span class='fupsl_show_hide'><i class="fa fa-caret-down"></i></span> </div>
                                    <div class='fupsl_network_settings_wrapper' style='display:none'>
<?php for( $i = 1;$i <= 4;$i++ ): ?>
                                            <div class='fupsl-theme fupsl-theme-<?php echo $i; ?>'>
                                                <label><input type="radio" id="fupsl-theme-<?php echo $i; ?>" value="<?php echo $i; ?>" class="fupsl-theme fupsl-png-theme" name="fupsl_icon_theme" <?php checked( $i, $options['fupsl_icon_theme'] ); ?> >
                                                    <span><?php _e( 'Theme ' . $i, 'frontend_user_pro' ); ?></span></label>
                                                <div class="fupsl-theme-previewbox">
                                                    <img src="<?php echo SFUP_IMAGE_DIR; ?>/preview-<?php echo $i; ?>.jpg" alt="theme preview">
                                                </div>
                                            </div>
<?php
endfor; ?>
                                    </div>
                                </div>
                            </div>

                            <div class='fupsl-settings'>
                                <div class='fupsl-text-settings'>
                                    <div class="fupsl-label"><?php _e( 'Text settings', 'frontend_user_pro' ); ?> <span class='fupsl_show_hide'><i class="fa fa-caret-down"></i></span> </div>
                                    <div class='fupsl_network_settings_wrapper' style='display:none'>
                                        <p class='fupsl-title-text-field'>
                                            <span><?php _e( 'Login text:', 'frontend_user_pro' ); ?></span> <input type='text' name='fupsl_title_text_field' id='fupsl-title-text' value='<?php
if( isset( $options['fupsl_title_text_field'] ) && $options['fupsl_title_text_field'] != '' ) {
    echo $options['fupsl_title_text_field'];
}
?>' />
                                        </p>
                                    </div>  
                                </div>
                            </div>

                            <div class='fupsl-settings'>
                                <div class='fupsl-logout-redirect-settings'>
                                    <div class="fupsl-label"><?php _e( 'Logout redirect link', 'frontend_user_pro' ); ?> <span class='fupsl_show_hide'><i class="fa fa-caret-down"></i></span> </div>
                                    <div class='fupsl_network_settings_wrapper' style='display:none'>
                                        <input type='radio' id='fupsl_custom_logout_redirect_home' class='fupsl_custom_logout_redirect_options' name='fupsl_custom_logout_redirect_options' value='home' <?php
if( isset( $options['fupsl_custom_logout_redirect_options'] ) ) {
    checked( $options['fupsl_custom_logout_redirect_options'], 'home', 'true' );
}
?> /> <label for='fupsl_custom_logout_redirect_home'><?php _e( 'Home page', 'frontend_user_pro' ); ?></label><br /><br />
                                        <input type='radio' id='fupsl_custom_logout_redirect_current' class='fupsl_custom_logout_redirect_options' name='fupsl_custom_logout_redirect_options' value='current_page' <?php
if( isset( $options['fupsl_custom_logout_redirect_options'] ) ) {
    checked( $options['fupsl_custom_logout_redirect_options'], 'current_page', 'true' );
}
?> /> <label for='fupsl_custom_logout_redirect_current'><?php _e( 'Current page', 'frontend_user_pro' ); ?></label><br /><br />


                                        <input type='radio' id='fupsl_custom_logout_redirect_custom' class='fupsl_custom_logout_redirect_options' name='fupsl_custom_logout_redirect_options' value='custom_page' <?php
if( isset( $options['fupsl_custom_logout_redirect_options'] ) ) {
    checked( $options['fupsl_custom_logout_redirect_options'], 'custom_page', 'true' );
}
?> /> <label for='fupsl_custom_logout_redirect_custom'><?php _e( 'Custom page', 'frontend_user_pro' ); ?></label><br />

                                        <div class='fupsl-custom-logout-redirect-link' <?php if( isset( $options['fupsl_custom_logout_redirect_options'] ) ) {
    if( $options['fupsl_custom_logout_redirect_options'] == 'custom_page' ) {
?> style='display: block' <?php
    } 
    else { ?> style='display:none' <?php
    }
}
?>>
                                            <p class='fupsl-title-text-field'>
                                                <span><?php _e( 'Logout redirect page:', 'frontend_user_pro' ); ?></span> <input type='text' name='fupsl_custom_logout_redirect_link' id='fupsl-custom-logout-redirect-link' value='<?php
if( isset( $options['fupsl_custom_logout_redirect_link'] ) && $options['fupsl_custom_logout_redirect_link'] != '' ) {
    echo $options['fupsl_custom_logout_redirect_link'];
}
?>' />
                                            </p>
                                            <div class='fupsl-info'>
                                                <span class='fupsl-info-note'><?php _e( 'instructions:', 'frontend_user_pro' ); ?></span> <br />
                                                <span class='fupsl-info-content'>Please set this value if you want to redirect the user to the custom page url(full url). If this field is not set they will be redirected back to current page.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class='fupsl-settings'>
                                <div class='fupsl-login-redir
                                ect-settings'>
                                    <div class="fupsl-label"><?php _e( 'Login redirect link', 'frontend_user_pro' ); ?> <span class='fupsl_show_hide'><i class="fa fa-caret-down"></i></span> </div>
                                    <div class='fupsl_network_settings_wrapper' style='display:none'>
                                        <input type='radio' id='fupsl_custom_login_redirect_home' class='fupsl_custom_login_redirect_options' name='fupsl_custom_login_redirect_options' value='home' <?php
if( isset( $options['fupsl_custom_login_redirect_options'] ) ) {
    checked( $options['fupsl_custom_login_redirect_options'], 'home', 'true' );
}
?> /> <label for='fupsl_custom_login_redirect_home'><?php _e( 'Home page', 'frontend_user_pro' ); ?></label><br /><br />
                                        <input type='radio' id='fupsl_custom_login_redirect_current' class='fupsl_custom_login_redirect_options' name='fupsl_custom_login_redirect_options' value='current_page' <?php
if( isset( $options['fupsl_custom_login_redirect_options'] ) ) {
    checked( $options['fupsl_custom_login_redirect_options'], 'current_page', 'true' );
}
?> /> <label for='fupsl_custom_login_redirect_current'><?php _e( 'Current page', 'frontend_user_pro' ); ?></label><br /><br />
                                        <div class='fupsl-custom-login-redirect-link1' >
                                            <div class='fupsl-info'>
                                                <span class='fupsl-info-note'><?php _e( 'instructions:', 'frontend_user_pro' ); ?></span> <br />
                                                <span class='fupsl-info-content'> If plugin can't detect what is the redirect uri for the page it will be redirected to home page.</span>
                                            </div>
                                        </div>
                                        <input type='radio' id='fupsl_custom_login_redirect_custom' class='fupsl_custom_login_redirect_options' name='fupsl_custom_login_redirect_options' value='custom_page' <?php
if( isset( $options['fupsl_custom_login_redirect_options'] ) ) {
    checked( $options['fupsl_custom_login_redirect_options'], 'custom_page', 'true' );
}
?> /> <label for='fupsl_custom_login_redirect_custom'><?php _e( 'Custom page', 'frontend_user_pro' ); ?></label><br />

                                        <div class='fupsl-custom-login-redirect-link' <?php if( isset( $options['fupsl_custom_login_redirect_options'] ) ) {
    if( $options['fupsl_custom_login_redirect_options'] == 'custom_page' ) {
?> style='display: block' <?php
    } 
    else { ?> style='display:none' <?php
    }
}
?>>
                                            <p class='fupsl-title-text-field'>
                                                <span><?php _e( 'Login redirect page:', 'frontend_user_pro' ); ?></span> <input type='text' name='fupsl_custom_login_redirect_link' id='fupsl-custom-login-redirect-link' value='<?php
if( isset( $options['fupsl_custom_login_redirect_link'] ) && $options['fupsl_custom_login_redirect_link'] != '' ) {
    echo $options['fupsl_custom_login_redirect_link'];
}
?>' />
                                            </p>
                                            <div class='fupsl-info'>
                                                <span class='fupsl-info-note'><?php _e( 'instructions:', 'frontend_user_pro' ); ?></span> <br />
                                                <span class='fupsl-info-content'>Please set this value if you want to redirect the user to the custom page url(full url). If this field is not set they will be redirected back to home page.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class='fupsl-settings'>
                                <div class='fupsl-user-avatar-settings'>
                                    <div class="fupsl-label"><?php _e( 'User avatar', 'frontend_user_pro' ); ?> <span class='fupsl_show_hide'><i class="fa fa-caret-down"></i></span> </div>
                                    <div class='fupsl_network_settings_wrapper fupsl_network_settings_outer' style='display:none'>
                                        <input type='radio' id='fupsl_user_avatar_default' class='fupsl_user_avatar_options' name='fupsl_user_avatar_options' value='default' <?php
if( isset( $options['fupsl_user_avatar_options'] ) ) {
    checked( $options['fupsl_user_avatar_options'], 'default', 'true' );
}
?> /> <label for='fupsl_user_avatar_default'><?php _e( 'Use wordpress provided default avatar.', 'frontend_user_pro' ); ?></label><br /><br />
                                        <input type='radio' id='fupsl_user_avatar_social' class='fupsl_user_avatar_options' name='fupsl_user_avatar_options' value='social' <?php
if( isset( $options['fupsl_user_avatar_options'] ) ) {
    checked( $options['fupsl_user_avatar_options'], 'social', 'true' );
}
?> /> <label for='fupsl_user_avatar_social'><?php _e( 'Use the profile picture from social media where available.', 'frontend_user_pro' ); ?></label><br /><br />
                                        <div class='fupsl-info'>
                                            <span class='fupsl-info-note'><?php _e( 'instructions:', 'frontend_user_pro' ); ?></span> <br />
                                            <span class='fupsl-info-content'>Please choose the options from where you want your users avatar to be loaded from. If you choose default wordpress avatar it will use the gravatar profile image if user have gravatar profile assocated with their registered email address.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class='fupsl-settings'>
                                <div class='fupsl-user-email-settings'>
                                    <div class="fupsl-label"><?php _e( 'Email notification settings', 'frontend_user_pro' ); ?> <span class='fupsl_show_hide'><i class="fa fa-caret-down"></i></span> </div>
                                    <div class='fupsl_network_settings_wrapper fupsl_network_settings_outer' style='display:none'>
                                        <input type='radio' id='fupsl_send_email_notification_yes' class='fupsl_send_email_notification_yes' name='fupsl_send_email_notification_options' value='yes' <?php
if( isset( $options['fupsl_send_email_notification_options'] ) ) {
    checked( $options['fupsl_send_email_notification_options'], 'yes', 'true' );
}
?> /> <label for='fupsl_send_email_notification_yes'><?php _e( 'Send email notification to both user and site admin.', 'frontend_user_pro' ); ?></label><br /><br />
                                        <input type='radio' id='fupsl_send_email_notification_no' class='fupsl_send_email_notification_no' name='fupsl_send_email_notification_options' value='no' <?php
if( isset( $options['fupsl_send_email_notification_options'] ) ) {
    checked( $options['fupsl_send_email_notification_options'], 'no', 'true' );
}
?> /> <label for='fupsl_send_email_notification_no'><?php _e( 'Do not send email notification to both user and site admin.', 'frontend_user_pro' ); ?></label><br /><br />
                                        <div class='fupsl-info'>
                                            <span class='fupsl-info-note'><?php _e( 'instructions:', 'frontend_user_pro' ); ?></span> <br />
                                            <span class='fupsl-info-content'>Here you can configure an options to send email notifications about user registration to site admin and user.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <!-- how to use section -->
                        <div class='fupsl-tab-contents' id='tab-fupsl-how-to-use' style="display:none">
<?php //include( SFUP_PLUGIN_DIR . 'includes/backend/how-to-use.php' ); ?>
                        </div>

                        <!-- about section -->
                        <div class='fupsl-tab-contents' id='tab-fupsl-about' style="display:none">
<?php //include( SFUP_PLUGIN_DIR . 'includes/backend/about.php' ); ?>
                        </div>

                        <!-- Save settings Button -->
                        <div class='fupsl-save-settings'>
<?php wp_nonce_field( 'fupsl_nonce_save_settings', 'fupsl_settings_action' ); ?>
                            <input type='submit' class='fupsl-submit-settings primary-button' name='fupsl_save_settings' value='<?php _e( 'Save settings', 'frontend_user_pro' ); ?>' />
                        </div>

                        <div class='fupsl-restore-settings'>
<?php $nonce = wp_create_nonce( 'fupsl-restore-default-settings-nonce' ); ?>
                            <a href="<?php echo admin_url() . 'admin-post.php?action=fupsl_restore_default_settings&_wpnonce=' . $nonce; ?>" onclick="return confirm('<?php _e( 'Are you sure you want to restore default settings?', 'frontend_user_pro' ); ?>')"><input type="button" value="Restore Default Settings" class="fupsl-reset-button button primary-button"/></a>
                        </div>

                    </div>
            </div>
        </div>
    </div>
</div>