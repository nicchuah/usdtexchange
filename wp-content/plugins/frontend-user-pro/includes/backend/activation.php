<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
$fupsl_settings = array();
$social_networks = array(0 => 'facebook', 1 => 'twitter', 2 => 'google');

$fupsl_settings['network_ordering'] = $social_networks;
//facebook settings
$facebook_parameters = array('fupsl_facebook_enable' => '0', 'fupsl_facebook_app_id' => '', 'fupsl_facebook_app_secret' => '', 'fupsl_profile_image_width' => '50', 'fupsl_profile_image_height' => '50');
$fupsl_settings['fupsl_facebook_settings'] = $facebook_parameters;
//twitter settings
$twitter_parameters = array('fupsl_twitter_enable' => '0', 'fupsl_twitter_api_key' => '', 'fupsl_twitter_api_secret' => '');
$fupsl_settings['fupsl_twitter_settings'] = $twitter_parameters;
//google settings
$google_parameters = array('fupsl_google_enable' => '0', 'fupsl_google_client_id' => '', 'fupsl_google_client_secret' => '');

$fupsl_settings['fupsl_google_settings'] = $google_parameters;

$fupsl_settings['fupsl_enable_disable_plugin'] = 'yes';

$display_options = array('login_form', 'register_form', 'comment_form');
$fupsl_settings['fupsl_display_options'] = $display_options;

$fupsl_settings['fupsl_icon_theme'] = '1';

$fupsl_settings['fupsl_title_text_field'] = 'Social connect:';
$fupsl_settings['fupsl_custom_logout_redirect_options'] = 'home';
$fupsl_settings['fupsl_custom_logout_redirect_link'] = '';

$fupsl_settings['fupsl_custom_login_redirect_options'] = 'home';
$fupsl_settings['fupsl_custom_login_redirect_link'] = '';

$fupsl_settings['fupsl_user_avatar_options'] = 'default';

$fupsl_settings['fupsl_send_email_notification_options'] = 'yes';

update_option( SFUP_SETTINGS, $fupsl_settings );
