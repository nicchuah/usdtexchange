<?php defined( 'ABSPATH' ) or die( 'No script kiddies please!' ); ?>
<?php $options = get_option( SFUP_SETTINGS ); ?>
<?php
if ( is_user_logged_in() ) {
    global $current_user;
    $user_info = "<span class='display-name'>{$current_user->data->display_name}</span>&nbsp;";
    $user_info .= get_avatar( $current_user->ID, 20 );

    if ( !empty( $_GET['redirect'] ) )
        $current_url = $_GET['redirect'];
    else
        $current_url = SFUP_Login_Check_Class::curPageURL();

    if ( isset( $options['fupsl_custom_logout_redirect_options'] ) && $options['fupsl_custom_logout_redirect_options'] != '' ) {
        if ( $options['fupsl_custom_logout_redirect_options'] == 'home' ) {
            $user_logout_url = wp_logout_url( home_url() );
        } else if ( $options['fupsl_custom_logout_redirect_options'] == 'current_page' ) {
            $user_logout_url = wp_logout_url( $current_url );
        } else if ( $options['fupsl_custom_logout_redirect_options'] == 'custom_page' ) {
            if ( $options['fupsl_custom_logout_redirect_link'] != '' ) {
                $logout_page = $options['fupsl_custom_logout_redirect_link'];
                $user_logout_url = wp_logout_url( $logout_page );
            } else {
                $user_logout_url = wp_logout_url( $current_url );
            }
        }
    } else {
        $user_logout_url = wp_logout_url( $current_url );
    }
    ?><div class="user-login">Welcome <b><?php echo $user_info; ?></b>&nbsp;|&nbsp;<a href="<?php echo $user_logout_url; ?>" title="Logout">Logout</a></div>
    <?php
} else {
    ?>
    <?php
    $current_url = SFUP_Login_Check_Class:: curPageURL();
    $encoded_url = urlencode( $current_url );
    ?>

    <?php $theme = $options['fupsl_icon_theme']; ?>

    <div class='fupsl-login-networks theme-<?php echo $theme; ?> clearfix'>
        <?php if ( isset( $attr['login_text'] ) && $attr['login_text'] != '' ) { ?>
            <span class='fupsl-login-new-text'><?php echo $attr['login_text']; ?></span>
        <?php } ?>
        <?php if ( isset( $_REQUEST['error'] ) || isset( $_REQUEST['denied'] ) ) { ?>
            <div class='fupsl-error'><?php _e( 'You have Access Denied. Please authorize the app to login.', 'frontend_user_pro' ); ?></div>
        <?php } ?>
        <div class='social-networks'>
            <?php foreach ( $options['network_ordering'] as $key => $value ): ?>
                <?php if ( $options["fupsl_{$value}_settings"]["fupsl_{$value}_enable"] === 'enable' ) { ?>
                    <a href="<?php echo wp_login_url() ?>?fupsl_login_id=<?php echo $value; ?>_login<?php
                    if ( $encoded_url ) {
                        echo "&state=" . base64_encode( "redirect_to=$encoded_url" );
                    }
                    ?>" title='<?php
                       _e( 'Login with', 'frontend_user_pro' );
                       echo ' ' . $value;
                       ?>'>
                        <div class="fupsl-icon-block icon-<?php echo $value; ?>">
                            <i class="fa fa-<?php echo $value; ?>"></i>
                            <span class="fupsl-login-text"><?php _e( 'Login', 'frontend_user_pro' ); ?></span>
                            <span class="fupsl-long-login-text"><?php _e( 'Login with', 'frontend_user_pro' ); ?><?php echo ' ' . $value; ?></span>
                        </div>
                    </a>
                <?php } ?>
    <?php endforeach; ?>
        </div>
    </div>
<?php
}