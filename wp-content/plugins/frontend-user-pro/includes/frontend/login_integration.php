<?php
defined( 'ABSPATH' ) or die( "No script kiddies please!" );
$options = get_option( SFUP_SETTINGS );

$redirect_to = isset( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : '';

$encoded_url = urlencode( $redirect_to );
?>
<div class='fupsl-login-networks theme-<?php echo $options['fupsl_icon_theme']; ?> clearfix'>
    <span class='fupsl-login-new-text'><?php echo $options['fupsl_title_text_field']; ?></span>
    <?php if ( isset( $_REQUEST['error'] ) || isset( $_REQUEST['denied'] ) ) { ?>
        <div class='fupsl-error'>
            <?php _e( 'You have Access Denied. Please authorize the app to login.', 'frontend_user_pro' ); ?>
        </div>
    <?php } ?>

    <div class='social-networks'>
        <?php foreach ( $options['network_ordering'] as $key => $value ): ?>
            <?php if ( $options["fupsl_{$value}_settings"]["fupsl_{$value}_enable"] === 'enable' ) { ?>
                <a href="<?php echo wp_login_url(); ?>?fupsl_login_id=<?php echo $value; ?>_login<?php
                if ( $encoded_url ) {
                    echo "&state=" . base64_encode( "redirect_to=$encoded_url" );
                }
                ?>" title='<?php
                   _e( 'Login with', 'frontend_user_pro' );
                   echo ' ' . $value;
                   ?>' >
                    <div class="fupsl-icon-block icon-<?php echo $value; ?> clearfix">
                        <i class="fa fa-<?php echo $value; ?>"></i>
                        <span class="fupsl-login-text"><?php _e( 'Login', 'frontend_user_pro' ); ?></span>
                        <span class="fupsl-long-login-text"><?php _e( 'Login with', 'frontend_user_pro' ); ?><?php echo ' ' . $value; ?></span>
                    </div>
                </a>
            <?php } ?>
<?php endforeach; ?>
    </div>
</div>