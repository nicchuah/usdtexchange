jQuery(document).ready(function($){
    //for sorting the social networks
    $('.network-settings').sortable({
        containment: "parent"
    });

    //for the tabs
    $('.fupsl-settings-tab').click(function(){
        $( '.fupsl-settings-tab' ).removeClass( 'fupsl-active-tab' );
        $(this).addClass( 'fupsl-active-tab' );
        var tab_id = 'tab-'+$(this).attr('id');
        $('.fupsl-tab-contents').hide();
        $('#'+tab_id).show();
    });


    $('.fupsl-label').click(function(){
        $(this).closest('.fupsl-settings').find('.fupsl_network_settings_wrapper').toggle('slow', function(){
            if ($(this).closest('.fupsl-settings').find('.fupsl_network_settings_wrapper').is(':visible')) {
                $(this).closest('.fupsl-settings').find('.fupsl_show_hide i').removeClass('fa-caret-down');
                $(this).closest('.fupsl-settings').find('.fupsl_show_hide i').addClass('fa-caret-up');
            }else {
                $(this).closest('.fupsl-settings').find('.fupsl_show_hide i').removeClass('fa-caret-up');
                $(this).closest('.fupsl-settings').find('.fupsl_show_hide i').addClass('fa-caret-down');
             }

        });
    });

    // for hide show options based on logout redirect options
    $('.fupsl_custom_logout_redirect_options').click(function(){
       if($(this).val()==='custom_page') {
            $('.fupsl-custom-logout-redirect-link').show('slow');
        }else{
            $('.fupsl-custom-logout-redirect-link').hide('show');
        }
    });

    // for hide show options based on logout redirect options
    $('.fupsl_custom_login_redirect_options').click(function(){
       if($(this).val()==='custom_page') {
            $('.fupsl-custom-login-redirect-link').show('slow');
        }else{
            $('.fupsl-custom-login-redirect-link').hide('show');
        }
    });

});