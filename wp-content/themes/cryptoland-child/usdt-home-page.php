<?php

	/*
	Template name: USDT Home Template
	*/

	get_header();

	do_action('cryptoland_page_header_action');

	// the_content
	if ( have_posts() ) :
		while ( have_posts() ) : the_post();
			the_content();
		endwhile;
	endif;
?>
<?php

	get_footer();

?>
