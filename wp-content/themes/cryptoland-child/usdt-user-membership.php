<?php

	/*
	Template name: USDT User Membership
	*/

	get_header();

	do_action('cryptoland_page_header_action');

	$user = wp_get_current_user();
	$siteurl = site_url();
	$userid = $current_user->ID;
	if ( in_array( 'member', (array) $user->roles ) ) {    
?>
	<div class="container user-dashboard-container">
		<div class="row">
			<div class="left-sidebar col-md-3">
				<?php
					if ( is_user_logged_in() ) {
						wp_nav_menu( array(
							'menu'           => 'Membership Menu',
							'menu_class'      => '', 
						));	
					}
				?>					
			</div>
			<div class="right-content col-md-9">
				<div class="right-content-container">
					<?php if ( is_user_logged_in() ) { ?>
					<h2><?php the_title(); ?></h2>
					<?php } ?>
					
					<?php
					if ( have_posts() ) :
						while ( have_posts() ) : the_post();
							the_content();
						endwhile;
					endif;
					?>
					
				</div>		
			</div>
		</div>
	</div>
<?php
	} else {
		echo "<div class='before-redirect-message'>Sorry, u're not allowed here, redirecting you now...</div>";		

		if ( in_array( 'administrator', (array) $user->roles ) ) { 
			header("Refresh:5; url=$siteurl/wp-admin");
		} else {
			header("Refresh:5; url=$siteurl");
		}
	}
?>
<?php

	get_footer();

?>
