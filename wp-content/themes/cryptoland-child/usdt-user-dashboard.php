<?php

	/*
	Template name: USDT User Dashboard
	*/

	get_header();

	do_action('cryptoland_page_header_action');

	$user = wp_get_current_user();
	$siteurl = site_url();
	$userid = $current_user->ID;
	if ( in_array( 'member', (array) $user->roles ) ) {    
?>
	<div class="container user-dashboard-container">
		<div class="row">
			<div class="left-sidebar vc_col-md-3">
				<?php
					if ( is_user_logged_in() ) {
						wp_nav_menu( array(
							'menu'           => 'Membership Menu',
							'menu_class'      => '', 
						));	
					}					
				?>					
			</div>
			<div class="right-content vc_col-md-9">
				<div class="right-content-container">
					<h2>Welcome <?php echo ucfirst($current_user->display_name);?>!</h2>					
				
										

					<div class="balance-container">
						<h3>Available Balances</h3>
						<div class="row balance-row">
							<div class="col-md-3 balance-col"><img class="bitcoin-img" src="<?php echo get_bloginfo('stylesheet_directory');?>/img/bitcoin-img.png" alt="Bitcoin logo" height="40" width="40"><span><strong>BTC</strong><?php echo ((do_shortcode('[swpm_show_member_info column="Bitcoin"]'))!='')?do_shortcode('[swpm_show_member_info column="Bitcoin"]'):'0'; ?></span></div>
							<div class="col-md-3 balance-col"><img class="bitcoin-img" src="<?php echo get_bloginfo('stylesheet_directory');?>/img/eth-img.png" alt="Ethereum logo" height="40" width="40"><span><strong>ETH</strong><?php echo ((do_shortcode('[swpm_show_member_info column="Ethereums"]'))!='')?do_shortcode('[swpm_show_member_info column="Ethereums"]'):'0'; ?></span></div>
							<div class="col-md-3 balance-col"><img class="bitcoin-img" src="<?php echo get_bloginfo('stylesheet_directory');?>/img/usdt-img.png" alt="Tether logo" height="40" width="40"><span><strong>USDT</strong><?php echo ((do_shortcode('[swpm_show_member_info column="Tethers"]'))!='')?do_shortcode('[swpm_show_member_info column="Tethers"]'):'0'; ?></span></div>
							<div class="col-md-3 balance-col"><img class="bitcoin-img" src="<?php echo get_bloginfo('stylesheet_directory');?>/img/euro-img.png" alt="Euro logo" height="40" width="40"><span><strong>EURO</strong><?php echo ((do_shortcode('[swpm_show_member_info column="Euro"]'))!='')?do_shortcode('[swpm_show_member_info column="Euro"]'):'0'; ?></span></div>							
							<div class="col-md-3 balance-col"><img class="bitcoin-img" src="<?php echo get_bloginfo('stylesheet_directory');?>/img/usd-img.png" alt="USD logo" height="40" width="40"><span><strong>USD</strong><?php echo ((do_shortcode('[swpm_show_member_info column="US Dollar"]'))!='')?do_shortcode('[swpm_show_member_info column="US Dollar"]'):'0'; ?></span></div>
						</div>
					</div>

					<div class="overflow-container">
						<h3>Transaction Quick View</h3>
						<table class="cryptotable transaction-table">
							<thead class="ct-head">
								<tr>
									<th>Date/Time</th>
									<th>Amount</th>
									<th>Currency Type</th>
									<th>Transaction Type</th>
									<th>Transaction Fee</th>
									<th>Payment Platform</th>
									<th>Status</th>							
								</tr>		
							</thead>
							<tbody class="ct-body">

							<?php 
							$args = 'posts_per_page=10&post_type=transaction&orderby=date&order=desc&meta_key=user&meta_value='.$userid;                
							query_posts( $args );

							// The Loop
							if ( have_posts() ) :
								while ( have_posts() ) : the_post();							
									$transdate = get_the_date( 'd M Y', $post->ID );	
									$transtime = get_the_time( '', $post->ID );						
								?>
										<tr class="crypto-row">
										<td><div class="date"><?php echo $transdate; ?></div><div class="time"><?php echo $transtime; ?></div></td>
										<td class="amount">
											<div>															
												<?php echo number_format(get_field('amount'), 2, '.', '');?>
											</div>				
										</td>
										<td>
										<div class="crypto-price"><?php echo get_field('type_of_currencies');?></div>
										</td>
										<td class="transact-type-td">
											<?php if(get_field('type_of_transaction') == 'Deposit'){ ?>
												<div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/deposit-icon.png" /><?php echo get_field('type_of_transaction');?></div>
											<?php } else if(get_field('type_of_transaction') == 'Transfer'){ ?>
												<div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/transfer-icon.png" /><?php echo get_field('type_of_transaction');?></div>
											<?php } else { ?>
												<div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/withdraw-icon.png" /><?php echo get_field('type_of_transaction');?></div>
											<?php } ?>
										</td>
										<td>
												<div>€ <?php echo get_field('transaction_fee');?></div>
										</td>
										<td>
												<div><?php echo get_field('payment_platform');?></div>
										</td>
										<td>
													<div><?php echo (get_field('status') == '')?"-":get_field('status');?></div>
											</td>
										</tr>
										
										
								<?php																			
								endwhile;											
								else :
								?>
									<tr class="crypto-row"><td colspan="7">No transaction yet.</td></tr>
								<?php 
								endif;
													
							?>
							</tbody>
						</table>
						<?php 											
						// Reset Query
						wp_reset_query();
						?>
					</div>					
				</div>		
			</div>
		</div>
	</div>
<?php
	} else {
		echo "<div class='before-redirect-message'>Sorry, u're not allowed here, redirecting you now...</div>";		

		if ( in_array( 'administrator', (array) $user->roles ) ) { 
			header("Refresh:5; url=$siteurl/wp-admin");
		} else {
			header("Refresh:5; url=$siteurl");
		}
	}
?>
<?php

	get_footer();

?>
