<?php

	/*
	Template name: USDT User Reset Password
	*/

	get_header();

	do_action('cryptoland_page_header_action');	
	
	$user = wp_get_current_user();
	$siteurl = site_url();
	$userid = $current_user->ID;
	if ( in_array( 'member', (array) $user->roles ) ) {    
?>
	<div class="container user-dashboard-container">
		<div class="row">
			<div class="left-sidebar col-md-3">
				<?php
					wp_nav_menu( array(
						'menu'           => 'User Menu',
						'menu_class'      => '', 
					));
				?>					
			</div>
			<div class="right-content col-md-9">
				<div class="right-content-container">
					<h3>Reset Your Password</h3>
					<?php echo do_shortcode('[wpfeup-resetpassword]'); ?>
				</div>		
			</div>
		</div>
	</div>
<?php
	} else {
		echo "<div class='before-redirect-message'>Sorry, u're not allowed here, redirecting you now...</div>";		

		if ( in_array( 'administrator', (array) $user->roles ) ) { 
			header("Refresh:5; url=$siteurl/wp-admin");
		} else {
			header("Refresh:5; url=$siteurl");
		}
	}
?>
<?php

	get_footer();

?>
