<?php

	/*
	Template name: USDT User Transactions
	*/

	get_header();

	do_action('cryptoland_page_header_action');

	$user = wp_get_current_user();
	$siteurl = site_url();
	$userid = $current_user->ID;
	if ( in_array( 'member', (array) $user->roles ) ) {    
?>
	<div class="container user-dashboard-container">
		<div class="row">
			<div class="left-sidebar col-md-3">
				<?php
					if ( is_user_logged_in() ) {
						wp_nav_menu( array(
							'menu'           => 'Membership Menu',
							'menu_class'      => '', 
						));	
					}					
				?>					
			</div>
			<div class="right-content col-md-9">
				<div class="right-content-container">									
					<h2>Transactions History</h2>
					<script>						
						jQuery('document').ready(function(){				
							jQuery('#transaction-month-filter option[value="<?php echo $_GET['date']; ?>"]').attr('selected','selected');						
						});	
						function filterpage() {							
							url = location.protocol + '//' + location.host + '/transactions/?&date=' + document.getElementById("transaction-month-filter").value;							
							window.location.href=url;
						}		
					</script>	
					
						<select id="transaction-month-filter" onChange="filterpage()">
							<option value="month/year">Filter by Month/Year</option>					
							<?php							
								$filterargs = 'post_type=transaction&orderby=date&order=desc&meta_key=user&meta_value='.$userid;    
								query_posts( $filterargs );						
								
								$darray = array();
								while ( have_posts() ) : the_post();						
									if(!in_array(get_the_date( 'F Y', $post->ID ), $darray, true)){
										array_push($darray, get_the_date( 'F Y', $post->ID ));	
									}
								endwhile;

								foreach ($darray as $value) {
									echo '<option value="'.date("m/Y", strtotime($value)).'">'.$value.'</option>';
							   	}
															
								wp_reset_query();
							?>
						</select>
						<div class="overflow-container">							
							<table class="cryptotable transaction-table">
								<thead class="ct-head">
									<tr>
										<th>Date/Time</th>
										<th>Amount</th>
										<th>Currency Type</th>
										<th>Transaction Type</th>
										<th>Fee</th>
										<th>Payment Platform</th>
										<th>Transferred To</th>
										<th>Received From</th>
										<th>Status</th>						
									</tr>		
								</thead>
								<tbody class="ct-body">

								<?php 
								$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
								$date = explode('/',$_GET['date']);
								$month = $date[0];
								$year = $date[1];						
								$args = 'posts_per_page=20&post_type=transaction&orderby=date&order=desc&paged='.$paged.'&meta_key=user&meta_value='.$userid.'&year=' . $year . '&monthnum=' . $month ;    
								query_posts( $args );						
								
								// The Loop
								if ( have_posts() ) :
									while ( have_posts() ) : the_post();							
										$transdate = get_the_date( 'd M Y', $post->ID );
										$transtime = get_the_time( '', $post->ID );						
									?>
											<tr class="crypto-row">
											<td><div class="date"><?php echo $transdate; ?></div><div class="time"><?php echo $transtime; ?></div></td>
											<td class="amount">
												<div>															
													<?php echo number_format(get_field('amount'), 2, '.', '');?>
												</div>				
											</td>
											<td>
											<div class="crypto-price"><?php echo get_field('type_of_currencies');?></div>
											</td>
											<td class="transact-type-td">
												<?php if(get_field('type_of_transaction') == 'Deposit'){ ?>
													<div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/deposit-icon.png" /><?php echo get_field('type_of_transaction');?></div>
												<?php } else if(get_field('type_of_transaction') == 'Transfer'){ ?>
													<div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/transfer-icon.png" /><?php echo get_field('type_of_transaction');?></div>
												<?php } else { ?>
													<div><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/withdraw-icon.png" /><?php echo get_field('type_of_transaction');?></div>
												<?php } ?>
											</td>
											<td>
												<div>€ <?php echo get_field('transaction_fee');?></div>
											</td>
											<td>
													<div><?php echo get_field('payment_platform');?></div>
											</td>
											<td>
													<div><?php echo (get_field('transferred_to') == '')?"-":get_field('transferred_to');?></div>
											</td>
											<td>
													<div><?php echo (get_field('received_from') == '')?"-":get_field('received_from');?></div>
											</td>
											<td>
													<div><?php echo (get_field('status') == '')?"-":get_field('status');?></div>
											</td>
											</tr>
											
											
									<?php																			
									endwhile;											
									else :
									?>
										<tr class="crypto-row"><td colspan="9">No transaction yet.</td></tr>
									<?php 
									endif;
														
								?>
								</tbody>
							</table>
							<?php 
								the_posts_pagination( array( 'mid_size'  => 5 ) ); 
								
								// Reset Query
								wp_reset_query();
							?>
						</div>
				</div>		
			</div>
		</div>
	</div>
<?php
	} else {
		echo "<div class='before-redirect-message'>Sorry, u're not allowed here, redirecting you now...</div>";		

		if ( in_array( 'administrator', (array) $user->roles ) ) { 
			header("Refresh:5; url=$siteurl/wp-admin");
		} else {
			header("Refresh:5; url=$siteurl");
		}
	}
?>
<?php

	get_footer();

?>
