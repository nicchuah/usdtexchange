<?php
/**
* Theme functions and definitions.
* This child theme was generated by Merlin WP.
*
* @link https://developer.wordpress.org/themes/basics/theme-functions/
*/

/*
* If your child theme has more than one .css file (eg. ie.css, style.css, main.css) then
* you will have to make sure to maintain all of the parent theme dependencies.
*
* Make sure you're using the correct handle for loading the parent theme's styles.
* Failure to use the proper tag will result in a CSS file needlessly being loaded twice.
* This will usually not affect the site appearance, but it's inefficient and extends your page's loading time.
*
* @link https://codex.wordpress.org/Child_Themes
*/
function cryptoland_child_enqueue_styles() {
    wp_enqueue_style( 'cryptoland-child-style',
        get_stylesheet_directory_uri() . '/style.css',
        false,
        wp_get_theme()->get('Version')
    );
    wp_enqueue_script( 'usdt-script', get_stylesheet_directory_uri() . '/js/usdt-script.js', false, wp_get_theme()->get('Version'));
}

add_action(  'wp_enqueue_scripts', 'cryptoland_child_enqueue_styles' );


/*** CRYPTO PRICE TABLE ***/
function cryptotable_function() { 
       
    $table = '<table class="cryptotable table">
            <thead class="ct-head">
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Change</th>
                    <th>Market Cap</th>
                </tr>		
            </thead>
            <tbody class="ct-body">';     
           
    /*** BITCOIN ***/        
    $table .= '<tr class="crypto-row">
    <td>
        <div class="crypto-container">
            <img class="bitcoin-img" src="'.get_bloginfo('stylesheet_directory').'/img/bitcoin-img.png" alt="Bitcoin logo" aria-label="Bitcoin logo" height="40" width="40">
            <div class="crypto-name">
                <span>Bitcoin</span>
                <span class="crypto-short">BTC</span>
            </div>
        </div>				
    </td>';       
            
    $args = 'posts_per_page=1&post_type=bitcoin&orderby=date&order=desc';            
    query_posts( $args );

    // The Loop
    while ( have_posts() ) : the_post();
    $table .= '<td>
    <div class="crypto-price pink">USD '.get_the_title().'</div>
    </td>';            
    $table .='<td>
            <div class="crypto-change">';
                
    $percentage = get_field('changes_percentage');		
    $percentupdown = ($percentage[0] == '-')?"down":"up";
        
    $table .= '<span class="'.$percentupdown.'">'.$percentage.'%</span></div></td>'; 
    $table .= '<td>
    <div class="crypto-price">USD '.get_field('market_cap').'</div>
    </td></tr>';  
    
    endwhile;
        
    // Reset Query
    wp_reset_query();
    /*** END OF BITCOIN ***/


    /*** ETHEREUM ***/
    $table .= '<tr class="crypto-row">
    <td>
        <div class="crypto-container">
            <img class="bitcoin-img" src="'.get_bloginfo('stylesheet_directory').'/img/eth-img.png" alt="Ethereum logo" aria-label="Ethereum logo" height="40" width="40">
            <div class="crypto-name">
                <span>Ethereum</span>
                <span class="crypto-short">ETH</span>
            </div>
        </div>				
    </td>';       
            
    $args1 = 'posts_per_page=1&post_type=eth&orderby=date&order=desc';            
    query_posts( $args1 );

    // The Loop
    while ( have_posts() ) : the_post();
    $table .= '<td>
    <div class="crypto-price">USD '.get_the_title().'</div>
    </td>';            
    $table .='<td>
            <div class="crypto-change">';
                
    $percentage = get_field('changes_percentage');		
    $percentupdown = ($percentage[0] == '-')?"down":"up";
        
    $table .= '<span class="'.$percentupdown.'">'.$percentage.'%</span></div></td>';
    $table .= '<td>
    <div class="crypto-price">USD '.get_field('market_cap').'</div>
    </td></tr>';   
    
    endwhile;
        
    // Reset Query
    wp_reset_query();
    /*** END OF ETHEREUM ***/

    /*** USDT ***/
    $table .= '<tr class="crypto-row">
    <td>
        <div class="crypto-container">
            <img class="bitcoin-img" src="'.get_bloginfo('stylesheet_directory').'/img/usdt-img.png" alt="Tether logo" aria-label="Tether logo" height="40" width="40">
            <div class="crypto-name">
                <span>Tether</span>
                <span class="crypto-short">USDT</span>
            </div>
        </div>				
    </td>';       
            
    $args2 = 'posts_per_page=1&post_type=usdt&orderby=date&order=desc';            
    query_posts( $args2 );

    // The Loop
    while ( have_posts() ) : the_post();
    $table .= '<td>
    <div class="crypto-price">USD '.get_the_title().'</div>
    </td>';            
    $table .='<td>
            <div class="crypto-change">';
                
    $percentage = get_field('changes_percentage');		
    $percentupdown = ($percentage[0] == '-')?"down":"up";
        
    $table .= '<span class="'.$percentupdown.'">'.$percentage.'%</span></div></td>'; 
    $table .= '<td>
    <div class="crypto-price">USD '.get_field('market_cap').'</div>
    </td></tr>';  
    
    endwhile;
        
    // Reset Query
    wp_reset_query();
    /*** END OF USDT ***/
            
    $table .='</tbody></table>';
    return $table;
 }

 function register_shortcodes(){
    add_shortcode('cryptotable', 'cryptotable_function');
 }

 add_action( 'init', 'register_shortcodes');

 function custom_header(){
    $hd = ot_get_option( 'cryptoland_header_display' );
        $hdtxtrans = ' '.ot_get_option( 'cryptoland_header_menu_txtrans' );
        $sh = ot_get_option( 'cryptoland_sticky_header' );
        $hld = ot_get_option( 'cryptoland_header_lang_display' );
        $hl = ot_get_option( 'cryptoland_header_lang', array() );

        $s_in = ot_get_option( 'cryptoland_header_signin_display' );
        $s_up = ot_get_option( 'cryptoland_header_signup_display' );
        $s_in_t = ot_get_option( 'cryptoland_header_signin_title' );
        $s_up_t = ot_get_option( 'cryptoland_header_signup_title' );
        $s_in_url = ot_get_option( 'cryptoland_header_signin_url' );
        $s_up_url = ot_get_option( 'cryptoland_header_signin_url' );
        $s_in_target = ot_get_option( 'cryptoland_header_signin_target' );
        $s_up_target = ot_get_option( 'cryptoland_header_signin_target' );
        $btn_tel_d = ot_get_option( 'cryptoland_header_telegram_display' );
        $tel_img = ot_get_option( 'cryptoland_header_telegram_img' );
        $tel_url = ot_get_option( 'cryptoland_header_telegram_url' );

        $p_h_d = rwmb_meta( 'cryptoland_page_header_display', true );
        $p_s_h = rwmb_meta( 'cryptoland_page_sticky_nav', true );
        $p_l_d = rwmb_meta( 'cryptoland_page_header_lang', true );
        $p_s_in_d = rwmb_meta( 'cryptoland_page_header_btnsignin', true );
        $p_s_up_d = rwmb_meta( 'cryptoland_page_header_btnsignup', true );
        $p_s_in = get_post_meta(get_the_ID(), 'cryptoland_page_header_btnsignin_title');
        $p_s_up = get_post_meta(get_the_ID(), 'cryptoland_page_header_btnsignup_title');
        $p_s_int = rwmb_meta('cryptoland_page_header_btnsigin_target');
        $p_s_upt = rwmb_meta('cryptoland_page_header_btnsignup_target');
        $p_tel_d = rwmb_meta( 'cryptoland_page_header_btntel_display', true );
        $p_tel_img = wp_get_attachment_url( get_post_meta(get_the_ID(), 'cryptoland_page_header_btntelimg', true ),'full' );
        $p_tel_url = rwmb_meta( 'cryptoland_page_header_btntelurl', true );
        $p_m_m = rwmb_meta( 'cryptoland_page_metabox_menu');
        $p_m_m_d = rwmb_meta( 'cryptoland_page_metabox_menu_display');

        if( is_page() ){
            $hd = $hd != 'off' ? $p_h_d  : $hd;
            $sh = $sh != 'off' ? $p_s_h  : $sh;
            $hld = $hld != 'off' ? $p_l_d  : $hld;
            $s_in = $s_in !='off' ? $p_s_in_d : $s_in;
            $s_up = $s_up !='off' ? $p_s_up_d : $s_up;
            $s_in_t = !empty($p_s_in[0]) && isset($p_s_in[0]) != '' ? $p_s_in[0] : $s_in_t;
            $s_in_url = !empty($p_s_in[1]) && isset($p_s_in[1]) != '' ? $p_s_in[1] : $s_in_url;
            $s_up_t = !empty($p_s_up[0]) && isset($p_s_up[0]) != '' ? $p_s_up[0] : $s_up_t;
            $s_up_url = !empty($p_s_up[1]) && isset($p_s_up[1]) != '' ? $p_s_up[1] : $s_up_url;
            $btn_tel_d = $btn_tel_d != 'off' ? $p_tel_d : $btn_tel_d;
            $tel_img = !empty($p_tel_img) && $p_tel_img !='' ? $p_tel_img : $tel_img;
            $tel_url = !empty($p_tel_url) && $p_tel_url !='' ? $p_tel_url : $tel_url;
            $s_in_target = $p_s_int;
            $s_up_target = $p_s_upt;

        }

        if ( $hd != 'off' ) { ?>

            <header class="header sticky-<?php echo esc_attr( $sh.$hdtxtrans ); ?>">

                <?php
                $logoclass = 'logo';
                do_action( 'cryptoland_logo_action', $logoclass );

                //metabox menu
                if( is_page() AND ( !empty( $p_m_m ) ) && ( $p_m_m_d != true ) ){ ?>
                    <ul class="menu">
                        <?php foreach ( $p_m_m as $m_i ) {
                            $menu_title = !empty($m_i[0]) ? $m_i[0] : esc_html__('Add title', 'cryptoland');
                            $menu_link = !empty($m_i[1]) ? $m_i[1] : '#0';
                            ?>
                            <li class="menu__item"><a href="<?php echo esc_attr( $menu_link ); ?>" title="<?php echo esc_attr( $menu_title ); ?>" class="menu__link"><?php echo esc_html( $menu_title ); ?></a></li>
                        <?php } ?>
                    </ul>
                    <?php
                    //default wp menu
                } else {
                    wp_nav_menu( array(
                        'menu'            => 'header_menu_1',
                        'theme_location'  => 'header_menu_1',
                        'depth'           => 2,
                        'menu_class'      => 'menu',
                        'menu_id'         => '',
                        'echo'            => true,
                        'fallback_cb'     => 'Cryptoland_Wp_Bootstrap_Navwalker::fallback',
                        'walker'          => new Cryptoland_Wp_Bootstrap_Navwalker()
                    ));
                }
                ?>

                <?php if ( $hld != 'off' OR $s_in != 'off' OR $s_up != 'off' OR $btn_tel_d != 'off' ) : ?>
                    <div class="header__right">
                        <?php if( $btn_tel_d != 'off' AND !empty($tel_img) ) : ?>
                            <a href="<?php echo esc_attr( $tel_url ); ?>" class="telegram-link">
                                <img src="<?php echo esc_attr( $tel_img ); ?>">
                            </a>
                        <?php endif; ?>

                        <?php if ( function_exists('pll_the_languages') ) : ?>
                            <?php cryptoland_header_lang(); ?>
                        <?php else : ?>
                            <?php if ( $hld != 'off' AND !empty($hl) ) : ?>
                                <select class="select">
                                    <?php foreach ( $hl as $item ) { ?>
                                        <?php $cryptoland_lang_item_url = isset($item['cryptoland_lang_item_url']) != '' ? $item['cryptoland_lang_item_url'] : ''; ?>
                                        <option value="<?php echo esc_attr( $cryptoland_lang_item_url ); ?>" ><?php echo esc_html( $item['cryptoland_lang_item'] ) ?></option>
                                    <?php } ?>
                                </select>
                            <?php endif; ?>
                        <?php endif; ?>


                        <?php if ( !(is_user_logged_in()) ) { ?>
                            <div class="sign-in-wrap"><a href="<?php echo site_url(); ?>/membership-login/" target="" class="btn-sign-in">Login</a></div>
                            <div class="sign-up-wrap"><a href="<?php echo site_url(); ?>/membership-registration/" target="" class="btn-sign-up">Sign Up</a></div>
                        <?php } else { ?>
                            <div class="sign-up-wrap"><a href="<?php echo site_url(); ?>/user-dashboard/" target="" class="btn-sign-up btn-dashboard">Dashboard</a></div>
                            <div class="sign-in-wrap"><a href="?swpm-logout=true" target="" class="btn-sign-in">Logout</a></div>                                                        
                        <?php } ?>
                        
                    </div>
                <?php endif; ?>

                <div class="btn-menu">
                    <div class="one"></div>
                    <div class="two"></div>
                    <div class="three"></div>
                </div>
            </header>

            <div class="fixed-menu<?php echo esc_attr( $hdtxtrans ); ?>">
                <div class="fixed-menu__header">

                    <?php
                    $moblogo = ot_get_option('cryptoland_mob_logo');
                    $btnclose = $moblogo == 'mob-logo-off' ? ' align-right' : '';
                    $logoclass = 'logo logo--color '.$moblogo;
                    do_action( 'cryptoland_logo_action', $logoclass );
                    ?>

                    <div class="btn-close<?php echo esc_attr( $btnclose ); ?>">
                        <svg xmlns="<?php echo esc_url( 'http://www.w3.org/2000/svg' ); ?>" xmlns:xlink="<?php echo esc_url( 'http://www.w3.org/1999/xlink' ); ?>" version="1.1" x="0px" y="0px" viewBox="0 0 47.971 47.971" style="enable-background:new 0 0 47.971 47.971;" xml:space="preserve" width="512px" height="512px">
                            <path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88   c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242   C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879   s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z" fill="#006DF0"/></svg>
                    </div>
                </div>

                <div class="fixed-menu__content">

                    <?php
                    $mob_btn = ( ot_get_option('cryptoland_header_btn_mobile_display') );
                    $mob_lang = ( ot_get_option('cryptoland_header_lang_mobile_display') );

                    //metabox menu
                    if( is_page() AND ( !empty( $p_m_m ) ) && ( $p_m_m_d != true ) ){ ?>
                        <ul class="mob-menu">
                            <?php foreach ( $p_m_m as $m_i ) {
                                $menu_title = isset($m_i[0]) != '' ? $m_i[0] : 'Add title';
                                $menu_link = isset($m_i[1]) != '' ? $m_i[1] : '#0';
                                ?>
                                <li class="mob-menu__item"><a href="<?php echo esc_url( $menu_link ); ?>" title="<?php echo esc_attr( $menu_title ); ?>" class="mob-menu__link"><?php echo esc_html( $menu_title ); ?></a></li>
                            <?php } ?>
                        </ul>
                        <?php
                        //default wp menu
                    } else {
                        wp_nav_menu( array(
                            'menu'           => 'header_menu_1',
                            'theme_location' => 'header_menu_1',
                            'depth'          => 2,
                            'menu_class'     => 'mob-menu',
                            'menu_id'        => '',
                            'echo'           => true,
                            'fallback_cb'    => 'Cryptoland_Mob_Wp_Bootstrap_Navwalker::fallback',
                            'walker'         => new Cryptoland_Mob_Wp_Bootstrap_Navwalker()
                        ));
                    }
                    ?>

                    <?php if ( function_exists( 'pll_the_languages' ) ) : ?>
                        <?php cryptoland_header_lang(); ?>
                    <?php else : ?>
                        <?php if ( $hld != 'off' AND $hl != '' ) : ?>
                            <select class="select mob-<?php echo esc_attr( $mob_lang ) ?>">
                                <?php foreach ( $hl as $item ) { ?>
                                    <?php $cryptoland_lang_item_url = isset($item['cryptoland_lang_item_url']) != '' ? $item['cryptoland_lang_item_url'] : ''; ?>
                                    <option value="<?php echo esc_attr( $cryptoland_lang_item_url ); ?>" ><?php echo esc_html( $item['cryptoland_lang_item'] ) ?></option>
                                <?php } ?>
                            </select>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if ( !(is_user_logged_in()) ) { ?>
                            <div class="sign-in-wrap"><a href="<?php echo site_url(); ?>/membership-login/" target="" class="btn-sign-in">Login</a></div>
                            <div class="sign-up-wrap"><a href="<?php echo site_url(); ?>/membership-registration/" target="" class="btn-sign-up">Sign Up</a></div>
                        <?php } else { ?>
                            <div class="sign-up-wrap"><a href="<?php echo site_url(); ?>/user-dashboard/" target="" class="btn-sign-up btn-dashboard">Dashboard</a></div>
                            <div class="sign-in-wrap"><a href="?swpm-logout=true" target="" class="btn-sign-in">Logout</a></div>                                                        
                        <?php } ?>                

                    
                </div>
            </div>
            <?php
        }
 }
 add_action( 'cryptoland_custom_header_action',  'custom_header', 10 );

 /* Disable WordPress Admin Bar for all users */
 add_filter( 'show_admin_bar', '__return_false' );

/*** REMOVE POST FROM ADMIN DASHBOARD  ***/
function post_remove (){ 
   remove_menu_page('edit.php');
}

add_action('admin_menu', 'post_remove');

// Removes from admin menu
add_action( 'admin_menu', 'my_remove_admin_menus' );
function my_remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
}
// Removes from post and pages
add_action('init', 'remove_comment_support', 100);

function remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}
// Removes from admin bar
function mytheme_admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );

add_action('admin_head', 'my_custom_fonts');


/*** HIDE UNWANTED FIELDS IN ADMIN MEMBER FORM ***/
function my_custom_fonts() {
  echo '<style>
  .swpm-admin-edit-address-street,
  .swpm-admin-edit-address-city,
  .swpm-admin-edit-address-state,
  .swpm-admin-edit-address-country,
  .swpm-admin-edit-address-zipcode,
  .swpm-admin-edit-company,
  .swpm-admin-edit-subscriber-id,
  .swpm-admin-edit-gender {
    display: none;
  } 
  </style>';
}