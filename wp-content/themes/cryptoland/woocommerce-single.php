

<div id="crypterium-woo-single" class="crypterium-woo-single"> <!-- Woo shop page general div -->

	<!-- HERO SECTION -->
	<?php cryptoland_hero_section(); ?>

    <div id="single-page-container" class="bg-white c-section -space-large">
		<div class="grid container">
			<div class="row row-xs-middle">

				<!-- Right sidebar -->
				<?php if( ot_get_option( 'cryptoland_cryptoland_woosingle_layout_layout' ) == 'right-sidebar' || ot_get_option( 'cryptoland_woosingle_layout' ) == '') { ?>
					<div class="col col-lg-8 col-md-8 col-sm-12 posts">

				<!-- Left sidebar -->
				<?php } elseif( ot_get_option( 'cryptoland_woosingle_layout' ) == 'left-sidebar') { ?>
                    <div id="widget-area" class="widget-area col-lg-4 col-xs-12 col-md-4 col-sm-12">
                        <?php dynamic_sidebar( 'cryptoland_product_sidebar' ); ?>
                    </div>
				    <div class="col col-lg-8 col-md-8 col-sm-12 posts">

				<!-- Sidebar none -->
				<?php } elseif( ot_get_option( 'cryptoland_woosingle_layout' ) == 'full-width') { ?>
					<div class="full-width-index col col--md-12">
				<?php } ?>

					<?php woocommerce_content(); ?>

			   </div><!-- End sidebar + content -->

				<?php if( ot_get_option( 'cryptoland_woosingle_layout' ) == 'right-sidebar' || ot_get_option( 'cryptoland_woosingle_layout' ) == '') { ?>
                    <div id="widget-area" class="widget-area col-lg-4 col-xs-12 col-md-4 col-sm-12">
                        <?php dynamic_sidebar( 'cryptoland_product_sidebar' ); ?>
                    </div>
                <?php } ?>

			</div><!-- End row -->
		</div><!-- End #container -->
	</div><!-- End #blog -->
</div><!-- End woo shop page general div -->
