<?php
	/**  The template for displaying the footer 	**/

	echo "</div>"; // Site Wrapper End

    if ( 'page' == ot_get_option( 'cryptoland_footer_template_type' ) && function_exists( 'cryptoland_vc_inject_shortcode_css' ) ) {

        if ( '' != ot_get_option( 'cryptoland_footer_custom_template' ) ) {

            cryptoland_vc_inject_shortcode_css( ot_get_option( 'cryptoland_footer_custom_template' ) );

            $content = get_post_field( 'post_content', ot_get_option( 'cryptoland_footer_custom_template' ) );

            echo do_shortcode( $content );
        }

    } elseif ( 'custom' == ot_get_option( 'cryptoland_footer_template_type' ) ) {

        if ( '' != ot_get_option( 'cryptoland_footer_custom_html' ) ) {

            echo do_shortcode( ot_get_option( 'cryptoland_footer_custom_html' ) );

        }

    } else {

        // theme footer area
        do_action('cryptoland_widgetize_action');
        do_action('cryptoland_copyright_action');

    }

    // action for add elements after theme footer
    do_action('cryptoland_after_footer');
    wp_footer();

?>

	</body>
</html>
